\begin{thebibliography}{83}
\expandafter\ifx\csname natexlab\endcsname\relax\def\natexlab#1{#1}\fi

\bibitem[{Akmal {et~al.}(1998)Akmal, Pandharipande, \& Ravenhall}]{akm98}
Akmal, A., Pandharipande, V.~R., \& Ravenhall, D.~G. 1998, Phys. Rev. C, 58,
  1804

\bibitem[{Antia(1993)}]{ant93}
Antia, H. 1993, The Astrophysical Journal Supplement Series, 84, 101

\bibitem[{Antoniadis {et~al.}(2013)Antoniadis, Freire, Wex, Tauris, Lynch, van
  Kerkwijk, Kramer, Bassa, Dhillon, Driebe, {et~al.}}]{ant13}
Antoniadis, J., Freire, P.~C., Wex, N., {et~al.} 2013, Science, 340, 1233232

\bibitem[{Baldo(1999)}]{bal99}
Baldo, M. 1999, Nuclear methods and the nuclear equation of state, Vol.~8
  (World Scientific)

\bibitem[{Baldo(2014)}]{bal14a}
Baldo, M. 2014, private communication

\bibitem[{Baldo {et~al.}(1997)Baldo, Bombaci, \& Burgio}]{bal97}
Baldo, M., Bombaci, I., \& Burgio, G. 1997, Astronomy and Astrophysics, 328,
  274

\bibitem[{Baldo {et~al.}(2014{\natexlab{a}})Baldo, Burgio, Centelles, Sharma,
  \& Vi{\~n}as}]{bal14NS}
Baldo, M., Burgio, G., Centelles, M., Sharma, B., \& Vi{\~n}as, X.
  2014{\natexlab{a}}, Physics of Atomic Nuclei, 77, 1157

\bibitem[{Baldo {et~al.}(2014{\natexlab{b}})Baldo, Burgio, Schulze, \&
  Taranto}]{bal14}
Baldo, M., Burgio, G.~F., Schulze, H.-J., \& Taranto, G. 2014{\natexlab{b}},
  Phys. Rev. C, 89, 048801

\bibitem[{Baldo {et~al.}(2012)Baldo, Polls, Rios, Schulze, \& Vida\~na}]{bal12}
Baldo, M., Polls, A., Rios, A., Schulze, H.-J., \& Vida\~na, I. 2012, Phys.
  Rev. C, 86, 064001

\bibitem[{Baldo {et~al.}(2013)Baldo, Robledo, Schuck, \& Vi\~nas}]{bal13}
Baldo, M., Robledo, L.~M., Schuck, P., \& Vi\~nas, X. 2013, Phys. Rev. C, 87,
  064305

\bibitem[{Becker {et~al.}(2014)Becker, Davesne, Meyer, Pastore, \&
  Navarro}]{bec14}
Becker, P., Davesne, D., Meyer, J., Pastore, A., \& Navarro, J. 2014, J. Phys.
  G: Nucl. Part. Phys, 42, 034001

\bibitem[{Bender {et~al.}(2003)Bender, Heenen, \& Reinhard}]{ben03}
Bender, M., Heenen, P.-H., \& Reinhard, P.-G. 2003, Rev. Mod. Phys., 75, 121

\bibitem[{Bogner {et~al.}(2005)Bogner, Schwenk, Furnstahl, \& Nogga}]{bog05}
Bogner, S., Schwenk, A., Furnstahl, R., \& Nogga, A. 2005, Nuclear Physics A,
  763, 59

\bibitem[{Bohr \& Mottelson(1998)}]{bohr1998nuclear}
Bohr, A. \& Mottelson, B.~R. 1998, Nuclear structure, Vol.~1 (World Scientific)

\bibitem[{Bombaci \& Lombardo(1991)}]{bom91}
Bombaci, I. \& Lombardo, U. 1991, Phys. Rev. C, 44, 1892

\bibitem[{Bombaci {et~al.}(2006)Bombaci, Polls, Ramos, Rios, \& Vidana}]{bom06}
Bombaci, I., Polls, A., Ramos, A., Rios, A., \& Vidana, I. 2006, Physics
  Letters B, 632, 638

\bibitem[{Bonche \& Vautherin(1981)}]{bon81}
Bonche, P. \& Vautherin, D. 1981, Nuclear Physics A, 372, 496

\bibitem[{Burgio \& Schulze(2010)}]{bur10}
Burgio, G. \& Schulze, H.-J. 2010, Astronomy \& Astrophysics, 518, A17

\bibitem[{Cao {et~al.}(2006)Cao, Lombardo, Shen, \& Giai}]{cao06}
Cao, L.~G., Lombardo, U., Shen, C.~W., \& Giai, N.~V. 2006, Phys. Rev. C, 73,
  014313

\bibitem[{Carlsson \& Dobaczewski(2010)}]{car10}
Carlsson, B.~G. \& Dobaczewski, J. 2010, Phys. Rev. Lett., 105, 122501

\bibitem[{Carlsson {et~al.}(2008)Carlsson, Dobaczewski, \& Kortelainen}]{car08}
Carlsson, B.~G., Dobaczewski, J., \& Kortelainen, M. 2008, Phys. Rev. C, 78,
  044326

\bibitem[{Chabanat {et~al.}(1997)Chabanat, Bonche, Haensel, Meyer, \&
  Schaeffer}]{cha97}
Chabanat, E., Bonche, P., Haensel, P., Meyer, J., \& Schaeffer, R. 1997, Nucl.
  Phys. A, 627, 710

\bibitem[{Chamel(2010)}]{cha10B}
Chamel, N. 2010, Phys. Rev. C, 82, 061307

\bibitem[{Chamel {et~al.}(2011)Chamel, Fantina, Pearson, \& Goriely}]{cha11}
Chamel, N., Fantina, A., Pearson, J., \& Goriely, S. 2011, Physical Review C,
  84, 062802

\bibitem[{Chamel \& Goriely(2010)}]{cha10}
Chamel, N. \& Goriely, S. 2010, Phys. Rev. C, 82, 045804

\bibitem[{Chamel \& Haensel(2008)}]{cha08}
Chamel, N. \& Haensel, P. 2008, Living Reviews in Relativity, 11

\bibitem[{Chamel {et~al.}(2013)Chamel, Page, \& Reddy}]{cha13}
Chamel, N., Page, D., \& Reddy, S. 2013, Phys. Rev. C, 87, 035803

\bibitem[{Chen {et~al.}(2010)Chen, Ko, Li, \& Xu}]{che10}
Chen, L.-W., Ko, C.~M., Li, B.-A., \& Xu, J. 2010, Phys. Rev. C, 82, 024321

\bibitem[{Danielewicz(2003)}]{dan03}
Danielewicz, P. 2003, Nuclear Physics A, 727, 233

\bibitem[{Danielewicz {et~al.}(2002)Danielewicz, Lacey, \& Lynch}]{dan02}
Danielewicz, P., Lacey, R., \& Lynch, W.~G. 2002, Science, 298, 1592

\bibitem[{Danielewicz \& Lee(2014)}]{dan14}
Danielewicz, P. \& Lee, J. 2014, Nuclear Physics A, 922, 1

\bibitem[{Davesne {et~al.}(2014{\natexlab{a}})Davesne, Meyer, Pastore, \&
  Navarro}]{dav14p}
Davesne, D., Meyer, J., Pastore, A., \& Navarro, J. 2014{\natexlab{a}}, arXiv
  preprint arXiv:1412.1934

\bibitem[{Davesne {et~al.}(2015)Davesne, Navarro, Becker, Jodon, Meyer, \&
  Pastore}]{dav15}
Davesne, D., Navarro, J., Becker, P., {et~al.} 2015, Phys. Rev. C, 91, 064303

\bibitem[{Davesne {et~al.}(2013)Davesne, Pastore, \& Navarro}]{dav13}
Davesne, D., Pastore, A., \& Navarro, J. 2013, J. Phys. G: Nucl. Part. Phys.,
  40, 095104

\bibitem[{Davesne {et~al.}(2014{\natexlab{b}})Davesne, Pastore, \&
  Navarro}]{dav14c}
Davesne, D., Pastore, A., \& Navarro, J. 2014{\natexlab{b}}, J.Phys., G41,
  065104

\bibitem[{Demorest {et~al.}(2010)Demorest, Pennucci, Ransom, Roberts, \&
  Hessels}]{dem10}
Demorest, P., Pennucci, T., Ransom, S., Roberts, M., \& Hessels, J. 2010,
  Nature, 467, 1081

\bibitem[{Douchin \& Haensel(2001)}]{dou11}
Douchin, F. \& Haensel, P. 2001, Astronomy \& Astrophysics, 380, 151

\bibitem[{Douchin {et~al.}(2000)Douchin, Haensel, \& Meyer}]{dou00}
Douchin, F., Haensel, P., \& Meyer, J. 2000, Nuclear Physics A, 665, 419

\bibitem[{Dutra {et~al.}(2012)Dutra, Louren\ifmmode~\mbox{\c{c}}\else
  \c{c}\fi{}o, S\'a~Martins, Delfino, Stone, \& Stevenson}]{dut12}
Dutra, M., Louren\ifmmode~\mbox{\c{c}}\else \c{c}\fi{}o, O., S\'a~Martins,
  J.~S., {et~al.} 2012, Phys. Rev. C, 85, 035201

\bibitem[{Fantoni {et~al.}(2001)Fantoni, Sarsa, \& Schmidt}]{fan01}
Fantoni, S., Sarsa, A., \& Schmidt, K.~E. 2001, Phys. Rev. Lett., 87, 181101

\bibitem[{Fortin {et~al.}(2014)Fortin, Zdunik, Haensel, \& Bejger}]{for14}
Fortin, M., Zdunik, J., Haensel, P., \& Bejger, M. 2014, arXiv preprint
  arXiv:1408.3052

\bibitem[{Fuchs(2006)}]{Fuc06}
Fuchs, C. 2006, Progress in Particle and Nuclear Physics, 56, 1

\bibitem[{Gambacurta {et~al.}(2011)Gambacurta, Li, Col\`o, Lombardo, Van~Giai,
  \& Zuo}]{gam11}
Gambacurta, D., Li, L., Col\`o, G., {et~al.} 2011, Phys. Rev. C, 84, 024301

\bibitem[{Gandolfi {et~al.}(2012)Gandolfi, Carlson, \& Reddy}]{gan12}
Gandolfi, S., Carlson, J., \& Reddy, S. 2012, Phys. Rev. C, 85, 032801

\bibitem[{Goriely {et~al.}(2009)Goriely, Chamel, \& Pearson}]{gor09}
Goriely, S., Chamel, N., \& Pearson, J.~M. 2009, Phys. Rev. Lett., 102, 152503

\bibitem[{Goriely {et~al.}(2010)Goriely, Chamel, \& Pearson}]{Gor10B}
Goriely, S., Chamel, N., \& Pearson, J.~M. 2010, Phys. Rev. C, 82, 035804

\bibitem[{Goriely {et~al.}(2013)Goriely, Chamel, \& Pearson}]{gor13}
Goriely, S., Chamel, N., \& Pearson, J.~M. 2013, Phys. Rev. C, 88, 024308

\bibitem[{Haensel(1995)}]{han95}
Haensel, P. 1995, Space Science Reviews, 74, 427

\bibitem[{Haensel \& Potekhin(2004)}]{hae04}
Haensel, P. \& Potekhin, A.~Y. 2004, Astronomy \& Astrophysics, 428, 191

\bibitem[{Haensel {et~al.}(2007)Haensel, Potekhin, \&
  Yakovlev}]{haensel2007neutron}
Haensel, P., Potekhin, A.~Y., \& Yakovlev, D.~G. 2007, Neutron stars 1:
  Equation of state and structure, Vol. 326 (Springer Science \& Business
  Media)

\bibitem[{Hebeler {et~al.}(2011)Hebeler, Bogner, Furnstahl, Nogga, \&
  Schwenk}]{heb11}
Hebeler, K., Bogner, S.~K., Furnstahl, R.~J., Nogga, A., \& Schwenk, A. 2011,
  Phys. Rev. C, 83, 031301

\bibitem[{Hebeler {et~al.}(2010)Hebeler, Lattimer, Pethick, \& Schwenk}]{heb10}
Hebeler, K., Lattimer, J.~M., Pethick, C.~J., \& Schwenk, A. 2010, Phys. Rev.
  Lett., 105, 161102

\bibitem[{Heiselberg \& Hjorth-Jensen(2000)}]{Hei00}
Heiselberg, H. \& Hjorth-Jensen, M. 2000, Physics Reports, 328, 237

\bibitem[{Iwamoto \& Pethick(1982)}]{iwa82}
Iwamoto, N. \& Pethick, C.~J. 1982, Phys. Rev. D, 25, 313

\bibitem[{Kl\"ahn {et~al.}(2006)Kl\"ahn, Blaschke, Typel, van Dalen, Faessler,
  Fuchs, Gaitanos, Grigorian, Ho, Kolomeitsev, Miller, R\"opke, Tr\"umper,
  Voskresensky, Weber, \& Wolter}]{kla06}
Kl\"ahn, T., Blaschke, D., Typel, S., {et~al.} 2006, Phys. Rev. C, 74, 035802

\bibitem[{Lattimer {et~al.}(1991)Lattimer, Pethick, Prakash, \&
  Haensel}]{lat91}
Lattimer, J.~M., Pethick, C., Prakash, M., \& Haensel, P. 1991, Physical Review
  Letters, 66, 2701

\bibitem[{Lejeune {et~al.}(1986)Lejeune, Grang{\'e}, Martzolff, \&
  Cugnon}]{lej86}
Lejeune, A., Grang{\'e}, P., Martzolff, M., \& Cugnon, J. 1986, Nuclear Physics
  A, 453, 189

\bibitem[{Lesinski {et~al.}(2007)Lesinski, Bender, Bennaceur, Duguet, \&
  Meyer}]{les07}
Lesinski, T., Bender, M., Bennaceur, K., Duguet, T., \& Meyer, J. 2007, Phys.
  Rev. C, 76, 014312

\bibitem[{Lesinski {et~al.}(2006)Lesinski, Bennaceur, Duguet, \& Meyer}]{les06}
Lesinski, T., Bennaceur, K., Duguet, T., \& Meyer, J. 2006, Phys. Rev. C, 74,
  044315

\bibitem[{Li \& Schulze(2008)}]{li08}
Li, Z.~H. \& Schulze, H.-J. 2008, Phys. Rev. C, 78, 028801

\bibitem[{Margueron {et~al.}(2002)Margueron, Navarro, \& Nguyen}]{mar02}
Margueron, J., Navarro, J., \& Nguyen, V.~G. 2002, Phys. Rev. C, 66, 014303

\bibitem[{Pandharipande {et~al.}(1972)Pandharipande, Garde, \&
  Srivastava}]{pan72}
Pandharipande, V., Garde, V., \& Srivastava, J. 1972, Physics Letters B, 38,
  485

\bibitem[{Pandharipande \& Ravenhall(1989)}]{pan89}
Pandharipande, V. \& Ravenhall, D. 1989, in Nuclear matter and heavy ion
  collisions (Springer), 103--132

\bibitem[{Pastore {et~al.}(2015)Pastore, Davesne, \& Navarro}]{PRep}
Pastore, A., Davesne, D., \& Navarro, J. 2015, Physics Reports, 563, 1

\bibitem[{Pastore {et~al.}(2014)Pastore, Martini, Davesne, Navarro, Goriely, \&
  Chamel}]{pasgor14}
Pastore, A., Martini, M., Davesne, D., {et~al.} 2014, Phys. Rev. C, 90, 025804

\bibitem[{Pearson {et~al.}(2012)Pearson, Chamel, Goriely, \& Ducoin}]{pea12}
Pearson, J., Chamel, N., Goriely, S., \& Ducoin, C. 2012, Physical Review C,
  85, 065803

\bibitem[{Raimondi {et~al.}(2011)Raimondi, Carlsson, \& Dobaczewski}]{rai11}
Raimondi, F., Carlsson, B.~G., \& Dobaczewski, J. 2011, Phys. Rev. C, 83,
  054311

\bibitem[{Rotival(2008)}]{rot08}
Rotival, V. 2008, PhD thesis, Universit\'e Paris-Diderot - Paris VII

\bibitem[{Salgado {et~al.}(1994{\natexlab{a}})Salgado, Bonazzola, Gourgoulhon,
  \& Haensel}]{sal94}
Salgado, M., Bonazzola, S., Gourgoulhon, E., \& Haensel, P. 1994{\natexlab{a}},
  Astronomy and Astrophysics, 291, 155

\bibitem[{Salgado {et~al.}(1994{\natexlab{b}})Salgado, Bonazzola, Gourgoulhon,
  \& Haensel}]{sal94b}
Salgado, M., Bonazzola, S., Gourgoulhon, E., \& Haensel, P. 1994{\natexlab{b}},
  Astronomy and Astrophysics Supplement Series, 108, 455

\bibitem[{Sharma {et~al.}(2015)Sharma, Centelles, Vinas, Baldo, \&
  Burgio}]{sha15}
Sharma, B., Centelles, M., Vinas, X., Baldo, M., \& Burgio, G. 2015
  [\eprint[arXiv]{1506.00375}]

\bibitem[{Singh {et~al.}(2013)Singh, Biswal, Bhuyan, Jha, \& Patra}]{Sin13}
Singh, S.~K., Biswal, S., Bhuyan, M., Jha, T., \& Patra, S. 2013
  [\eprint[arXiv]{1312.5840}]

\bibitem[{Skyrme(1959)}]{sky59}
Skyrme, T. H.~R. 1959, Nucl. Phys., 9, 615

\bibitem[{Steiner {et~al.}(2010)Steiner, Lattimer, \& Brown}]{ste10}
Steiner, A.~W., Lattimer, J.~M., \& Brown, E.~F. 2010, The Astrophysical
  Journal, 722, 33

\bibitem[{Stergioulas(2003)}]{ste03}
Stergioulas, N. 2003, Living Rev. Relativity, 6

\bibitem[{Suleimanov {et~al.}(2011)Suleimanov, Poutanen, \& Werner}]{sul11}
Suleimanov, V., Poutanen, J., \& Werner, K. 2011, Astronomy \& Astrophysics,
  527, A139

\bibitem[{Tsang {et~al.}(2009)Tsang, Zhang, Danielewicz, Famiano, Li, Lynch, \&
  Steiner}]{tsa09}
Tsang, M.~B., Zhang, Y., Danielewicz, P., {et~al.} 2009, Phys. Rev. Lett., 102,
  122701

\bibitem[{Typel {et~al.}(2013)Typel, Oertel, \& Klaehn}]{typ13}
Typel, S., Oertel, M., \& Klaehn, T. 2013, arXiv preprint arXiv:1307.5715

\bibitem[{Vautherin \& Brink(1972)}]{vau72}
Vautherin, D. \& Brink, D.~M. 1972, Phys. Rev. C, 5, 626

\bibitem[{Vidana {et~al.}(2002)Vidana, Polls, \& Ramos}]{vid02}
Vidana, I., Polls, A., \& Ramos, A. 2002, Physical Review C, 65, 035804

\bibitem[{Vidaurre {et~al.}(1984)Vidaurre, Navarro, \& Bernabeu}]{vid84}
Vidaurre, A., Navarro, J., \& Bernabeu, J. 1984, Astron.Astrophys., 135, 361

\bibitem[{Yakovlev {et~al.}(2001)Yakovlev, Kaminker, Gnedin, Oleg, \&
  Haensel}]{yak01}
Yakovlev, D., Kaminker, A., Gnedin, Oleg, Y., \& Haensel, P. 2001, Physics
  Reports, 354, 1

\bibitem[{Zhou {et~al.}(2004)Zhou, Burgio, Lombardo, Schulze, \& Zuo}]{zho04}
Zhou, X.~R., Burgio, G.~F., Lombardo, U., Schulze, H.-J., \& Zuo, W. 2004,
  Phys. Rev. C, 69, 018801

\end{thebibliography}
