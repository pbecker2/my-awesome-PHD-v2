%%
%% npw.cls
%%
%% Style similar to the one of the journal Physica Scripta.
%% For authors of Topical volume "XIX NPW, Kazimierz Dolny, 2012"
%%
%%
%% Commands
%% \author{...}
%% \title{...}
%% \pacs{...}
%%
%% acknowledgements
%% \begin{ack} ... \end{ack}
%%
%% abstract
%% \begin{abstract} ... \end{abstract}
%%
%% references
%% \begin{thebibliography}{12} ... \end{thebibliography}
%%

\LoadClass[twocolumn,10pt,a4paper]{article}

\NeedsTeXFormat{LaTeX2e} 
\ProvidesClass{npw}[2012/10/03 Custom class for Topical Volume of
Physica Scripta]

% email
\newcommand\email[1]{\footnote{{e-mail: \it #1}}}
%\renewcommand{\baselinestretch}{1}

%% Acknowledgement
\newenvironment{ack}{%
  \subsection*{Acknowledgement}\small}{\normalsize}

\renewcommand\figurename{\small\it Fig.\rm}
\renewcommand\refname{\section*{References}}

% Based on JEEP.STY

% jeep.sty, December 20, 1991
% G. W. Stewart
% Department of Computer Science
% University of Maryland
% College Park, MD 20784
%
% stewart@thales.umd.edu

% This is a personal customization of the LaTeX style files
% article (4/16/88) and report (5/26/88).  The changes are
% described more fully in the document jeep.tex.  Briefly
% the style
%
%    1. Changes the default page sizes to reflect the fact
%       that most output will be on 8.5 x 11 inch paper.
%
%    2. Adds commands that allow the casual LaTeX to change
%       the formats of chapter, section, and theorem heads.
%
%    3. Adds commands to number equations and theorems within
%       sections or subsections.
%
%    5. Changes the default numbering of enumerate to arabic.
%       Adds a new enumeration environment in which the items
%       are close together.
%
%    6. Adds a command to produce a compact table of contents.
%
%    7. Adds commands to produce symbolic footnote marks and
%       equation labels.
%
%    8. Adds commands by John hobby that make \bigl, \bigr, etc.
%       work properly.
%
%    9. Adds Lamport's alltt environment--a quasi-verbatim
%       environment that recognizes TeX and LaTeX commands.

\setlength{\pdfpagewidth}{210mm}
\setlength{\pdfpageheight}{297mm}

\setlength{\topmargin}{-0.6in}
\setlength{\oddsidemargin}{-10mm}
\setlength{\evensidemargin}{-10mm}
\setlength{\textwidth}{180mm}

\setlength{\topskip}{0em}
\setlength{\headheight}{0em}
\setlength{\footskip}{20pt}

\textheight=62\baselineskip
\advance\textheight by \topskip

\makeatletter

% Define the command \mychapter{<head1>}{<head2>}.  <head1>
% is the text to put in the chapter head if there is a
% chapter number; <head2>, if not.

\def\@chapapp{Chapter}
\def\chapapp{\@chapapp}

\def\mychapter#1#2{
   \def\@chaphead##1{#1}
   \def\@schaphead##1{#2}
}

\def\@makechapterhead#1{ \vspace*{\chaptopsep} { \parindent 0pt \raggedright
 \ifnum \c@secnumdepth >\m@ne \@chaphead{#1} \else \@schaphead{#1} \fi
 \par \nobreak \vskip \chapaftersep } }

\def\@makeschapterhead#1{ \vspace*{\chaptopsep} { \parindent 0pt \raggedright
 \@schaphead{#1}\par
 \nobreak \vskip \chapaftersep } }

% \chaptopsep is the space between the top of the text page and the
% chapter head.  \chapaftersep is the space between the chapter
% head and the text.

\newlength{\chaptopsep}
\setlength{\chaptopsep}{.5in}
\newlength{\chapaftersep}
\setlength{\chapaftersep}{.5in}

% Default chapter headings.

\mychapter{\LARGE \sc \thechapter. #1}{\LARGE \sc #1}

% Define the command \mysection{<sec>}{<style>}{<prefix>}.
% <sec> is the section level--section, subsection, etc.  <style>
% is the style in which the section head is to be set; e.g.,
% \large\bf.  <prefix> is what proceeds the head text, usually
% \the<sec> followed by some putctuation.


\def\@startsection#1#2#3#4#5#6{\if@noskipsec \leavevmode \fi
   \par \@tempskipa #4\relax
   \@afterindenttrue
   \ifdim \@tempskipa <\z@ \@tempskipa -\@tempskipa \@afterindentfalse\fi
   \if@nobreak \everypar{}\else
     \addpenalty{\@secpenalty}\addvspace{\@tempskipa}\fi \@ifstar
     {\@ssect{#3}{#4}{#5}{#1}}{\@dblarg{\@sect{#1}{#2}{#3}{#4}{#5}{#6}}}}


\def\@sect#1#2#3#4#5#6[#7]#8{\ifnum #2>\c@secnumdepth
     \def\@svsec{}\else 
     \refstepcounter{#1}
     \edef\@svsec{\expandafter\relax\csname @pre#1\endcsname}\fi
     \@tempskipa #5\relax
      \ifdim \@tempskipa>\z@ 
        \begingroup \expandafter\relax\csname @#1style\endcsname\relax
          \@hangfrom{\hskip #3\relax\@svsec}{\interlinepenalty \@M #8\par}
        \endgroup
       \csname #1mark\endcsname{#7}\addcontentsline
         {toc}{#1}{\ifnum #2>\c@secnumdepth \else
                      \protect\numberline{\csname the#1\endcsname}\fi
                    #7}\else
        \def\@svsechd{%
            \expandafter\relax\csname @#1style\endcsname\relax
            \hskip #3\@svsec #8\csname #1mark\endcsname
                      {#7}\addcontentsline
                           {toc}{#1}{\ifnum #2>\c@secnumdepth \else
                             \protect\numberline{\csname the#1\endcsname}\fi
                       #7}}\fi
     \@xsect{#5}}


\def\@ssect#1#2#3#4#5{\@tempskipa #3\relax
   \ifdim \@tempskipa>\z@
     \begingroup \expandafter\relax\csname @#4style\endcsname\relax
     \@hangfrom{\hskip #1}{\interlinepenalty \@M #5\par}\endgroup
   \else \def\@svsechd{#4\hskip #1\relax #5}\fi
    \@xsect{#3}}

\def\mysection#1#2#3{
     \expandafter\def\csname @#1style\endcsname{#2}
     \expandafter\def\csname @pre#1\endcsname{#3}
}

% Default section heads.

\mysection{section}{\normalsize\bf}{\thesection.~}
\mysection{subsection}{\normalsize\bf}{\thesubsection.~}
\mysection{subsubsection}{\normalsize\bf}{\thesubsubsection.~}
\mysection{paragraph}{\normalsize\bf}{\theparagraph.}
\mysection{subparagraph}{\normalsize\bf}{\thesubparagraph.}

% Redefine theorem environment macros to put \theoremcounterend
% (default ".") after the "theorem" head.

\def\@begintheorem#1#2{\sl \trivlist
   \item[\hskip \labelsep{\bf #1\ #2\thmcounterend}]}
\def\@opargbegintheorem#1#2#3{\sl \trivlist
      \item[\hskip \labelsep{\bf #1\ #2\ (#3)\thmcounterend}]}
\def\thmcounterend{.}


% Define \numberbysection to number figures, table, equations, and
% theorems within sections.  Similarly for \numberbysubsection.  Any
% theorem-like environment numbered with theorem inherits this
% numbering (however, be sure to define the.  theorem environment
% before invoking \numberbysection or \numberbysubsection).

\def\numberbysection{\renewcommand{\thesection}{\arabic{section}}
                     \renewcommand{\theequation}{\thesection.\arabic{equation}}
                     \@addtoreset{equation}{section}
                     \renewcommand{\thetheorem}{\thesection.\arabic{theorem}}
                     \@addtoreset{theorem}{section}
                     \renewcommand{\thefigure}{\thesection.\arabic{figure}}
                     \@addtoreset{figure}{section}
                     \renewcommand{\thetable}{\thesection.\arabic{table}}
                     \@addtoreset{table}{section}}

\def\numberbysubsection{\renewcommand{\thesection}{\arabic{section}}
              \renewcommand{\thesubsection}{\arabic{subsection}}
              \renewcommand{\theequation}{\thesubsection.\arabic{equation}}
              \@addtoreset{equation}{subsection}
              \renewcommand{\thetheorem}{\thesubsection.\arabic{theorem}}
              \@addtoreset{theorem}{subsection}
              \renewcommand{\thefigure}{\thesubsection.\arabic{figure}}
              \@addtoreset{figure}{subsection}
              \renewcommand{\thetable}{\thesubsection.\arabic{table}}
              \@addtoreset{table}{subsection}}

\@definecounter{theorem}


% Change the labels of enumerate and outline to arabic numbers.

\def\labelenumi{\arabic{enumi}.} 
\def\theenumi{\arabic{enumi}} 
\def\labelenumii{\arabic{enumii}.}
\def\theenumii{\arabic{enumii}}
\def\p@enumii{\theenumi.}
\def\labelenumiii{\arabic{enumiii}.}
\def\theenumiii{\arabic{enumiii}}
\def\p@enumiii{\theenumi.\theenumii.}
\def\labelenumiv{\arabic{enumiv}.}
\def\theenumiv{\arabic{enumiv}} 
\def\p@enumiv{\p@enumiii.\theenumiii}

% Outline is a new list style--the same as enumerate with
% less space between the items.

\def\outline{\ifnum \@enumdepth >3 \@toodeep\else
      \advance\@enumdepth \@ne 
      \edef\@enumctr{enum\romannumeral\the\@enumdepth}\list
      {\csname label\@enumctr\endcsname}{\usecounter
        {\@enumctr}\def\makelabel##1{\hss\llap{##1}}
         \parsep \z@ \itemsep \z@ 
         \ifnum \@enumdepth > 1 \topsep \z@ \fi}\fi}

\let\endoutline =\endlist


% Define \tighttoc to alter table of contents entry so that
% sections are treated like subsections, subsubsections, etc.

\def\tighttoc{\def\l@section{\@dottedtocline{1}{0em}{1.4em}}}

% Define \symbolnote{<num>}{<text>} to produce a note with footnote
% mark <num> (less than 10).  The commands \symbolmark{<num>}
% and \symboltext{<num>}{<text>} are analogues of
% \footnotemark[<num>] and \footnotetext[<num>]{<text>}.

\def\symbolnote#1#2{\let\@thefootn=\thefootnote%
\let\@thempfootn=\thempfootnote%
\renewcommand{\thefootnote}{\fnsymbol{footnote}}%
\renewcommand{\thempfootnote}{\fnsymbol{mpfootnote}}%
\footnotemark[#1]%
\footnotetext[#1]{#2}%
\let\thefootnote=\@thefootn\let\thempfootnote=\@thempfootn
}

\def\symbolmark#1{\let\@thefootn=\thefootnote%
\let\@thempfootn=\thempfootnote%
\renewcommand{\thefootnote}{\fnsymbol{footnote}}%
\renewcommand{\thempfootnote}{\fnsymbol{mpfootnote}}%
\footnotemark[#1]%
\let\thefootnote=\@thefootn\let\thempfootnote=\@thempfootn
}

\def\symboltext#1#2{\let\@thefootn=\thefootnote%
\let\@thempfootn=\thempfootnote%
\renewcommand{\thefootnote}{\fnsymbol{footnote}}%
\renewcommand{\thempfootnote}{\fnsymbol{mpfootnote}}%
\footnotetext[#1]{#2}%
\let\thefootnote=\@thefootn\let\thempfootnote=\@thempfootn
}


% Define symboleqn environment to make a displayed equation with
% a symbolic equation label.

\newenvironment{symboleqn}[1]{
   \def\@savesymbol{#1}$$}{\eqno \@savesymbol $$\global\@ignoretrue}

% Fix Plain's \bigl, \Bigl, etc. macros so that they try to scale with
% LaTeX size changes.  This uses the fact that \@setsize sets \ht\strutbox
% to be 70% of the normal unstretched baselineskip.  This is
% the work of John Hobby.

\def\big#1{{\hbox{$\left#1\vcenter to1.428\ht\strutbox{}\right.\n@space$}}}
\def\Big#1{{\hbox{$\left#1\vcenter to2.142\ht\strutbox{}\right.\n@space$}}}
\def\bigg#1{{\hbox{$\left#1\vcenter to2.857\ht\strutbox{}\right.\n@space$}}}
\def\Bigg#1{{\hbox{$\left#1\vcenter to3.571\ht\strutbox{}\right.\n@space$}}}

% Add a new environment literatim which is like the verbatim 
% environment except that \, {, and } have their usual meanings.  
% Thus, other commands and environments can appear within an `literatim'
% environment.  This is essentially Lamport's alltt environment
% (the two sentences above were lifted from his description)
% with the following additions.  The old special commands $, &, ^,
% and _, are \$, \&, \^, and \_.  The command \@ will produce
% a verbatim backslash.  A nonoptional argument to the the
% environment specifies the comment character (preceeded by
% a backslash).  The character thus taken can be produced by \`.
% If the argument is null, there is no comment character.
% (Nb., space remains an active character in equations, which 
% can have some unusual side effects).

\def\docspecials{\do\ \do\$\do\&%
  \do\#\do\^\do\^^K\do\_\do\^^A\do\%\do\~}
{\catcode`\/=0\catcode`\\=12/xdef/@bsl{\}}
\def\literatim#1{\trivlist \item[]\if@minipage\else\vskip\parskip\fi
\leftskip\@totalleftmargin\rightskip\z@
\parindent\z@\parfillskip\@flushglue\parskip\z@
\def\@nll{}\def\@arg{#1}
\ifx\@nll\@arg\def\@newcc{}\else\def\@newcc{\catcode`#1=14}\fi
\let\@=\@bsl
\let\@dollar=$
\let\@amper=&
\let\circumflex=\^
\@tempswafalse \def\par{\if@tempswa\hbox{}\fi\@tempswatrue\@@par}
\obeylines \tt \catcode``=13 \@noligs \let\do\@makeother \docspecials
\let\$=\@dollar
\let\&=\@amper
\let\_=\sb
\let\^=\sp
\frenchspacing\@vobeyspaces\@newcc}

\let\endliteratim=\endtrivlist

\if@titlepage
  \renewenvironment{abstract}{%
    \titlepage
      \null\vfil
      \@beginparpenalty\@lowpenalty
      \begin{center}%
        \bfseries \abstractname
        \@endparpenalty\@M
      \end{center}}%
     {\par\vfil\null\endtitlepage}
\else
  \renewenvironment{abstract}{%
      \if@twocolumn
        \subsection*{\abstractname}%
      \footnotesize
      \else
        \begin{center}%
          {\section*{\abstractname}\vspace{-.5em}\vspace{\z@}}%
        \end{center}%
        \quotation
      \fi}
    {\if@twocolumn\else\endquotation\par\fi}
\fi

%% thebibliography

\renewenvironment{thebibliography}[1]
{  \renewcommand{\baselinestretch}{0.7}%
  \subsubsection*{\refname}%
  \@mkboth{\MakeUppercase\refname}{\MakeUppercase\refname}%
  \list{\@biblabel{\@arabic\c@enumiv}}%
  {\settowidth\labelwidth{\@biblabel{#1}}%
    \leftmargin\labelwidth
    \advance\leftmargin\labelsep
    \@openbib@code
    \usecounter{enumiv}%
    \let\p@enumiv\@empty
    \renewcommand\theenumiv{\@arabic\c@enumiv}}%
  \sloppy
  \clubpenalty4000
  \@clubpenalty \clubpenalty
  \widowpenalty4000%
  \sfcode`\.\@m}
{\def\@noitemerr
  {\@latex@warning{Empty `thebibliography' environment}}%
  \endlist}



%% pacs
\def\@pacs{No PACS!}
\newcommand{\pacs}[1]{\gdef\@pacs{#1}}


%% maketitle

\def\@maketitle{%
   \newpage
   \null
%  \vskip 2em%
   \begin{center}%
   \let \footnote \thanks
     {\LARGE\bf \@title \par}%
     \vskip 1.5em%
     {\large
       \lineskip .5em%
       \begin{tabular}[t]{c}%
         \@author
       \end{tabular}\par}%
     \vskip 1em%
     {\large {\sc Pacs Ref}:~\@pacs}%
     \par
     \vskip 1em%
     {\large \@date}%
   \end{center}%
   \par
   \vskip 1em}

\makeatother
