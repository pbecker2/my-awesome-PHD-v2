\begin{thebibliography}{43}
\expandafter\ifx\csname natexlab\endcsname\relax\def\natexlab#1{#1}\fi
\expandafter\ifx\csname bibnamefont\endcsname\relax
  \def\bibnamefont#1{#1}\fi
\expandafter\ifx\csname bibfnamefont\endcsname\relax
  \def\bibfnamefont#1{#1}\fi
\expandafter\ifx\csname citenamefont\endcsname\relax
  \def\citenamefont#1{#1}\fi
\expandafter\ifx\csname url\endcsname\relax
  \def\url#1{\texttt{#1}}\fi
\expandafter\ifx\csname urlprefix\endcsname\relax\def\urlprefix{URL }\fi
\providecommand{\bibinfo}[2]{#2}
\providecommand{\eprint}[2][]{\url{#2}}

\bibitem[{\citenamefont{Bender et~al.}(2003)\citenamefont{Bender, Heenen, and
  Reinhard}}]{ben03}
\bibinfo{author}{\bibfnamefont{M.}~\bibnamefont{Bender}},
  \bibinfo{author}{\bibfnamefont{P.-H.} \bibnamefont{Heenen}},
  \bibnamefont{and} \bibinfo{author}{\bibfnamefont{P.-G.}
  \bibnamefont{Reinhard}}, \bibinfo{journal}{Rev. Mod. Phys.}
  \textbf{\bibinfo{volume}{75}}, \bibinfo{pages}{121} (\bibinfo{year}{2003}).

\bibitem[{\citenamefont{Todd-Rutel and Piekarewicz}(2005)}]{tod05}
\bibinfo{author}{\bibfnamefont{B.}~\bibnamefont{Todd-Rutel}} \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Piekarewicz}},
  \bibinfo{journal}{Phys. Rev. Lett.} \textbf{\bibinfo{volume}{95}},
  \bibinfo{pages}{122501} (\bibinfo{year}{2005}).

\bibitem[{\citenamefont{Goriely et~al.}(2009)\citenamefont{Goriely, Chamel, and
  Pearson}}]{gor09}
\bibinfo{author}{\bibfnamefont{S.}~\bibnamefont{Goriely}},
  \bibinfo{author}{\bibfnamefont{N.}~\bibnamefont{Chamel}}, \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{J.~M.} \bibnamefont{Pearson}},
  \bibinfo{journal}{Phys. Rev. Lett.} \textbf{\bibinfo{volume}{102}},
  \bibinfo{pages}{152503} (\bibinfo{year}{2009}).

\bibitem[{\citenamefont{Erler et~al.}(2013)\citenamefont{Erler, Horowitz,
  Nazarewicz, Rafalski, and Reinhard}}]{erl13}
\bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Erler}},
  \bibinfo{author}{\bibfnamefont{C.}~\bibnamefont{Horowitz}},
  \bibinfo{author}{\bibfnamefont{W.}~\bibnamefont{Nazarewicz}},
  \bibinfo{author}{\bibfnamefont{M.}~\bibnamefont{Rafalski}}, \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{P.-G.} \bibnamefont{Reinhard}},
  \bibinfo{journal}{Phys.Rev.} \textbf{\bibinfo{volume}{C87}},
  \bibinfo{pages}{044320} (\bibinfo{year}{2013}).

\bibitem[{\citenamefont{Skyrme}(1959)}]{sky59}
\bibinfo{author}{\bibfnamefont{T.~H.~R.} \bibnamefont{Skyrme}},
  \bibinfo{journal}{Nucl. Phys.} \textbf{\bibinfo{volume}{9}},
  \bibinfo{pages}{615} (\bibinfo{year}{1959}).

\bibitem[{\citenamefont{Perli\ifmmode~\acute{n}\else \'{n}\fi{}ska
  et~al.}(2004)\citenamefont{Perli\ifmmode~\acute{n}\else \'{n}\fi{}ska,
  Rohozi\ifmmode~\acute{n}\else \'{n}\fi{}ski, Dobaczewski, and
  Nazarewicz}}]{per04}
\bibinfo{author}{\bibfnamefont{E.}~\bibnamefont{Perli\ifmmode~\acute{n}\else
  \'{n}\fi{}ska}}, \bibinfo{author}{\bibfnamefont{S.~G.}
  \bibnamefont{Rohozi\ifmmode~\acute{n}\else \'{n}\fi{}ski}},
  \bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Dobaczewski}},
  \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{W.}~\bibnamefont{Nazarewicz}},
  \bibinfo{journal}{Phys. Rev. C} \textbf{\bibinfo{volume}{69}},
  \bibinfo{pages}{014316} (\bibinfo{year}{2004}).

\bibitem[{\citenamefont{Vautherin and Brink}(1972)}]{vau72}
\bibinfo{author}{\bibfnamefont{D.}~\bibnamefont{Vautherin}} \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{D.~M.} \bibnamefont{Brink}},
  \bibinfo{journal}{Phys. Rev. C} \textbf{\bibinfo{volume}{5}},
  \bibinfo{pages}{626} (\bibinfo{year}{1972}).

\bibitem[{\citenamefont{Sadoudi et~al.}(2013)\citenamefont{Sadoudi, Duguet,
  Meyer, and Bender}}]{sad13}
\bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Sadoudi}},
  \bibinfo{author}{\bibfnamefont{T.}~\bibnamefont{Duguet}},
  \bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Meyer}}, \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{M.}~\bibnamefont{Bender}},
  \bibinfo{journal}{Phys. Rev. C} \textbf{\bibinfo{volume}{88}},
  \bibinfo{pages}{064326} (\bibinfo{year}{2013}).

\bibitem[{\citenamefont{Dobaczewski et~al.}(2014)\citenamefont{Dobaczewski,
  Nazarewicz, and Reinhard}}]{dob14}
\bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Dobaczewski}},
  \bibinfo{author}{\bibfnamefont{W.}~\bibnamefont{Nazarewicz}},
  \bibnamefont{and} \bibinfo{author}{\bibfnamefont{P.}~\bibnamefont{Reinhard}},
  \bibinfo{journal}{J. Phys. G: Nucl. Part. Phys.}
  \textbf{\bibinfo{volume}{41}}, \bibinfo{pages}{074001}
  (\bibinfo{year}{2014}).

\bibitem[{\citenamefont{Bertsch et~al.}(2007)\citenamefont{Bertsch, Dean, and
  Nazarewicz}}]{ber07}
\bibinfo{author}{\bibfnamefont{G.~F.} \bibnamefont{Bertsch}},
  \bibinfo{author}{\bibfnamefont{D.~J.} \bibnamefont{Dean}}, \bibnamefont{and}
  \bibinfo{author}{\bibnamefont{Nazarewicz}}, \bibinfo{journal}{SciDAC Review}
  \textbf{\bibinfo{volume}{6}} (\bibinfo{year}{2007}).

\bibitem[{\citenamefont{Furnstahl}(2011)}]{fur11}
\bibinfo{author}{\bibfnamefont{R.}~\bibnamefont{Furnstahl}},
  \bibinfo{journal}{Nucl. Phys. News} \textbf{\bibinfo{volume}{21}},
  \bibinfo{pages}{18} (\bibinfo{year}{2011}).

\bibitem[{\citenamefont{Kortelainen et~al.}(2010)\citenamefont{Kortelainen,
  Lesinski, Mor\'e, Nazarewicz, Sarich, Schunck, Stoitsov, and Wild}}]{kor10}
\bibinfo{author}{\bibfnamefont{M.}~\bibnamefont{Kortelainen}},
  \bibinfo{author}{\bibfnamefont{T.}~\bibnamefont{Lesinski}},
  \bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Mor\'e}},
  \bibinfo{author}{\bibfnamefont{W.}~\bibnamefont{Nazarewicz}},
  \bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Sarich}},
  \bibinfo{author}{\bibfnamefont{N.}~\bibnamefont{Schunck}},
  \bibinfo{author}{\bibfnamefont{M.~V.} \bibnamefont{Stoitsov}},
  \bibnamefont{and} \bibinfo{author}{\bibfnamefont{S.}~\bibnamefont{Wild}},
  \bibinfo{journal}{Phys. Rev. C} \textbf{\bibinfo{volume}{82}},
  \bibinfo{pages}{024313} (\bibinfo{year}{2010}).

\bibitem[{\citenamefont{Kortelainen et~al.}(2014)\citenamefont{Kortelainen,
  McDonnell, Nazarewicz, Olsen, Reinhard, Sarich, Schunck, Wild, Davesne, Erler
  et~al.}}]{kor13}
\bibinfo{author}{\bibfnamefont{M.}~\bibnamefont{Kortelainen}},
  \bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{McDonnell}},
  \bibinfo{author}{\bibfnamefont{W.}~\bibnamefont{Nazarewicz}},
  \bibinfo{author}{\bibfnamefont{E.}~\bibnamefont{Olsen}},
  \bibinfo{author}{\bibfnamefont{P.-G.} \bibnamefont{Reinhard}},
  \bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Sarich}},
  \bibinfo{author}{\bibfnamefont{N.}~\bibnamefont{Schunck}},
  \bibinfo{author}{\bibfnamefont{S.~M.} \bibnamefont{Wild}},
  \bibinfo{author}{\bibfnamefont{D.}~\bibnamefont{Davesne}},
  \bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Erler}},
  \bibnamefont{et~al.}, \bibinfo{journal}{Phys. Rev. C}
  \textbf{\bibinfo{volume}{89}}, \bibinfo{pages}{054314}
  (\bibinfo{year}{2014}).

\bibitem[{\citenamefont{Yannouleas and Landman}(2007)}]{yan07}
\bibinfo{author}{\bibfnamefont{C.}~\bibnamefont{Yannouleas}} \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{U.}~\bibnamefont{Landman}},
  \bibinfo{journal}{Rep. Progr. Phys.} \textbf{\bibinfo{volume}{70}},
  \bibinfo{pages}{2067} (\bibinfo{year}{2007}).

\bibitem[{\citenamefont{Lesinski et~al.}(2007)\citenamefont{Lesinski, Bender,
  Bennaceur, Duguet, and Meyer}}]{les07}
\bibinfo{author}{\bibfnamefont{T.}~\bibnamefont{Lesinski}},
  \bibinfo{author}{\bibfnamefont{M.}~\bibnamefont{Bender}},
  \bibinfo{author}{\bibfnamefont{K.}~\bibnamefont{Bennaceur}},
  \bibinfo{author}{\bibfnamefont{T.}~\bibnamefont{Duguet}}, \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Meyer}},
  \bibinfo{journal}{Phys. Rev. C} \textbf{\bibinfo{volume}{76}},
  \bibinfo{pages}{014312} (\bibinfo{year}{2007}).

\bibitem[{\citenamefont{Carlsson et~al.}(2008)\citenamefont{Carlsson,
  Dobaczewski, and Kortelainen}}]{car08}
\bibinfo{author}{\bibfnamefont{B.~G.} \bibnamefont{Carlsson}},
  \bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Dobaczewski}},
  \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{M.}~\bibnamefont{Kortelainen}},
  \bibinfo{journal}{Phys. Rev. C} \textbf{\bibinfo{volume}{78}},
  \bibinfo{pages}{044326} (\bibinfo{year}{2008}).

\bibitem[{\citenamefont{Carlsson and Dobaczewski}(2010)}]{car10}
\bibinfo{author}{\bibfnamefont{B.~G.} \bibnamefont{Carlsson}} \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Dobaczewski}},
  \bibinfo{journal}{Phys. Rev. Lett.} \textbf{\bibinfo{volume}{105}},
  \bibinfo{pages}{122501} (\bibinfo{year}{2010}).

\bibitem[{\citenamefont{Lesinski et~al.}(2006)\citenamefont{Lesinski,
  Bennaceur, Duguet, and Meyer}}]{les06}
\bibinfo{author}{\bibfnamefont{T.}~\bibnamefont{Lesinski}},
  \bibinfo{author}{\bibfnamefont{K.}~\bibnamefont{Bennaceur}},
  \bibinfo{author}{\bibfnamefont{T.}~\bibnamefont{Duguet}}, \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Meyer}},
  \bibinfo{journal}{Phys. Rev. C} \textbf{\bibinfo{volume}{74}},
  \bibinfo{pages}{044315} (\bibinfo{year}{2006}).

\bibitem[{\citenamefont{Hellemans et~al.}(2013)\citenamefont{Hellemans,
  Pastore, Duguet, Bennaceur, Davesne, Meyer, Bender, and Heenen}}]{hel13}
\bibinfo{author}{\bibfnamefont{V.}~\bibnamefont{Hellemans}},
  \bibinfo{author}{\bibfnamefont{A.}~\bibnamefont{Pastore}},
  \bibinfo{author}{\bibfnamefont{T.}~\bibnamefont{Duguet}},
  \bibinfo{author}{\bibfnamefont{K.}~\bibnamefont{Bennaceur}},
  \bibinfo{author}{\bibfnamefont{D.}~\bibnamefont{Davesne}},
  \bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Meyer}},
  \bibinfo{author}{\bibfnamefont{M.}~\bibnamefont{Bender}}, \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{P.-H.} \bibnamefont{Heenen}},
  \bibinfo{journal}{Phys. Rev. C} \textbf{\bibinfo{volume}{88}},
  \bibinfo{pages}{064323} (\bibinfo{year}{2013}).

\bibitem[{\citenamefont{Pastore et~al.}(2015)\citenamefont{Pastore, Davesne,
  and Navarro}}]{pas15}
\bibinfo{author}{\bibfnamefont{A.}~\bibnamefont{Pastore}},
  \bibinfo{author}{\bibfnamefont{D.}~\bibnamefont{Davesne}}, \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Navarro}},
  \bibinfo{journal}{Phys. Rep.} \textbf{\bibinfo{volume}{563}},
  \bibinfo{pages}{1} (\bibinfo{year}{2015}).

\bibitem[{\citenamefont{Raimondi et~al.}(2011)\citenamefont{Raimondi, Carlsson,
  and Dobaczewski}}]{rai11}
\bibinfo{author}{\bibfnamefont{F.}~\bibnamefont{Raimondi}},
  \bibinfo{author}{\bibfnamefont{B.~G.} \bibnamefont{Carlsson}},
  \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Dobaczewski}},
  \bibinfo{journal}{Phys. Rev. C} \textbf{\bibinfo{volume}{83}},
  \bibinfo{pages}{054311} (\bibinfo{year}{2011}).

\bibitem[{\citenamefont{Becker et~al.}(2014)\citenamefont{Becker, Davesne,
  Meyer, Pastore, and Navarro}}]{bec14}
\bibinfo{author}{\bibfnamefont{P.}~\bibnamefont{Becker}},
  \bibinfo{author}{\bibfnamefont{D.}~\bibnamefont{Davesne}},
  \bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Meyer}},
  \bibinfo{author}{\bibfnamefont{A.}~\bibnamefont{Pastore}}, \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Navarro}},
  \bibinfo{journal}{J. Phys. G: Nucl. Part. Phys}
  \textbf{\bibinfo{volume}{42}}, \bibinfo{pages}{034001}
  (\bibinfo{year}{2014}).

\bibitem[{\citenamefont{Davesne et~al.}(2015)\citenamefont{Davesne, Meyer,
  Pastore, and Navarro}}]{dav14k}
\bibinfo{author}{\bibfnamefont{D.}~\bibnamefont{Davesne}},
  \bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Meyer}},
  \bibinfo{author}{\bibfnamefont{A.}~\bibnamefont{Pastore}}, \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Navarro}},
  \bibinfo{journal}{Phys. Scripta, in press}  (\bibinfo{year}{2015}),
  \eprint{arXiv:1412.1934}.

\bibitem[{\citenamefont{Carlsson et~al.}(2010)\citenamefont{Carlsson,
  Dobaczewski, Toivanen, and Vesel\'y}}]{car10b}
\bibinfo{author}{\bibfnamefont{B.~G.} \bibnamefont{Carlsson}},
  \bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Dobaczewski}},
  \bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Toivanen}}, \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{P.}~\bibnamefont{Vesel\'y}},
  \bibinfo{journal}{Comp. Phys. Comm.} \textbf{\bibinfo{volume}{181}},
  \bibinfo{pages}{1641} (\bibinfo{year}{2010}).

\bibitem[{\citenamefont{Pastore et~al.}(2013)\citenamefont{Pastore, Davesne,
  Bennaceur, Meyer, and Hellemans}}]{pas13}
\bibinfo{author}{\bibfnamefont{A.}~\bibnamefont{Pastore}},
  \bibinfo{author}{\bibfnamefont{D.}~\bibnamefont{Davesne}},
  \bibinfo{author}{\bibfnamefont{K.}~\bibnamefont{Bennaceur}},
  \bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Meyer}}, \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{V.}~\bibnamefont{Hellemans}},
  \bibinfo{journal}{Phys. Scripta} \textbf{\bibinfo{volume}{T154}},
  \bibinfo{pages}{014014} (\bibinfo{year}{2013}).

\bibitem[{\citenamefont{Chabanat et~al.}(1997)\citenamefont{Chabanat, Bonche,
  Haensel, Meyer, and Schaeffer}}]{cha97}
\bibinfo{author}{\bibfnamefont{E.}~\bibnamefont{Chabanat}},
  \bibinfo{author}{\bibfnamefont{P.}~\bibnamefont{Bonche}},
  \bibinfo{author}{\bibfnamefont{P.}~\bibnamefont{Haensel}},
  \bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Meyer}}, \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{R.}~\bibnamefont{Schaeffer}},
  \bibinfo{journal}{Nucl. Phys. A} \textbf{\bibinfo{volume}{627}},
  \bibinfo{pages}{710} (\bibinfo{year}{1997}).

\bibitem[{\citenamefont{Wiringa et~al.}(1995)\citenamefont{Wiringa, Stoks, and
  Schiavilla}}]{wir95}
\bibinfo{author}{\bibfnamefont{R.}~\bibnamefont{Wiringa}},
  \bibinfo{author}{\bibfnamefont{V.}~\bibnamefont{Stoks}}, \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{R.}~\bibnamefont{Schiavilla}},
  \bibinfo{journal}{Phys. Rev. C} \textbf{\bibinfo{volume}{51}},
  \bibinfo{pages}{38} (\bibinfo{year}{1995}).

\bibitem[{\citenamefont{Baldo et~al.}(1997)\citenamefont{Baldo, Bombaci, and
  Burgio}}]{bal97}
\bibinfo{author}{\bibfnamefont{M.}~\bibnamefont{Baldo}},
  \bibinfo{author}{\bibfnamefont{I.}~\bibnamefont{Bombaci}}, \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{G.}~\bibnamefont{Burgio}},
  \bibinfo{journal}{Astron. Astrophys.} \textbf{\bibinfo{volume}{328}},
  \bibinfo{pages}{274} (\bibinfo{year}{1997}).

\bibitem[{\citenamefont{Som\`a and Bozek}(2008)}]{som08}
\bibinfo{author}{\bibfnamefont{V.}~\bibnamefont{Som\`a}} \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{P.}~\bibnamefont{Bozek}},
  \bibinfo{journal}{Phys. Rev. C} \textbf{\bibinfo{volume}{78}},
  \bibinfo{pages}{054003} (\bibinfo{year}{2008}).

\bibitem[{\citenamefont{Bombaci et~al.}(2005)\citenamefont{Bombaci, Fabrocini,
  Polls, and Vidana}}]{Bom05}
\bibinfo{author}{\bibfnamefont{I.}~\bibnamefont{Bombaci}},
  \bibinfo{author}{\bibfnamefont{A.}~\bibnamefont{Fabrocini}},
  \bibinfo{author}{\bibfnamefont{A.}~\bibnamefont{Polls}}, \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{I.}~\bibnamefont{Vidana}},
  \bibinfo{journal}{Physics Letters B} \textbf{\bibinfo{volume}{609}},
  \bibinfo{pages}{232 } (\bibinfo{year}{2005}).

\bibitem[{\citenamefont{Vida\~na et~al.}(2009)\citenamefont{Vida\~na,
  Provid\^encia, Polls, and Rios}}]{vid09}
\bibinfo{author}{\bibfnamefont{I.}~\bibnamefont{Vida\~na}},
  \bibinfo{author}{\bibfnamefont{C.}~\bibnamefont{Provid\^encia}},
  \bibinfo{author}{\bibfnamefont{A.}~\bibnamefont{Polls}}, \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{A.}~\bibnamefont{Rios}},
  \bibinfo{journal}{Phys. Rev. C} \textbf{\bibinfo{volume}{80}},
  \bibinfo{pages}{045806} (\bibinfo{year}{2009}).

\bibitem[{\citenamefont{Tews et~al.}(2013)\citenamefont{Tews, Kr\"uger,
  Hebeler, and Schwenk}}]{tew13}
\bibinfo{author}{\bibfnamefont{I.}~\bibnamefont{Tews}},
  \bibinfo{author}{\bibfnamefont{T.}~\bibnamefont{Kr\"uger}},
  \bibinfo{author}{\bibfnamefont{K.}~\bibnamefont{Hebeler}}, \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{A.}~\bibnamefont{Schwenk}},
  \bibinfo{journal}{Phys. Rev. Lett.} \textbf{\bibinfo{volume}{110}},
  \bibinfo{pages}{032504} (\bibinfo{year}{2013}).

\bibitem[{\citenamefont{Davesne et~al.}(2014)\citenamefont{Davesne, Pastore,
  and Navarro}}]{dav14c}
\bibinfo{author}{\bibfnamefont{D.}~\bibnamefont{Davesne}},
  \bibinfo{author}{\bibfnamefont{A.}~\bibnamefont{Pastore}}, \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Navarro}},
  \bibinfo{journal}{J. Phys. G: Nucl. Part. Phys.}
  \textbf{\bibinfo{volume}{41}}, \bibinfo{pages}{065104}
  (\bibinfo{year}{2014}).

\bibitem[{\citenamefont{Davesne et~al.}(2013)\citenamefont{Davesne, Pastore,
  and Navarro}}]{dav13}
\bibinfo{author}{\bibfnamefont{D.}~\bibnamefont{Davesne}},
  \bibinfo{author}{\bibfnamefont{A.}~\bibnamefont{Pastore}}, \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Navarro}},
  \bibinfo{journal}{J. Phys. G: Nucl. Part. Phys.}
  \textbf{\bibinfo{volume}{40}}, \bibinfo{pages}{095104}
  (\bibinfo{year}{2013}).

\bibitem[{\citenamefont{Hebeler et~al.}(2011)\citenamefont{Hebeler, Bogner,
  Furnstahl, Nogga, and Schwenk}}]{heb11}
\bibinfo{author}{\bibfnamefont{K.}~\bibnamefont{Hebeler}},
  \bibinfo{author}{\bibfnamefont{S.~K.} \bibnamefont{Bogner}},
  \bibinfo{author}{\bibfnamefont{R.~J.} \bibnamefont{Furnstahl}},
  \bibinfo{author}{\bibfnamefont{A.}~\bibnamefont{Nogga}}, \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{A.}~\bibnamefont{Schwenk}},
  \bibinfo{journal}{Phys. Rev. C} \textbf{\bibinfo{volume}{83}},
  \bibinfo{pages}{031301} (\bibinfo{year}{2011}).

\bibitem[{\citenamefont{Bogner et~al.}(2005)\citenamefont{Bogner, Schwenk,
  Furnstahl, and Nogga}}]{bog05}
\bibinfo{author}{\bibfnamefont{S.}~\bibnamefont{Bogner}},
  \bibinfo{author}{\bibfnamefont{A.}~\bibnamefont{Schwenk}},
  \bibinfo{author}{\bibfnamefont{R.}~\bibnamefont{Furnstahl}},
  \bibnamefont{and} \bibinfo{author}{\bibfnamefont{A.}~\bibnamefont{Nogga}},
  \bibinfo{journal}{Nuclear Physics A} \textbf{\bibinfo{volume}{763}},
  \bibinfo{pages}{59} (\bibinfo{year}{2005}).

\bibitem[{\citenamefont{Rotival}(2008)}]{rot08}
\bibinfo{author}{\bibfnamefont{V.}~\bibnamefont{Rotival}}, Ph.D. thesis,
  \bibinfo{school}{Universit\'e Paris-Diderot - Paris VII}
  (\bibinfo{year}{2008}).

\bibitem[{\citenamefont{Goriely et~al.}(2003)\citenamefont{Goriely, Samyn,
  Bender, and Pearson}}]{gor03}
\bibinfo{author}{\bibfnamefont{S.}~\bibnamefont{Goriely}},
  \bibinfo{author}{\bibfnamefont{M.}~\bibnamefont{Samyn}},
  \bibinfo{author}{\bibfnamefont{M.}~\bibnamefont{Bender}}, \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{J.~M.} \bibnamefont{Pearson}},
  \bibinfo{journal}{Phys. Rev. C} \textbf{\bibinfo{volume}{68}},
  \bibinfo{pages}{054325} (\bibinfo{year}{2003}).

\bibitem[{\citenamefont{Washiyama et~al.}(2012)\citenamefont{Washiyama,
  Bennaceur, Avez, Bender, Heenen, and Hellemans}}]{kou12}
\bibinfo{author}{\bibfnamefont{K.}~\bibnamefont{Washiyama}},
  \bibinfo{author}{\bibfnamefont{K.}~\bibnamefont{Bennaceur}},
  \bibinfo{author}{\bibfnamefont{B.}~\bibnamefont{Avez}},
  \bibinfo{author}{\bibfnamefont{M.}~\bibnamefont{Bender}},
  \bibinfo{author}{\bibfnamefont{P.-H.} \bibnamefont{Heenen}},
  \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{V.}~\bibnamefont{Hellemans}},
  \bibinfo{journal}{Phys. Rev. C} \textbf{\bibinfo{volume}{86}},
  \bibinfo{pages}{054309} (\bibinfo{year}{2012}).

\bibitem[{\citenamefont{Kl\"upfel et~al.}(2009)\citenamefont{Kl\"upfel,
  Reinhard, B\"urvenich, and Maruhn}}]{klu09}
\bibinfo{author}{\bibfnamefont{P.}~\bibnamefont{Kl\"upfel}},
  \bibinfo{author}{\bibfnamefont{P.-G.} \bibnamefont{Reinhard}},
  \bibinfo{author}{\bibfnamefont{T.~J.} \bibnamefont{B\"urvenich}},
  \bibnamefont{and} \bibinfo{author}{\bibfnamefont{J.~A.}
  \bibnamefont{Maruhn}}, \bibinfo{journal}{Phys. Rev. C}
  \textbf{\bibinfo{volume}{79}}, \bibinfo{pages}{034310}
  (\bibinfo{year}{2009}).

\bibitem[{\citenamefont{Hebeler}(2014)}]{hebeler}
\bibinfo{author}{\bibfnamefont{K.}~\bibnamefont{Hebeler}},
  \bibinfo{journal}{private communication}  (\bibinfo{year}{2014}).

\bibitem[{\citenamefont{Baldo}(2014)}]{bal14}
\bibinfo{author}{\bibfnamefont{M.}~\bibnamefont{Baldo}},
  \bibinfo{journal}{private communication}  (\bibinfo{year}{2014}).

\bibitem[{\citenamefont{Pastore et~al.}(2014)\citenamefont{Pastore, Davesne,
  and Navarro}}]{prep}
\bibinfo{author}{\bibfnamefont{A.}~\bibnamefont{Pastore}},
  \bibinfo{author}{\bibfnamefont{D.}~\bibnamefont{Davesne}}, \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Navarro}},
  \bibinfo{journal}{Physics Reports} \textbf{\bibinfo{volume}{563}},
  \bibinfo{pages}{1 } (\bibinfo{year}{2014}).

\end{thebibliography}
