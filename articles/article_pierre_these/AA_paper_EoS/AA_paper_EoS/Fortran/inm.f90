module inm

  use param
  implicit none

  double precision :: L0f,L2f,L4f,L6f
  double precision :: L0g,L2g,L4g,L6g
  double precision :: L0fp,L2fp,L4fp,L6fp
  double precision :: L0gp,L2gp,L4gp,L6gp

  !
  !!
  !
contains
  !=========================================================
  !  EFFECTIVE MASS SKYRME SNM
  !=========================================================

  subroutine mass_skyrmeSNM(rho,mSsurm,mstarN)
    implicit none

    double precision :: rho,mstarN,kf,mSsurm
    double precision :: a1,a2,a3,ratio


    a1=(3*t1 +t2*(5.+4*x2))/8.d0
    a2=(3*t14+t24*(5+4*x24))/4.d0*(tpi2*rho )**(2.d0/3.0)
    a3=9*(3*t16+t26*(5+4*x26))/5.d0*(tpi2*rho)**(4.d0/3.0)

    ratio=rho*(a1+a2+a3)/(2*hb2m)

    mstarN=1.d0/(1.d0+ratio)
    mSsurm=mstarN
    mstarN=mstarN*nucleonmass

  end subroutine mass_skyrmeSNM

  !
  !!
  !

  subroutine landau
    !================================================
    ! Routine to calculate Landau parameters in SNM
    !================================================

    implicit none
    !=========== Landau parameters
    integer :: ikf
    double precision :: f0,f1,f2,kf,rho
    double precision :: g0,g1,g2,L6fp,f0p,f1p,f2p
    double precision :: g0p,g1p,g2p,N0
    double precision :: f3,f3p,g3,g3p
    double precision :: mSsurm,xxx,hbarc,hbarc2
    double precision :: mstarN


    hbarc=197.326968d0
    hbarc2=hbarc*hbarc


    L0f=3*t0
    L0g= -t0*(1.-2*x0)
    L0fp=-t0*(1.+2*x0)
    L0gp=-t0

    L2f=3*t1+(5.+4*x2)*t2 
    L2g=-(1.-2.*x1)*t1+(1.+2*x2)*t2
    L2fp=-(1.+2.*x1)*t1+(1.+2*x2)*t2
    L2gp=-t1+t2

    L4f=3*t14+(5.+4*x24)*t24
    L4g=-(1.-2*x14)*t14+(1.+2*x24)*t24
    L4fp=-(1.+2*x14)*t14+(1.+2*x24)*t24
    L4gp=-t14+t24

    L6f=3*t16+(5.+4*x26)*t26
    L6g=-(1.-2*x16)*t16+(1.+2*x26)*t26
    L6fp=-(1.+2*x16)*t16+(1.+2*x26)*t26
    L6gp=-t16+t26

    open(unit=11,file='LandauSNM.dat')
    do ikf=1,250

       kf=ikf*0.01d0
       rho=(kf**3/tpi2)
       call  mass_skyrmeSNM(rho,mSsurm,mstarN)

       f0=0.25d0*L0f+kf**2*L2f/8+kf**4*L4f/6+kf**6*L6f+(alpha+1.)*(alpha+2.)*t3*rho**alpha/16.d0
       g0=0.25d0*L0g+kf**2*L2g/8+kf**4*L4g/6+kf**6*L6g-(1.-2*x3)*t3*(rho**alpha)/24.d0
       f0p=0.25d0*L0fp+kf**2*L2fp/8+kf**4*L4fp/6+kf**6*L6fp-(1.+2*x3)*t3*rho**alpha/24.d0
       g0p=0.25d0*L0gp+kf**2*L2gp/8+kf**4*L4gp/6+kf**6*L6gp-t3*rho**alpha/24.d0

       f1=-kf**2*L2f/8.-kf**4*L4f/4.-9*kf**6*L6f/5.0d0
       g1=-kf**2*L2g/8.-kf**4*L4g/4.-9*kf**6*L6g/5.0d0
       f1p=-kf**2*L2fp/8.-kf**4*L4fp/4.-9*kf**6*L6fp/5.0d0
       g1p=-kf**2*L2gp/8.-kf**4*L4gp/4.-9*kf**6*L6gp/5.0d0

       f2=kf**4*L4f/12+kf**6*L6f
       g2=kf**4*L4g/12+kf**6*L6g
       f2p=kf**4*L4fp/12+kf**6*L6fp
       g2p=kf**4*L4gp/12+kf**6*L6gp

       f3=-kf**6*L6f/5.
       g3=-kf**6*L6g/5.
       f3p=-kf**6*L6fp/5.
       g3p=-kf**6*L6gp/5.

       N0=(2*mstarN)/((pi**2)*hbarc2)*kf
       !             1   2      3      4       5    6      7       8    9     10    11     12     13    14
       if(rho.le.0.35)write(11,888)rho,mstarN,f0*N0,g0*N0,f0p*N0,g0p*N0,f1*N0,g1*N0,f1p*N0,g1p*N0,f2*N0,g2*N0,f2p*N0,g2p*N0 &
                                ! 15   16    17    18
            &                   ,f3*N0,g3*N0,f3p*N0,g3p*N0
    enddo

888 format(18(1x,E12.5))
    close(11)

  end subroutine landau

  !
  !!
  !
  subroutine partialwaveSNM
    !=========================================================
    ! Routine to calculate partial wave decomposition of N3LO
    ! in SNM
    !=========================================================
    implicit none

    integer :: ikf
    double precision :: E1S0,E3S1,kf,rho
    double precision :: E1P0,E3P0,E3P1,E3P2
    double precision :: E3D1,E3D2,E3D3,E1D2
    double precision :: E1F3,E3F2,E3F3,E3F4

    !======= open files

    open(unit=50,file='PartialwaveS.dat')
    open(unit=51,file='PartialwaveP.dat')
    open(unit=52,file='PartialwaveD.dat')
    open(unit=53,file='PartialwaveCoupled.dat')


    do ikf=1,ikfmax

       kf=ikf*dkf
       rho=(kf**3/tpi2)
       ! ----------- S
       E1S0= (3.d0/16)*t0*(1.-x0)*rho+t3/32*(1.-x3)*rho**(1.d0+alpha) &
            +(9.d0/160)*t1*(1.-x1)*rho*kf**2 &
            +(9.d0/280)*t14*(1.-x14)*rho*kf**4 &
            +(1.d0/10)*t16*(1.-x16)*rho*kf**6 

       E3S1=(3.d0/16)*t0*(1.+x0)*rho+(1.d0/32)*t3*(1.+x3)*rho**(alpha+1.d0) &
            +(9.d0/160)*t1*(1.+x1)*rho*kf**2 &
            +(9.d0/280)*t14*(1.+x14)*rho*kf**4 &
            +(1.d0/10)*t16*(1.+x16)*rho*kf**6 

       ! ---------- P
       E1P0=3.d0/160*t2*(1.d0-x2)*rho*kf**2 &
            +(9.d0/560)*t24*(1.d0-x24)*rho*kf**4 &
            +(3.d0/50)*t26*(1.d0-x26)*rho*kf**6 

       E3P0=3.d0/160*t2*(1.d0+x2)*rho*kf**2 &
            +(9.d0/560)*t24*(1.d0+x24)*rho*kf**4 &
            +(3.d0/50)*t26*(1.d0+x26)*rho*kf**6 

       E3P1=9.d0/160*t2*(1.d0+x2)*rho*kf**2 &
            +(27.d0/560)*t24*(1.d0+x24)*rho*kf**4 &
            +(9.d0/50)*t26*(1.d0+x26)*rho*kf**6 

       E3P2=3.d0/32*t2*(1.d0+x2)*rho*kf**2 &
            +(9.d0/112)*t24*(1.d0+x24)*rho*kf**4 &
            +(3.d0/10)*t26*(1.d0+x26)*rho*kf**6 

       !----- D
       E1D2=(9.d0/560.)*t14*(1.-x14)*rho*kf**4 &
            +(1.d0/10.)*t16*(1.-x16)*rho*kf**6

       E3D1=(9./2800)*t14*(1+x14)*rho*kf**4 &
            +(1./50)*t16*(1+x16)*rho*kf**6

       E3D2=(3.d0/560.)*t14*(1.+x14)*rho*kf**4 &
            +(1.d0/30.)*t16*(1.+x16)*rho*kf**6

       E3D3=(3./400)*t14*(1+x14)*rho*kf**4 &
            +(7./150)*t16*(1+x16)*rho*kf**6

       !----- F
       E1F3=t26/150. *(1.-x26)*rho*kf**6
       E3F2=t26/70. *(1.+x26)*rho*kf**6
       E3F3=t26/50. *(1.+x26)*rho*kf**6
       E3F4=9.*t26/350. *(1.+x26)*rho*kf**6

       !======== Adding tensor

       E3P0=E3P0-(3.d0/80)*to*rho*kf**2-(9./140)*to4*rho*kf**4-(17./750)*to6*rho*kf**6
       E3P1=E3P1+(9.d0/160)*to*rho*kf**2+(27./280)*to4*rho*kf**4+(17./500)*to6*rho*kf**6
       E3P2=E3P2-(3.d0/160)*to*rho*kf**2-(9./280)*to4*rho*kf**4-(17./1500)*to6*rho*kf**6

       E3D1=E3D1-(9.d0/1000)*te4*rho*kf**4-(7.d0/1500)*te6*rho*kf**6
       E3D2=E3D2+(3.d0/200)*te4*rho*kf**4+(7.d0/900)*te6*rho*kf**6
       E3D3=E3D3-(3.d0/500)*te4*rho*kf**4-(7.d0/2250)*te6*rho*kf**6

       E3F2=E3F2-3*to6/875. *rho*kf**6
       E3F3=E3F3+3*to6/500. *rho*kf**6
       E3F4=E3F4-9*to6/3500. *rho*kf**6

       ! ===== Adding spin orbit

       E3P0=E3P0+W0/40.*rho*kf**2
       E3P1=E3P1+3*W0/80.*rho*kf**2
       E3P2=E3P2-W0/16.*rho*kf**2

       write(50,500)kf,rho,E1S0,E3S1
       write(51,501)kf,rho,E1P0,E3P0,E3P1,E3P2
       write(52,501)kf,rho,E1D2,E3D1,E3D2,E3D3
       write(53,500)kf,rho,E3S1+E3D1,E3P2+E3F2


500    format(4(1x,E14.6))
501    format(6(1x,E14.6))

    enddo
    !=================

    close(50)
    close(51)
    close(52)
    close(53)

    return
  end subroutine partialwaveSNM

  !
  !!
  !
  subroutine STchannels
    implicit none

    integer :: ikf 
    double precision :: kf,rho,tau,EE(0:1,-1:1),Etest,mstarN
    double precision :: mSsurm


    open(unit=10,file='SkyrmeST.dat')
    open(unit=70,file='effectivemass.dat')


    do ikf=1,ikfmax

       kf=ikf*dkf
       rho=(kf**3)/tpi2
       tau=3.d0*rho*kf**2/5.d0

       EE(0,0)=t2*(1.-x2)*tau/32.+9*t24*(1.d0-x24)*rho*kf**4/560.d0 &
            +t26*(1.-x26)*rho*kf**6/15.d0

       EE(0,1)=3.*t0*(1.-x0)*rho/16 &
            +t3*(1.-x3)*rho**(alpha+1.)/32+3*t1*(1.-x1)*tau/32.d0 &
            +27.d0*t14*(1.d0-x14)*rho*kf**4/560.d0 &
            +t16*(1.d0-x16)*rho*kf**6/5.d0


       EE(1,0)=3*t0*(1+x0)*rho/16 &
            +t3*(1+x3)*rho**(alpha+1.)/32+3*t1*(1+x1)*tau/32.d0 &
            +27.d0*t14*(1.d0+x14)*rho*kf**4/560.d0 &
            +t16*(1.d0+x16)*rho*kf**6/5.d0


       EE(1,1)=9.d0*t2*(1.d0+x2)*tau/32.d0+81.d0*t24*(1.d0+x24)*rho*kf**4/560 &
            +3.d0*t26*(1.+x26)*rho*kf**6/5.d0

       Etest=(3.d0/5.d0)*hb2m*kf**2+EE(0,0)+EE(0,1)+EE(1,0)+EE(1,1)


       !                        1   2   3      4         5     6        7
       if(rho.le.1)write(10,99)kf,rho,EE(0,0),EE(0,1),EE(1,0),EE(1,1),Etest

       call mass_skyrmeSNM(rho,mSsurm,mstarN)
       write(70,*)kf,rho,mSsurm

    enddo

    close(70)
99  format(7(1x,F12.7))
    close(10)
  end subroutine STchannels


  !
  !!
  !

  subroutine PlotEoS_Asym
    !=========================================
    ! Equation of state in Asymmetric Matter
    !
    ! (non-polarized)
    !
    !=========================================
    implicit none
    integer :: ikf,iY
    integer :: i02,i08
    integer, parameter :: iymax=50
    double precision :: kf,rho,Y
    double precision :: Etot(ikfmax,0:iymax)


    open(unit=90,file='EOS_Asymmetric.dat')
    open(unit=91,file='Surface_Eos_ANM.dat')

    do iy=0,iymax
       Y=(1.d0*iy)/(1.d0*iymax)

       do ikf=1,ikfmax
          kf=ikf*dkf
          rho=(kf**3)/tpi2 ! density total

          Etot(ikf,iy)=EoSANM(rho,Y)
          write(91,*)rho,Y,Etot(ikf,iy)
       enddo
       write(91,*)
    enddo

    do ikf=1,ikfmax

       kf=ikf*dkf
       rho=(kf**3)/tpi2 ! density total
       i02=Nint(0.2d0*iymax)! selecting the index for Y=0.2
       i08=Nint(0.8d0*iymax)! selecting the index for Y=0.8

       write(90,777)rho,Etot(ikf,0),Etot(ikf,i02),Etot(ikf,i08),Etot(ikf,iymax)
    enddo

777 format(5(1x,F13.5))
    close(90)
    close(91)
    return
  end subroutine PlotEoS_Asym
  !
  !!
  !

  function EoSANM(rho,Y)
    implicit none
    double precision,intent(in) :: rho,Y

    double precision :: kf,t12,t37,t35
    double precision :: rhon,kfn,rhop,kfp
    double precision :: Ekin,Epot0,Epot2,Epot4,Epot6
    double precision :: EoSANM,C20,C21,C40,C41,C60,C61

    t12=0.5d0
    t37=3.d0/7.d0
    t35=3.d0/5.d0

    C20=(3*t1+(5.+4*x2)*t2)
    C21=(-(2*x1+1.)*t1+(2*x2+1.)*t2)

    C40=(3*t14+(5.+4*x24)*t24)
    C41=(-(2*x14+1.)*t14+(2*x24+1.)*t24)

    C60=(3*t16+(5.+4*x26)*t26)
    C61=(-(2*x16+1.)*t16+(2*x26+1.)*t26)

    rhon= t12*rho*(1.d0+Y)
    kfn=(npi2*rhon)**(1.d0/3.d0)
    rhop=rho-rhon
    kfp=(npi2*rhop)**(1.d0/3.d0)

    ! kinetic term
    Ekin=t35*hb2m*(rhon*kfn**2+rhop*kfp**2)

    ! 0th order
    Epot0=(3./8.)*(t0+t3*rho**alpha/6.)*rho**2 &
         +(-(2.*x0+1.)*t0-(2.*x3+1.)*t3*rho**alpha/6.)*(rhon-rhop)**2/8.d0

    ! 2nd order
    Epot2=(3.d0/80.d0)*C20*(rhon*kfn**2+rhop*kfp**2)*rho &
         +(3.d0/80.d0)*C21*(rhon*kfn**2-rhop*kfp**2)*(rhon-rhop) 

    ! 4th order
    Epot4=(1.d0/32.d0)*C40*(t37*(rhon*kfn**4+rhop*kfp**4)*rho &
         +t35*(rhon*kfn**2+rhop*kfp**2)**2) &
         +(1.d0/32.d0)*C41*(t37*(rhon-rhop)*(rhon*kfn**4-rhop*kfp**4) &
         +t35*(rhon*kfn**2-rhop*kfp**2)**2)

    ! 6th order
    Epot6=(1.d0/16.d0)*C60*((rhon*kfn**6+rhop*kfp**6)*rho/3.d0 &
         +9.*(rhon*kfn**2+rhop*kfp**2)*(rhon*kfn**4+rhop*kfp**4)/(5)) &
         +(1.d0/16.d0)*C61*((rhon-rhop)*(rhon*kfn**6-rhop*kfp**6)/(3.d0) &
         +9.*(rhon*kfn**4-rhop*kfp**4)*(rhon*kfn**2-rhop*kfp**2)/(5.d0)) 

    EoSANM=(Ekin+Epot0+Epot2+Epot4+Epot6)/rho

    return
  end function EoSANM
  !
  !!
  !

  subroutine EoS_pola
    !=================================================
    ! Equation of state in Polarized symmetric Matter
    !
    ! (no change in isospin)
    !
    !================================================
    implicit none
    integer :: ikf,iY
    integer :: i02,i08
    integer, parameter :: iymax=50
    double precision :: kf,rho,Y,t12,t37,t35
    double precision :: rhou,kfu,rhod,kfd
    double precision :: C20,C21,C40,C41,C60,C61
    double precision :: Ekin,Epot0,Epot2,Epot4,Epot6
    double precision :: Etot(ikfmax,0:iymax)

    t12=0.5d0
    t37=3.d0/7.d0
    t35=3.d0/5.d0

    C20=(3.*t1+(5.+4*x2)*t2)
    C21=((2*x1-1.)*t1+(2*x2+1.)*t2)

    C40=(3.*t14+(5.+4*x24)*t24)
    C41=((2*x14-1.)*t14+(2*x24+1.)*t24)

    C60=(3.*t16+(5.+4*x26)*t26)
    C61=((2*x16-1.)*t16+(2*x26+1.)*t26)

    open(unit=90,file='EOS_Polarized.dat')
    open(unit=91,file='Surface_Eos_PolSNM.dat')

    do iy=0,iymax
       Y=(1.d0*iy)/(1.d0*iymax)

       do ikf=1,ikfmax

          kf=ikf*dkf
          rho=(kf**3)/tpi2 ! density total

          rhou= t12*rho*(1.d0+Y)
          kfu=(npi2*rhou)**(1.d0/3.d0)
          rhod=rho-rhou
          kfd=(npi2*rhod)**(1.d0/3.d0)

          ! kinetic term
          Ekin=t35*hb2m*(rhou*kfu**2+rhod*kfd**2)/rho

          ! 0th order
          Epot0=3.*(t0+t3*rho**alpha/6.)*rho/8. &
               +((2.*x0-1.)*t0+(2.*x3-1.)*t3*rho**alpha/6.)*(rhou-rhod)**2/(8*rho)

          ! 2nd order
          Epot2=(3.d0/80.d0)*C20*(rhou*kfu**2+rhod*kfd**2) &
               +(3.d0/80.d0)*C21*(rhou*kfu**2-rhod*kfd**2)*(rhou-rhod)/rho 

          ! 4th order
          Epot4=(1.d0/32.d0)*C40*(t37*(rhou*kfu**4+rhod*kfd**4) &
               +t35*(rhou*kfu**2+rhod*kfd**2)**2/rho) &
               +(1.d0/32.d0)*C41*(t37*(rhou-rhod)/rho*(rhou*kfu**4-rhod*kfd**4) &
               +t35*(rhou*kfu**2-rhod*kfd**2)**2/rho)

          ! 6th order
          Epot6=(1.d0/16.d0)*C60*((rhou*kfu**6+rhod*kfd**6)/3.d0 &
               +9.*(rhou*kfu**2+rhod*kfd**2)*(rhou*kfu**4+rhod*kfd**4)/(5*rho)) &
               +(1.d0/16.d0)*C61*((rhou-rhod)*(rhou*kfu**6-rhod*kfd**6)/(3.d0*rho) &
               +9.*(rhou*kfu**4-rhod*kfd**4)*(rhou*kfu**2-rhod*kfd**2)/(5*rho)) 

          Etot(ikf,iy)=Ekin+Epot0+Epot2+Epot4+Epot6
          write(91,*)rho,Y,Etot(ikf,iy)
       enddo
       write(91,*)
    enddo

    do ikf=1,ikfmax

       kf=ikf*dkf
       rho=(kf**3)/tpi2 ! density total
       i02=Nint(0.2d0*iymax)! selecting the index for Y=0.2
       i08=Nint(0.8d0*iymax)! selecting the index for Y=0.8

       write(90,777)rho,Etot(ikf,0),Etot(ikf,i02),Etot(ikf,i08),Etot(ikf,iymax)
    enddo

777 format(5(1x,F13.5))
    close(90)
    close(91)
    return
  end subroutine EoS_pola

  !
  !!
  !
  subroutine asym_param
    implicit none

    integer :: ikf
    double precision :: kf,rho,asym(1:3)
    double precision :: term0,C20,C21,C22,C23
    double precision :: C40,C41,C42,C43
    double precision :: C60,C61,C62,C63


    open(unit=20,file='Asymmetry_energy.dat')


    C20=(3.*t1+(5.+4*x2)*t2)
    C21=(-(2*x1+1.)*t1+(2*x2+1.)*t2)

    !
    C40=(3.*t14+(5.+4*x24)*t24)
    C41=(-(2*x14+1.)*t14+(2*x24+1.)*t24)
!
    !
    C60=(3.*t16+(5.+4*x26)*t26)
    C61=(-(2*x16+1.)*t16+(2*x26+1.)*t26)


    do ikf=1,ikfmax*2
       kf=ikf*dkf
       rho=(kf**3)/tpi2 ! density total
       !
       ! Isospin symmetry energy
       term0=-(2.d0*x0+1.d0)*t0-(2.d0*x3+1.d0)*t3*rho**alpha/6.d0

       asym(1)=hb2m*kf**2/3.d0 &
            +(term0)*rho/8.d0+(1.d0/16.d0)*(C20/3.d0+C21)*rho*kf**2 &
            +(1.d0/32.d0)*(4.d0*C40/3.d0+8.d0*C41/3.d0)*rho*kf**4 &
            +(1.d0/32.d0)*(48.d0*C60/5.d0+16.d0*C61)*rho*kf**6
       !

       write(20,555)rho,asym(1)!,asym(2),asym(3)

555    format(2(1x,F15.6))
    enddo
    close(20)
    return
  end subroutine asym_param

  !
  !!
  !

  subroutine speedsound

    implicit none

    integer ikf
    double precision kf,rho,vsound
    double precision C20,C21,C40,C41,C60,C61

    C20=(3.*t1+(5.+4*x2)*t2)
    C21=(-(2*x1+1.)*t1+(2*x2+1.)*t2)

    !
    C40=(3.*t14+(5.+4*x24)*t24)
    C41=(-(2*x14+1.)*t14+(2*x24+1.)*t24)
    !
    !
    C60=(3.*t16+(5.+4*x26)*t26)
    C61=(-(2*x16+1.)*t16+(2*x26+1.)*t26)

    open(unit=20,file='vsoundPNM.dat')
    do ikf=1,3*ikfmax
       kf=ikf*dkf
       rho=(kf**3)/npi2 ! density total

       vsound=(2.d0/3.d0)*hb2m*kf**2+(1.d0/4.d0)*(2*(1.-x0)*t0+((1.-x3)/6.)*(1.+alpha)*(2.+alpha)*t3*rho**alpha)*rho &
            +(1.d0/3.d0) *((1.-x1 )*t1 +3*(1.+x2 )*t2 )*rho*kf**2 &
            +(1.d0/2.d0) *((1.-x14)*t14+3*(1.+x24)*t24)*rho*kf**4 &
            +(16.d0/5.d0)*((1.-x16)*t16+3*(1.+x26)*t26)*rho*kf**6 
       if(vsound.lt.0.)vsound=0.
       write(20,*)rho,sqrt(vsound/ nucleonmass)
    enddo

    close(20)
    return
  end subroutine speedsound


  subroutine mass_asym

    implicit none


    integer iY,irho,i
    double precision rho,rhon,rhop
    double precision kfn,kfp,t12,kf
    double precision fnn,fpn,fnp,fpp
    double precision mn,mp,Y,mSsurm,mstarN
    double precision DeltaT,t53,t23,t73,t43
    double precision C20,C22
    double precision C40,C42
    double precision C60,C62,m(2)

    character*80 fname 

    t12=1.d0/2.d0
    t53=5.d0/3.d0
    t43=4.d0/3.d0
    t73=7.d0/3.d0
    t23=2.d0/3.d0

    C20=(3.*t1+(5.+4*x2)*t2)
    C22=-((2*x1+1.)*t1-(2*x2+1.)*t2)
    !
    C40=(3.*t14+(5.+4*x24)*t24)
    C42=-((2*x14+1.)*t14-(2*x24+1.)*t24)
    !
    C60=(3.*t16+(5.+4*x26)*t26)
    C62=-((2*x16+1.)*t16-(2*x26+1.)*t26)


    do iY=0,10,1

       Y=iY*0.1d0

       write(fname,'("Effective_mass_",F3.1,".dat")')Y
       open(unit=30,file=fname)

       do irho=1,irhomax
          rho=irho*drho
          kf=(tpi2*rho)**(1.d0/3.d0)

          rhon= t12*rho*(1.d0+Y)
          kfn=(npi2*rhon)**(1.d0/3.d0)
          rhop=rho-rhon
          kfp=(npi2*rhop)**(1.d0/3.d0)

          do i=1,2
             DeltaT=Y
             if(i.eq.2)DeltaT=-DeltaT
             m(i)= C22*rho*DeltaT/16.d0 + (C20*rho)/16.d0 &
                  +1.d0/32.d0*C40*kF**2*((1.d0-DeltaT)**t53+2*(1.d0 +DeltaT)**t23+(1.d0+DeltaT)**t53)*rho &
                  +3.d0/160.d0* C60*kF**4*(5.d0*(1.d0-DeltaT)**t73+14.d0*(1.d0-DeltaT)**t53*(1.d0 + DeltaT)**t23 &
                  +10.d0*(1.d0+DeltaT)**t43 +19.d0*(1.d0+DeltaT)**t73)*rho &
                  +1.d0/32.d0*C42*kF**2*(2.d0*DeltaT*(1.d0+DeltaT)**t23-(1.d0-DeltaT)**t53&
                  +(1.d0+DeltaT)**t53)*rho&
                  +3.d0/160.d0*C62*kF**4*(10.d0*DeltaT*(1.d0 + DeltaT)**t43 -5*(1.d0-DeltaT)**t73 &
                  -14.d0*(1.d0-DeltaT)**t53*(1.d0+ DeltaT)**t23+19.d0*(1.d0+DeltaT)**t73)*rho

          enddo

          ! neutrons
          mn=m(1)+hb2m
          mp=m(2)+hb2m

          write(30,*)rho,mn,mp
       enddo
       close(30)
    enddo



    rho=0.165d0
    write(fname,'("Effective_mass_saturation.dat")')
    open(unit=30,file=fname)

    do iY=0,100,1

       Y=iY*0.01d0

       kf=(tpi2*rho)**(1.d0/3.d0)
       do i=1,2
          DeltaT=Y
          if(i.eq.2)DeltaT=-DeltaT
          m(i)= C22*rho*DeltaT/16.d0 + (C20*rho)/16.d0 &
               +1.d0/32.d0*C40*kF**2*((1-DeltaT)**t53+2*(1+DeltaT)**t23+(1+DeltaT)**t53)*rho &
               +3.d0/160.d0* C60*kF**4*(5.d0*(1.d0-DeltaT)**t73+14.d0*(1.d0-DeltaT)**t53*(1.d0 + DeltaT)**t23 &
               +10.d0*(1.d0+DeltaT)**t43 +19.d0*(1.d0+DeltaT)**t73)*rho &
               +1.d0/32.d0*C42*kF**2*(2.d0*DeltaT*(1.d0+DeltaT)**t23-(1-DeltaT)**t53&
               +(1+DeltaT)**t53)*rho&
               +3.d0/160.d0*C62*kF**4*(10.d0*DeltaT*(1+DeltaT)**t43 -5*(1-DeltaT)**t73 &
               -14.d0*(1-DeltaT)**t53*(1+ DeltaT)**t23+19.d0*(1+DeltaT)**t73)*rho

       enddo



       mn=m(1)+hb2m! 
       mp=m(2)+hb2m



       write(30,*)Y,hb2m/mn,hb2m/mp
    enddo
    close(30)



  end subroutine mass_asym

end module inm
