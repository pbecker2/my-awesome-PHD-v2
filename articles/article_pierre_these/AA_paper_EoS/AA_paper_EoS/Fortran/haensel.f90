module haensel

  use param
  implicit none

  double precision :: rhoH(1000),EH(1000),PH(1000)
  integer :: iH

contains

  subroutine readHaesnel

    implicit none

    double precision xr,xe,xp

    rhoH=0.D0
    EH=0.D0
    iH=0

    open(unit=20,file='haensel.store')

100 continue

    read(20,*,END=101)xr,xe,xp
    ih=ih+1
    rhoH(ih)=xr
    EH(ih)=xe
    PH(ih)=xp
    go to 100

101 continue

    if(ih.eq.0)then
       write(*,*)'I did not find the file of Haensle!!!!!'
       stop
    endif

    close(20)
  end subroutine readHaesnel
  !
  !!
  !
  subroutine InterpolateDH(x,EoS)
    implicit none

    integer :: i1,i2,j
    double precision :: df,EoS,EE
    double precision :: x

    df=999999999999.D0

    do j=2,ih-1

       if(abs(x-rhoH(j)).lt.df)then
          df=abs(x-rhoH(j))
          if(x.gt.rhoH(j))then
             i1=j-1
             i2=j
          else
             i1=j
             i2=j+1
          endif

       endif

    enddo


    ! linear interpolation

    EE=EH(i1)+(EH(i2)-EH(i1))*(x-rhoh(i1))/(rhoH(i2)-rhoH(i1))

    EoS=EE*x

    return


  end subroutine InterpolateDH


  subroutine InterpolateDHpress(x,Press)
    implicit none

    integer :: i1,i2,j
    double precision :: df,EoS,PP
    double precision :: x,Press
    double precision :: a,b,c,d
    df=999999999999.D0

    do j=2,ih-1

       if(abs(x-rhoH(j)).lt.df)then
          df=abs(x-rhoH(j))
          if(x.gt.rhoH(j))then
             i1=j-1
             i2=j
          else
             i1=j
             i2=j+1
          endif

       endif


    enddo


    ! linear interpolation
    a=-0.0172718d0
    b=4.45662 
    c=-61.5792   
    d=875.768

    Press=a+b*x+c*x**2+d*x**3

    !Press=PH(i1)+(PH(i2)-PH(i1))*(x-rhoh(i1))/(rhoH(i2)-rhoH(i1))

    return

  end subroutine InterpolateDHpress


end module haensel
