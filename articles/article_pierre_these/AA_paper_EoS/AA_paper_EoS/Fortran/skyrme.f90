



program skyrme

  use param 
  use inm
  use star
  use haensel


  implicit none

  integer          :: ivlyb
  integer          :: irho
  double precision :: dens,YY,Yp,E,P



  call readHaesnel


  ivlyb=4
  !=1 SLy4
  !=2 N2LO
  !=3 N3LO

  !==========================================
  ! setting the parameters of the interaction
  call force(ivlyb)

  !==============================
  ! Landau parameters in SNM
  Write(*,*)'Call Landau SNM....'
  call landau

  !==================================
  ! Partial wave decomposition in SNM
  write(*,*)'Calling Partial waves JLST....'
  call partialwaveSNM

  !==============================
  ! ST channels in SNM
  write(*,*)'Calling ST decomposition ...'
  call STchannels


  !
  do irho=1,1200
     open(unit=20,file='Proton_fraction.dat')
     open(unit=21,file='EoS_electrons.dat')
     open(unit=22,file='Eos_betaEq.dat')

     dens=0.001d0*irho
     call protonfraction(dens,YY,Yp)
     call EoSelectron(dens,Yp,E)
     call Pelectron(dens,Yp,P)
     write(20,*)dens,Yp,YY
     write(21,888)dens,E,P,press(dens)


    write(22,888)dens,EoSANM(dens,YY)
  enddo
888 format(4(1x,E15.8))

  close(20)
  close(21)
  close(22)

  !========= Equation of state

  write(*,*)'Calling EoS....'
  call asym_param

  call PlotEoS_Asym

  call EoS_pola

  call EoSgeneral

  call speedsound

  call mass_asym

  !==== TOV equations ===========

  write(*,*)'Call TOV equations NS'
  call  neutron_star


  call test
  !
  !!
  !
contains

  subroutine EoSgeneral
    implicit none

    integer :: irho,ikf,ng
    double precision :: kf,rho,tau
    double precision :: EOS_snm(irhomax),EOS_pnm(irhomax)
    double precision :: EOS_polpnm(irhomax), EOS_polsnm(irhomax)

    open(unit=40,file='EoS_matters.dat')

    do irho=1,irhomax
       rho=irho*drho

       !==== SNM 
       ng=4 ! degrees of freedom
       kf=(6.d0*pi**2*rho/ng)**(1.d0/3.d0)

       EOS_snm(irho)=(3.d0/5.d0)*hb2m*kf**2+(3.d0/8.d0)*(t0+(t3/6)*rho**alpha)*rho &
            +(3.d0/80.d0)*(3*t1+(5.+4.*x2)*t2)*rho*kf**2 &
            +(9.d0/280.d0)*(3*t14+(5+4*x24)*t24)*rho*kf**4 &
            +(2.d0/15.d0)*(3*t16+(5+4*x26)*t26)*rho*kf**6 

       !============ Pol SNM =============
       ng=2 ! degrees of freedom
       kf=(6.d0*pi**2*rho/ng)**(1.d0/3.d0)

       EOS_polsnm(irho)=(3.d0/5.d0)*hb2m*kf**2+(1.d0/4.d0)*((1.+x0)*t0+((1.+x3)/6.)*t3*rho**alpha)*rho &
            +(3.d0/40.d0)*((1.+x1)*t1+3.*(1.+x2)*t2)*rho*kf**2 &
            +(9.d0/140.d0)*((1.+x14)*t14+3.*(1.+x24)*t24)*rho*kf**4 &
            +(4.d0/15.d0)*((1.+x16)*t16+3.*(1.+x26)*t26)*rho*kf**6 
       !
       !============ PNM =============
       !
       ng=2 ! degrees of freedom
       kf=(6.d0*pi**2*rho/ng)**(1.d0/3.d0)

       EOS_pnm(irho)=(3.d0/5.d0)*hb2m*kf**2+(1.d0/4.d0)*((1.-x0)*t0+((1.-x3)/6.)*t3*rho**alpha)*rho &
            +(3.d0/40.d0)*((1.-x1)*t1+3*(1.+x2)*t2)*rho*kf**2 &
            +(9.d0/140.d0)*((1.-x14)*t14+3*(1.+x24)*t24)*rho*kf**4 &
            +(4.d0/15.d0)*((1.-x16)*t16+3*(1.+x26)*t26)*rho*kf**6 

       !===== Polarized PNM
       ng=1 ! degrees of freedom
       kf=(6.d0*pi**2*rho/ng)**(1.d0/3.d0)

       EOS_polpnm(irho)=(3.d0/5.d0)*hb2m*kf**2+(3.d0/10.d0)*(1.+x2)*t2*rho*kf**2 &
            +(9.d0/35.d0)*(1.+x24)*t24*rho*kf**4 &
            +(16.d0/15.d0)*(1.+x26)*t26*rho*kf**6 
       !                         1       2          3               4               5
!        if(rho.le.1.) &
     ! write(40,98)rho,EOS_snm(irho),(EOS_pnm(irho)*rho+939.5*rho)*10.D0**(12)*1.783,EOS_polsnm(irho),EOS_polpnm(irho)

    write(40,98)rho,EOS_snm(irho),EOS_pnm(irho),EOS_polsnm(irho),EOS_polpnm(irho)
    Enddo
98  format(5(1x,E14.6))


    close(40)
    return
  end subroutine EoSgeneral
end program skyrme

