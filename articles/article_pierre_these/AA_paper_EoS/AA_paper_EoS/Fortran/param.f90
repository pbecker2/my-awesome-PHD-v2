module param
  implicit none
  integer, parameter :: ikfmax=440
  integer, parameter :: irhomax=1000*5
  double precision, parameter :: drho=0.001d0
  double precision, parameter :: dkf=0.01d0
  double precision, parameter :: pi=3.1415926535897932d0
  double precision, parameter :: hbarc=197.326968d0
  double precision, parameter :: hbarc2=hbarc**2

  !
  double precision :: t0,x0
  double precision :: t1,x1,t2,x2,t3,x3,alpha
  double precision :: t14,x14,t24,x24,hb2m,W0
  double precision :: t16,x16,t26,x26,to,to4,to6,te4,te6
  !
  double precision,parameter :: npi2=3.d0*pi**2
  double precision,parameter :: npi23=(npi2)**(2.d0/3.d0)
  double precision,parameter :: tpi2=3.d0*pi**2/2.d0
  double precision,parameter :: tpi23=(tpi2)**(2.d0/3.d0)
  double precision :: nucleonmass=(938.2720d0+939.5653d0)/2.0d0 ! [MeV]

end module param



subroutine force(ivlyb)

  use param

  implicit none

  integer :: ivlyb

  t0=0.d0;      x0=0.d0
  t1=0.d0;     x1=0.d0
  t2=0.d0;     x2=0.d0
  t3=0.d0;     x3=0.d0
  t14=0.d0;     x14=0.d0
  t24=0.d0;     x24=0.d0
  t16=0d0;     x16=0.d0
  t26=0.d0;     x26=0.d0
  alpha=1.d0/6.d0
  !
  to=0.d0;     te4=0.d0
  to4=0.d0;     te6=0.d0
  to6=0.d0;     W0=0.000d0

  select case(ivlyb)
  case(1)
     !=== SLY4 =====
     t0=-2488.913d0; x0=0.834d0
     t1=486.818d0;     x1=-0.344d0
     t2=-546.395d0;     x2=-1.d0
     t3=13777d0;     x3=1.354d0
     t14=0.d0;     x14=0.d0
     t24=0.d0;     x24=0.d0
     t16=0d0;     x16=0.d0
     t26=0.d0;     x26=0.d0
     alpha=1.d0/6.d0
     !
     to=0.d0
     te4=0.d0;     to4=0.d0
     te6=0.d0;     to6=0.d0
     !
     W0=123.000d0

  case(2)
     t0=-2394.15d0;     x0=0.632433d0
     t1=-19.381d0;     x1=35.182d0
     t2=513.267d0;     x2=-1.01914
     t3=13763.d0;     x3=1.d0
     t14=9.63577d0;     x14=3.65615d0
     t24=-65.3664d0;     x24=-1.22006d0
     t16=0.d0;     x16=0.d0
     t26=0.d0;     x26=0.d0
     alpha=1.d0/6.d0
     !
     to=401.816d0
     te4=-294.851d0;     to4=-12.0604d0
     te6=0.d0;     to6=0.d0
     !
     W0=245.741d0
  case(3)

     !=== N3LO =====
     t0=-2742.94d0;     x0=0.202208d0
     t1=95.5013d0;     x1=0.286608d0
     t2=528.992d0;     x2=-1.01263d0
     t3=16000.d0;     x3=0.2d0
     t14=-17.9822d0;     x14=1.00499d0
     t24=-68.4368d0;     x24=-1.20122d0
     t16=0.0d0;     x16=0d0
     t26=0.685403d0;     x26=-1.25d0
     alpha=1.d0/6.d0

  case(4)

     !=== N3LO bis =====
     t0=-2518.24d0;     x0=0.253758d0
     t1=207.3d0;     x1=-0.168828d0
     t2=527.93d0;     x2=-1.01306d0
     t3=13763.d0;     x3=0.3d0
     t14=-23.6909d0;     x14=0.564973d0
     t24=-68.263d0;     x24=-1.20224d0
     t16=0.0d0;     x16=0d0
     t26=0.689983d0;     x26=-1.25d0
     alpha=1.d0/6.d0

  case(5)

     !=== LNS =====
     t0=-2484.97d0;     x0=0.06277d0
     t1=266.735d0;     x1=0.65845d0
     t2=-337.135d0;     x2=-0.95382d0
     t3=14588.2d0;     x3=-0.03413d0
   
     alpha=1.d0/6.d0

!!$     !
!!$     to=0.d0
!!$     te4=0.d0;     to4=0.d0
!!$     te6=0.d0;     to6=0.d0
!!$     !
!!$     W0=0.d0

    !=== N3LO =====
!!$     t0=-2500.49d0;     x0=0.598658d0
!!$     t1=337.793d0;     x1=-2.18967d0
!!$     t2=587.22d0;     x2=-1.08109d0
!!$     t3=13763.d0;     x3=1.d0
!!$     t14=-116.489d0;     x14=-0.477614d0
!!$     t24=-108.342d0;     x24=-1.33548d0
!!$     t16=2.84983d0;     x16=-0.161801d0
!!$     t26=1.23644d0;     x26=-1.51102d0
!!$     alpha=1.d0/6.d0
    !
     to=608.126d0
     te4=-504.419d0;     to4=-72.78d0
     te6=107.776d0;     to6=20.7379d0
    !
     W0=241.748d0


     !
  case (12)
     t3=16000d0; x3=0.2d0
     t0=-2742.94d0; x0=0.202208d0
     t1=95.5013d0; x1=0.286608d0
     t2=528.992d0; x2=-1.01263d0
     t14=-17.9822d0; x14=1.00499d0
     t24=-68.4368d0; x24=-1.20122d0
     t16=0.d0; x16=0.d0
     t26=0.685403d0; x26=-1.25d0 


!!$  case (14)
!!$     t0= -1883.68781034; t1= 277.50021224; t2= 608.43090559; t3= 13901.94834463
!!$     x0= 0.00974375; x1= -1.77784395; x2= -1.67699035; x3= -0.38079041
!!$
!!$
!!$     alpha=0.32195599

  case default
     stop 'errorrrr'
  end select
  hb2m=20.7355d0
  !call convert

  return
end subroutine force


subroutine convert

  use param
  implicit none

  double precision b014,b104
  double precision ccc,aa103,aa013,aa105,aa015
  double precision aa115,aa005,aa107,aa017,aa117,aa007

  ccc=1./(2./(3.*pi**2))

  !write(*,*)ccc
  b014=t3*(1.-x3)/32 *ccc**(1+alpha)
  b104=t3*(1.-x3)/32 *ccc**(1+alpha)

  aa103=3./16.*t0/ccc*(1.+x0)
  aa013=3./16.*t0/ccc*(1.-x0)

  aa105=9./160.*t1/ccc*(1.+x1)
  aa015=9./160.*t1/ccc*(1.-x1)

  aa115=27./160.*t2/ccc*(1.+x2)
  aa005=3./160.*t2/ccc*(1.-x2)


  aa107=27./560*t14*(1.+x14)/ccc
  aa017=27./560*t14*(1.-x14)/ccc

  aa117=81./560.*t24*(1.+x24)/ccc
  aa007=9./560.*t24*(1.-x24)/ccc


  write(*,*)'----- Coeffieicents'
  !    write(*,*)b014,b104
  write(*,*)'aa103 ',aa103   
  write(*,*)'aa013 ',aa013   
  write(*,*)'aa105 ',aa105   
  write(*,*)'aa015 ',aa015
  write(*,*)'aa115 ',aa115   
  write(*,*)'aa005 ',aa005
  write(*,*)'aa107 ',aa107
  write(*,*)'aa017 ',aa017
  write(*,*)'aa007 ',aa007
  write(*,*)'aa117 ',aa117




  return
end subroutine convert
