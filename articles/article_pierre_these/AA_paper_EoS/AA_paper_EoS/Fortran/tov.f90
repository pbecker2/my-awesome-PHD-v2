! Module to calculate the mass (SM) and radius (R) for a neutron star

! The constants:
! [hbarc]=197.327 MeVfm
! [G]=6.67259 MeV^-2/hbarc/c^4
! [sm_solar]=1.989D+30 Kg
! Notice
! 1 Kg= 1.D+30/1.78266270D0 1 MEv/c^2
!
! The program runs with dimensioless variables:
! P=P/central_energy_density
! rho=rho/rho_central
! r=r/r0 with r0=1.D+19 fm =10 Km
! m=m/m_solar
!
! Prints out the central density in fm^-3, radius in m and neutron
! star mass in terms of solar masses

!===============================================================================


module star
  use param
  use inm
  use haensel

  ! gravitational constants
  integer :: ibetaeq=1 ! to do (1) or not (0) the beta equlibrium
  double precision, parameter :: fourpi=4*(2.d0*asin(1.d0))
  double precision, public, parameter :: g=6.67259D-45*hbarc
  double precision, parameter :: rhotrans=0.0d0

  ! number of central density chosen
  integer, public, parameter :: number_central_densities=1000
  integer, public, parameter :: number_differential_eqs=2
  integer, public, parameter :: max_rk4_steps=10000
  double precision, public, parameter :: diff_eq_step=0.001d0
  double precision, public, parameter :: first_density=0.001d0  ! fm^-3
  double precision, public, parameter :: last_density=2.1d0 ! fm^-3
  !
  double precision, public, parameter :: xkg=1.E+30/1.78266270d0
  double precision, public, parameter :: sm_solar=1.989E+30*xkg
  double precision, public :: central_energy_density
  double precision, public, allocatable :: YYfraction(:)  
contains

  function rho(x)!functionelle d'energie E et pas E/rho!!!
    implicit none
    double precision, intent(in) :: x
    double precision :: rho,kf,dens,EOSPNM,Pre,Y,Yp,Elec
    !    integer :: ng

    
    call protonfraction(x,Y,Yp)
    if(ibetaeq.eq.0)then 
       Y=1.d0; Yp=0.d0
    endif

    ! Test simulating presence of nuclei--> use SNM
    

    call EoSelectron(x,Yp,Elec)

    dens=x
    ! dens [fm^-3]
    !
    !============ PNM =============
    !
    !       write(*,*)'Ale'
    !    ng=2 ! degrees of freedom
    kf=(6.d0*pi**2*dens/2.)**(1.d0/3.d0)

    EOSPNM=EoSANM(x,Y)



    if(x.ge.rhotrans)then
       rho=EOSPNM*x+938.926*x*Yp+939.5*x*(1.D0-Yp)+Elec*x*Yp
    else

       call InterpolateDH(x,rho)
    endif



    return
  end function rho

  !
  !!
  !

  function press(x)! pressure as a function of the density
    implicit none
    double precision, intent(in) :: x
    double precision :: press,kin,c2,c4,c6,EE,t12
    double precision :: a0,a2,a4,a6,kf,dx,Y,Yp,Pel
    t12=1.0d0/2.d0
    dx=0.001d0

    call protonfraction(x,Y,Yp)
    if(ibetaeq.eq.0)then 
       Y=1.d0; Yp=0.d0
    endif
   

    call Pelectron(x,Yp,Pel)


    if(x.ge.rhotrans)then
       press=x**2*(EoSANM(x+dx,Y)-EoSANM(x,Y))/dx+Pel
    else
       call InterpolateDHpress(x,press)
    endif


    return
  end function press



  function tov(r,e_rho,y)
    implicit none
    double precision :: e_rho, tov, r
    double precision, dimension(number_differential_eqs), intent(in):: y

    tov=-(e_rho+y(1))*(y(2)+(r**3)*y(1))/(r*r-2*r*y(2))
  end function tov



  subroutine neutron_star


    implicit none
    integer :: i,j
    double precision, dimension (0:number_central_densities) :: cen_dens,radius, smass
    double precision, dimension (number_differential_eqs) :: ynew, y, ders
    double precision :: star_radius, ync, density_step, pressure, const_2, const_1
    double precision :: x

    !  do i=1,1000
    !   x=i*(1.d0/1000.d0)
    !   write(888,*)x,rho(x),press(x)
    !  enddo

    !stop
    cen_dens=0.d0; radius=0.d0; smass=0.d0

    open(unit=88,file='MassRadius.dat')

    density_step=(last_density-first_density)/number_central_densities


    do i=1,number_central_densities

       ync=first_density+density_step*i
       central_energy_density=rho(ync) !MeV/fm^3

       pressure=0.
       pressure=press(ync) ! MeV/fm^3

       const_1=10.d0**(-19.)/sqrt(fourpi*g*central_energy_density)
       const_2=fourpi*central_energy_density/ &
            (sqrt(fourpi*g*central_energy_density)**3)

       ! dimensionless pressure at the center

       y(1)=pressure/central_energy_density

       star_radius=diff_eq_step/10.

       y(2)=(star_radius**3)/3.


       ! Runge Kutta solution

       j=0

       do while((pressure > 0.).and. (j<=max_rk4_steps))
          j=j+1

          star_radius=star_radius+diff_eq_step

          call derivatives(star_radius,y,ders)

          ! RK4 procedure

          call runge_kutta_4(star_radius,y,ynew,ders)
          ! new values for
          ! y(1)=pressure
          ! y(2)=mass
          ! y(3)= metric function

          y=ynew 

          ! meVfm^-3

          pressure=y(1)*central_energy_density

       enddo

       cen_dens(i)=ync

       radius(i)=star_radius*const_1
       smass(i)=y(2)*const_2/sm_solar

!       if(smass(i).gt.smass(i-1).and.radius(i).lt.3.) &
            write(88,*)radius(i),smass(i),cen_dens(i)

    enddo

    close(88)

    return
  end subroutine neutron_star


  subroutine runge_kutta_4(x,y,yout,dydx)

    implicit none

    double precision, dimension (number_differential_eqs)::yt,dyt,dym
    double precision, dimension (number_differential_eqs), intent(in):: y,dydx
    double precision, dimension (number_differential_eqs), intent(out):: yout

    double precision :: hh, h6, xh
    double precision, intent(in) :: x

    hh=diff_eq_step*0.5; h6=diff_eq_step/6.; xh=x+hh

    ! first rk-step
    yt=y+hh*dydx
    call derivatives(xh,yt,dyt)

    ! second rk-step
    yt=y+hh*dyt
    call derivatives(xh,yt,dym)

    ! third rk-step
    yt=y+diff_eq_step*dym; dym=dyt+dym
    call derivatives(x+diff_eq_step,yt,dyt)

    ! fourth rk-step
    yout=y+h6*(dydx+dyt+2.*dym)


  end subroutine runge_kutta_4

  !

  subroutine derivatives(r,y,ders)

    implicit none

    double precision :: e_rho,x
    double precision, intent(in) :: r
    double precision, dimension(number_differential_eqs)::ders,y
    double precision :: p0

    if(y(1)>0.)then
       ! for a given pressure (y(1)*central_energy_density) => n???
       p0=y(1)*central_energy_density
       x=revert(p0)
       !x=(y(1)*central_energy_density/363.44)**(1./2.54)
       e_rho=rho(x)/central_energy_density
       ! TOV equation dp/dr
       ders(1)=tov(r,e_rho,y)
       ! dm/dr=
       ders(2)=(r**2)*e_rho
    endif

  contains


    function revert(pp)! density as a function of the pressure
      implicit none

      integer :: i,imax
      double precision :: pp,pA,pB
      double precision :: rhoA,rhoB
      double precision :: rhoM,pM,eps,revert,eps2
      double precision :: eps3,diff,P1,P2,R1,R2


      !write(*,*)'entro',PP
      eps=PP/(10.d0**5.d0)
      !eps2=PP/(10.d0**3.d0)
      !eps3=PP/(10.d0)

      imax=20000


      rhoA=0.d0
      rhoB=last_density

      pA=rho(rhoA)
      pB=rho(rhoB)

      do i=1,imax ! secant method
         rhoM=(rhoA+rhoB)/2.d0

         pM=press(rhoM)

         if(pM.gt.pp)then
            rhoB=rhoM
         else
            rhoA=rhoM
         endif
         diff=abs(rhoB-rhoA)


         ! write(*,*)pM,PP,abs(pM-PP),eps
         if(abs(pM-PP).le.eps)go to 300


         !
      enddo
      write(*,*) 'NON ho trovato !!!',PP,Pm,rhoA,rhoB
      !stop
      rhoM=0.d0
300   continue

      revert=rhoM
      return
    end function revert

  end subroutine derivatives


  !
  !!========================================
  ! 

  subroutine protonfraction(rho,YY,Yp)
    implicit none

    integer :: ikf,i
    integer,parameter :: itermax=10000
    double precision, parameter :: tol=0.001d0
    double precision :: kf,rho,Ya,Yb,Ym,t12,YY,Yp
    double precision :: rhon,rhop,kfn,kfp,kfe,mue
    double precision :: hb2me,diff,dmunmup,xxx


    t12=1.d0/2.d0


    Ya=-1.d0
    Yb=1.d0
    if(rho.gt.0.02d0)then


       do i=1,itermax
          Ym=(Ya+Yb)/2.d0
          rhon= t12*rho*(1.d0+Ym)
          kfn=(npi2*rhon)**(1.d0/3.d0)
          rhop=rho-rhon
          kfp=(npi2*rhop)**(1.d0/3.d0)
          kfe=(npi2*rhop)**(1.d0/3.d0)! rho_e=rho_p
          ! ultra-relativist electrons.....
          mue=hbarc*kfe

          call hatmu(dmunmup)
          diff=(mue-dmunmup)!/mue

          if(diff.gt.0.d0)then
             Ya=Ym
          else
             Yb=Ym
          endif

          if(abs(diff).le.tol) go to 401
          !       write(*,*)rho,mue-dmunmup,Ym

       enddo
       write(*,*)rho,Ym,diff

       write(*,*)'Hai fallito!!!'
       stop
401    continue

       YY=Ym
       Yp=rhop/rho
    else
       YY=1.d0
       Yp=0.d0
    endif
  contains 

    subroutine hatmu(res)
      ! i calculate 2* d(E/A)/dY

      double precision,parameter:: ddy=0.001d0
      double precision :: a1,a2,Ypstep,res

      Ypstep=Ym+ddy
      if(Ypstep.gt.1.d0)Ypstep=1.d0

      a1=EoSANM(rho,Ypstep)
      a2=EoSANM(rho,Ym)

      res=2.d0*(a1-a2)/ddy


      return
    end subroutine hatmu
    !
  end subroutine protonfraction
  !
  !!
  !

  subroutine EoSelectron(x,Yp,E)

    implicit none

    double precision :: x,Yp,rhoe
    double precision :: E,z,pf,kfe,mel,n0

    mel=0.501d0
    rhoe=x*Yp
    kfe=(npi2*rhoe)**(1.d0/3.d0)! rho_e=rho_p

    pf=hbarc*kfe

    z=pf/mel
    !n0=((8.d0*pi/3.d0)*(mel/(2*pi*hbarc)))*mel*(1.d0/8.d0)

    !write(*,*)n0,Yp,z
    E=2*pi*pf**4/(2*pi*hbarc)**3 !n0*(-88*pi/(2*pi*hbarc)**3*pf**4/4 !n0*(-8*z**3+3*sqrt(z**2+1)*(2*z**3+z)-3*asinh(z))   

    E=E/(x)
    !write(*,*)-8*z**3+3*sqrt(z**2+1)*(2*z**3+z),-3*asinh(z)
    return
  end subroutine EoSelectron


  subroutine Pelectron(x,Yp,P)

    implicit none

    double precision :: x,Yp,rhoe
    double precision :: P,z,pf,kfe,mel,n0
    double precision :: dpf,dx,E1,E2

    dx=0.001d0


    call EoSelectron(x,Yp,E1)
    call EoSelectron(x+dx,Yp,E2)

    P=x**2*(E2-E1)/dx
!  
    return
  end subroutine Pelectron

  !
  !!
  !


  subroutine test

    implicit none

    integer ix
    double precision x,dx
    dx=0.001D0


    open(unit=10,file='test.dat')

    do ix=1,200
       x=ix*dx

       write(10,*)x,rho(x),press(x)
    enddo
    close(10)
  end subroutine test


end module star
