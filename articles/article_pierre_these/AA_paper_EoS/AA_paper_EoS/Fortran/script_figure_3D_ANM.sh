#!/bin/bash -l
#
# SCRIPT TO MAKE FIGURE 3D pictures of ANM N3LO
#

file='Surface_Eos_ANM.dat'
figure='ANM.gnuplot'
eps000='EOS_ANM.eps'





echo " set term postscript eps color enh \"Helvetica\" 14 
       set output \"${eps000}\"
       set border linewidth 2
       set yrange [0:1]
       set xrange [0:0.8]
       set zrange [-20:300]
       set view map
       set xlabel \"n [fm^{-3}]\" font \"Helvetica,25 \"
       set ylabel \"Y\" font \"Helvetica,25\"
       set zlabel  \"\" offset 12,6,1 font \"Helvetica,25\"
       set hidden3d
       set contour base
       set cntrparam levels incremental -20,5,300 
       set pm3d implicit at s
       unset clabel
       set style data lines 
       unset surface
       set xtics font \"Helvetica,22\" 
       set ytics font \"Helvetica,22\" 
       set cbtics font \"Helvetica,22\" 

       set label \"E/A [MeV]\"  at graph  0.4, graph  1.1 font \"Helvetica,25\"

       set palette positive nops_allcF maxcolors 0 gamma 1.5 color model CMY 

       splot \"${file}\" u 1:2:3 with pm3d  lc rgb \"#007700\" title\" \"
       

       set terminal x11  ">>$figure
gnuplot $figure
rm *.gnuplot
epstopdf $eps000


#
# SCRIPT TO MAKE FIGURE 3D pictures of Polarized N3LO
#

file='Surface_Eos_PolSNM.dat'
figure='POL.gnuplot'
eps000='EOS_PolSNM.eps'





echo " set term postscript eps color enh \"Helvetica\" 14 
       set output \"${eps000}\"
       set border linewidth 2
       set yrange [0:1]
       set xrange [0:0.8]
       set zrange [-20:300]
       set view map
       set xlabel \" {/Symbol r}[fm^{-3}]\" font \"Helvetica,25 \"
       set ylabel \"Y_s\" font \"Helvetica,25\"
       set zlabel  \"\" offset 12,6,1 font \"Helvetica,25\"
       set hidden3d
       set contour base
       set cntrparam levels incremental -20,5,300 
       set pm3d implicit at s
       unset clabel
       set style data lines 
       unset surface
       set xtics font \"Helvetica,22\" 
       set ytics font \"Helvetica,22\" 
       set cbtics font \"Helvetica,22\" 

       set label \"E/A [MeV]\"  at graph  0.4, graph  1.1 font \"Helvetica,25\"

       set palette positive nops_allcF maxcolors 0 gamma 1.5 color model CMY 

       splot \"${file}\" u 1:2:3 with pm3d  lc rgb \"#007700\" title\" \"
       

       set terminal x11  ">>$figure
gnuplot $figure
rm *.gnuplot
epstopdf $eps000
