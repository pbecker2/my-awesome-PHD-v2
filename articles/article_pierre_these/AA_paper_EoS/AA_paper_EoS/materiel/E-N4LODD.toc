\contentsline {section}{\numberline {1}The N4LO interaction}{2}
\contentsline {section}{\numberline {2}Densities, Fermi momenta, symmetry parameters}{2}
\contentsline {section}{\numberline {3}Preparing sums over momenta}{2}
\contentsline {section}{\numberline {4}Kinetic energy}{3}
\contentsline {section}{\numberline {5}Interaction order by order}{3}
\contentsline {subsection}{\numberline {5.1}N0LO}{3}
\contentsline {subsection}{\numberline {5.2}N1LO}{3}
\contentsline {subsection}{\numberline {5.3}N2LO}{4}
\contentsline {subsection}{\numberline {5.4}N3LO}{4}
\contentsline {subsection}{\numberline {5.5}N4LO}{5}
\contentsline {section}{\numberline {6}The mother of all formulae}{6}
\contentsline {section}{\numberline {7}GS expectation values}{8}
\contentsline {subsection}{\numberline {7.1}Symmetric nuclear matter}{9}
\contentsline {subsection}{\numberline {7.2}Asymmetric nuclear matter}{9}
\contentsline {subsection}{\numberline {7.3}Spin polarized symmetric nuclear matter}{10}
\contentsline {subsection}{\numberline {7.4}Fully spin-polarized symmetric nuclear matter}{11}
\contentsline {subsection}{\numberline {7.5}Neutron matter}{11}
\contentsline {subsection}{\numberline {7.6}Fully spin polarized neutron matter}{11}
\contentsline {section}{\numberline {8}Symmetry energies}{12}
\contentsline {subsection}{\numberline {8.1}Second-order expansions in the asymmetry parameters}{12}
\contentsline {subsection}{\numberline {8.2}A test: $E/A)_0$}{12}
\contentsline {subsection}{\numberline {8.3}Symmetry energies}{12}
\contentsline {subsection}{\numberline {8.4}In terms of Landau parameters}{13}
