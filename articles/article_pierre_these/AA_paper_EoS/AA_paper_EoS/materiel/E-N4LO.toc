\contentsline {section}{\numberline {1}The N4LO interaction}{3}
\contentsline {subsection}{\numberline {1.1}Up to N3LO}{3}
\contentsline {subsection}{\numberline {1.2}N4LO}{3}
\contentsline {subsection}{\numberline {1.3}N4LO contribution to (S,T) channels}{4}
\contentsline {subsection}{\numberline {1.4}N4LO contribution to partial waves}{5}
\contentsline {section}{\numberline {2}Densities, Fermi momenta, symmetry parameters}{6}
\contentsline {section}{\numberline {3}Preparing sums over momenta}{6}
\contentsline {section}{\numberline {4}Kinetic energy}{7}
\contentsline {section}{\numberline {5}Interaction order by order}{7}
\contentsline {subsection}{\numberline {5.1}N0LO}{7}
\contentsline {subsection}{\numberline {5.2}N1LO}{7}
\contentsline {subsection}{\numberline {5.3}N2LO}{8}
\contentsline {subsection}{\numberline {5.4}N3LO}{8}
\contentsline {subsection}{\numberline {5.5}N4LO}{9}
\contentsline {section}{\numberline {6}The mother of all formulae}{10}
\contentsline {section}{\numberline {7}GS expectation values}{13}
\contentsline {subsection}{\numberline {7.1}The required quantities}{13}
\contentsline {subsection}{\numberline {7.2}Symmetric nuclear matter}{13}
\contentsline {subsection}{\numberline {7.3}Asymmetric nuclear matter}{13}
\contentsline {subsection}{\numberline {7.4}Spin polarized symmetric nuclear matter}{15}
\contentsline {subsection}{\numberline {7.5}Fully spin-polarized symmetric nuclear matter}{16}
\contentsline {subsection}{\numberline {7.6}Neutron matter}{16}
\contentsline {subsection}{\numberline {7.7}Fully spin polarized neutron matter}{17}
\contentsline {subsection}{\numberline {7.8}Spin-polarized neutron matter}{17}
\contentsline {section}{\numberline {8}Symmetry energies}{18}
\contentsline {subsection}{\numberline {8.1}Second-order expansions in the asymmetry parameters}{18}
\contentsline {subsection}{\numberline {8.2}A test: $E/A)_0$}{19}
\contentsline {subsection}{\numberline {8.3}Symmetry energies}{19}
\contentsline {subsection}{\numberline {8.4}In terms of Landau parameters}{20}
\contentsline {section}{\numberline {9}Preparation of the fit}{21}
\contentsline {subsection}{\numberline {9.1}The expressions}{21}
\contentsline {subsection}{\numberline {9.2}Definitions of coefficients}{21}
\contentsline {subsection}{\numberline {9.3}Proposal: N$(n)$LO for (S,T) and N$(n+1)$LO for SNM and PNM}{22}
\contentsline {subsubsection}{\numberline {9.3.1}N2LO for (S,T) and N3LO for SNM and PNM}{23}
\contentsline {subsubsection}{\numberline {9.3.2}N3LO for (S,T) and N4LO for SNM and PNM}{23}
