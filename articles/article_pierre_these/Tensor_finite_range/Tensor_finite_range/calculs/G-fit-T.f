ccc   fitting baldo's delta_p with VT1+VT2 and delta_d with VT1-VT2
        implicit real*8(a-h,o-z)
        dimension xkf(16),dpB(16),ddB(16),dfB(16)
        dimension G1(16),G2(16),G3(16),expint(16)
        open(unit=10,file='Baldo-DELTA.dat')
        open(unit=11,file='G-d-Baldo.dat')
        open(unit=20,file='G-fit-T.dat')
        open(unit=21,file='G-marcella.dat')
        open(unit=22,file='G-VLyB33.dat')
        open(unit=23,file='G-W0-VT.dat')

        pi=4.d0*datan(1.d0)
ccc     values for W0 and mu_T
        W0=103.d0
         xmug=1.2d0

ccc   construct the differences
        do i=1,16
        read(10,*) xkf(i),a,b,c,d,e,f
        dpB(i)=b/3.d0-a
        ddB(i)=c/5.d0-e/7.d0
        dfB(i)=d/7.d0-f/9.d0
        write(11,*) xkf(i),dpB(i),ddB(i),dfB(i)
        enddo

ccc  constructing the required functions GL(mu kF)
ccc   first the exponential integral
ccc   simpson integral with a shameful small step
        do i=1,16
        a=xmug*xkf(i)
        pas=0.000001
        tfin=10.d0
        t=a**2
        som=exp(-t)/t/2.d0
        do while(t.lt.tfin)
        t=t+pas
        som=som-dexp(-t)/t
        enddo
        expint(i)=pas*som
        enddo

        euler=0.5772156649d0
        do i=1,16
        a=xmug*xkf(i)
        G1(i)=1/a**3*(6-6*a**2-a**4-6*dexp(-a**2)
     1          +4*a**2*(euler-expint(i)+dlog(a**2)))
        G2(i)=1/a**3*(10-34*a**2-a**4-10*dexp(-a**2)
     1          +12*(2+a**2)*(euler-expint(i)+dlog(a**2)))
        G3(i)=1/a**5*(120-94*a**2-86*a**4-a**6
     1          -(120+26*a**2)*dexp(-a**2)
     1          +24*a**2*(5+a**2)*(euler-expint(i)+dlog(a**2)))
        enddo
c        write(*,*) G1(1),G2(1),G3(1)

ccc allow for fitting with less than 16 points
        write(*,*) 'npoints'
        read(*,*) npoints

ccc   first p-waves with W0 fixed
        SG=0.d0
        SY=0.d0
        do i=1,npoints
        y=dpB(i)+W0*xkf(i)**5/(120*pi**2)
        x=-27*xmuG**2/(40*dsqrt(pi))*G1(i)
        SG=SG+x**2
        SY=SY+y*x
        enddo
        VTsum=SY/SG 
        write(*,*) 'W0=',W0,' VTsum=',VTsum
         do i=1,16
        dpCAL=-W0*xkf(i)**5/(120*pi**2)
     1               -VTsum*27*xmuG**2/(40*sqrt(pi))*G1(i)
        dfCAL=-3*VTsum*xmuG**2/(10*dsqrt(pi))*G3(i)
        write(20,*) xkf(i),dpCAL,dfCAL
        enddo

        write(20,*)

ccc  now fit W0 and VTsum
       SG=0.d0
       S10=0.d0
       S5G=0.d0
       SYG=0.d0
       SY5=0.d0
       do i=1,npoints
       SG=SG+(G1(i))**2
        S5G=S5G+G1(i)*xkf(i)**5
        S10=s10+xkf(i)**10
        SYG=SYG+dpB(i)*G1(i)
        SY5=sY5+dpB(i)*xkf(i)**5
        enddo
        DD=SG*S10-S5G**2
        AA=SYG*S10-S5G*SY5
        BB=SG*SY5-S5G*SYG
        AA=AA/DD
        BB=BB/DD
        WW0=-120*pi**2*BB
        VTsum=-AA*40*dsqrt(pi)/(27*xmug**2)
        write(*,*) 'WW0=',WW0,' VTsum=',VTsum
 
        do i=1,16
        dpCAL=AA*G1(i)+BB*xkf(i)**5
        dfCAL=-3*VTsum*xmug**2/(10*dsqrt(pi))*G3(i)
        write(23,*) xkf(i),dpCAL,dfCAL
        enddo

        write(20,*)
ccc   fit d-waves 
        SG=0.d0
        SY=0.d0
        do i=1,npoints
        y=ddB(i)
        x=-27*xmug**2/(280*dsqrt(pi))*G2(i)
        SG=SG+x**2
        SY=SY+y*x
        enddo
        VTdif=SY/SG
        write(*,*) 'VTdif=',VTdif
        write(20,*)
        do i=1,16
        ddCAL=-VTdif*27*xmug**2/(280*dsqrt(pi))*G2(i)
        write(20,*) xkf(i),ddCAL
        enddo

ccc     marcella's results
        VTsum=-75.d0
        VTdif=VTsum-2*60.d0
c      original values 
c        W0=130.d0
c        VTsum=-20.d0
c        VTdif=VTsum-2*110.d0

        do i=1,16
        dpMAR=-27*xmug**2/(40*dsqrt(pi))*VTsum*G1(i)
     1        -W0/(120*pi**2)*xkf(i)**5
        ddMAR=-VTdif*27*xmug**2/(280*dsqrt(pi))*G2(i)
        dfMAR=-3*VTsum*xmug**2/(10*dsqrt(pi))*G3(i)
ccc     sign
        dpMARm=27*xmug**2/(40*dsqrt(pi))*VTsum*G1(i)
     1        -W0/(120*pi**2)*xkf(i)**5
        dfMARm=3*VTsum*xmug**2/(10*dsqrt(pi))*G3(i)
        write(21,*) xkf(i),dpMAR,dpMARm,ddMAR,dfMAR,dfMARm
        enddo

ccc     SLyB33 results
        w0=241.748d0
        to2=608.126d0
        to4=-72.78d0
        to6=20.7379d0
        do i=1,16
        rho=2.d0/(3*pi**2)*xkf(i)**3
        x=xkf(i)
        dp=(-w0/80.d0+9/160.d0*to2)*rho*x**2
     1     +27/280.d0*rho*x**4+17/500.d0*rho*x**6
        write(22,*) x,dp
        enddo

        end

