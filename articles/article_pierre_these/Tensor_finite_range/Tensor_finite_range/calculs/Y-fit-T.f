ccc   fitting baldo's delta_p etc with Nakada
        implicit real*8(a-h,o-z)
        dimension xkf(16),dpB(16),ddB(16),dfB(16)
        dimension YLS1(2,16),YLS2(2,16),YLS3(2,16),DILS(2,16)
        dimension YTN1(2,16),YTN2(2,16),YTN3(2,16),DITN(2,16)
        dimension tLSE(2),tLSO(2),xmLS(2)
        dimension tTNE(2),tTNO(2),xmTN(2)
        open(unit=10,file='Baldo-DELTA.dat')
        open(unit=20,file='Y-fit-T.dat')
        open(unit=30,file='dilogs.dat')
         pi=4.d0*datan(1.d0)
ccc     Nakada's parameters
        tLSE(1)=-9181.8d0
        tLSO(1)=-3414.6d0
        tLSE(2)=-606.6d0
        tLSO(2)=-1137.6d0
        xmLS(1)=1/0.25d0
        xmLS(2)=1/0.4d0
        tTNE(1)=-131.52d0
        tTNO(1)=29.28d0
        tTNE(2)=-3.708d0
        tTNO(2)=1.872d0
        xmTN(1)=1/0.4d0
        xmTN(2)=1/0.7d0

c        write(*,*) tTNE(1),tTNE(2),tTNO(1),tTNO(2)


ccc   construct the differences
        do i=1,16
        read(10,*) xkf(i),a,b,c,d,e,f
        dpB(i)=b/3.d0-a
        ddB(i)=c/5.d0-e/7.d0
        dfB(i)=d/7.d0-f/9.d0
        enddo

ccc  constructing the required dilogarithmic functions
        do i=1,16
        a1=4*(xkf(i)/xmLS(1))**2
        a2=4*(xkf(i)/xmLS(2))**2
        b1=4*(xkf(i)/xmTN(1))**2
        b2=4*(xkf(i)/xmTN(2))**2  
        pas=0.0005
        tfin=70.d0
        t=pas
        somLS1=0.d0
        somLS2=0.d0
        somTN1=0.d0
        somTN2=0.d0
        do while(t.lt.tfin)
        t=t+pas
        somLS1=somLS1-t/(1.d0+dexp(t)/a1)
        somLS2=somLS2-t/(1.d0+dexp(t)/a2)
        somTN1=somTN1-t/(1.d0+dexp(t)/b1)
        somTN2=somTN2-t/(1.d0+dexp(t)/b2)
        enddo
        DILS(1,i)=somLS1*pas
        DILS(2,i)=somLS2*pas
        DITN(1,i)=somTN1*pas
        DITN(2,i)=somTN2*pas    
        write(30,*) xkf(i),DILS(1,i),DILS(2,i),DITN(1,i),DITN(2,i)   
        enddo
ccc     constructing the Y functions
        do i=1,16
        a1=xkf(i)/xmLS(1)
        a2=xkf(i)/xmLS(2)
        b1=xkf(i)/xmTN(1)
        b2=xkf(i)/xmTN(2)  
        YLS1(1,i)=1/a1**3*(-4*a1**2-88*a1**4+128*a1**3*datan(2*a1)
     1     +(1-24*a1**2+16*a1**4)*dlog(1.d0+4*a1**2)
     2     +16*a1**2*DILS(1,i))
        YLS1(2,i)=1/a2**3*(-4*a2**2-88*a2**4+128*a2**3*datan(2*a2)
     1     +(1-24*a2**2+16*a2**4)*dlog(1.d0+4*a2**2)
     1     +16*a2**2*DILS(2,i))
        YTN1(1,i)=1/b1**3*(4*b1**2*(3-2*b1**2)
     1     -3*(1+4*b1**2)*dlog(1.d0+4*b1**2)-8*b1**2*DITN(1,i))
        YTN1(2,i)=1/b2**3*(4*b2**2*(3-2*b2**2)
     1     -3*(1+4*b2**2)*dlog(1.d0+4*b2**2)-8*b2**2*DITN(2,i))
        YLS2(1,i)=1/a1**3*(-172*a1**2-312*a1**4+640*a1**3*datan(2*a1)
     1     +(31-24*a1**2+48*a1**4)*dlog(1.d0+4*a1**2)
     2     +12*(-1+12*a1**2)*DILS(1,i))
        YLS2(2,i)=1/a2**3*(-172*a2**2-312*a2**4+640*a2**3*datan(2*a2)
     1     +(31-24*a2**2+48*a2**4)*dlog(1.d0+4*a2**2)
     2     +12*(-1+12*a2**2)*DILS(2,i))
        YTN2(1,i)=1/b1**3*(4*b1**2*(29-2*b1**2)
     1     -17*(1+4*b1**2)*dlog(1.d0+4*b1**2)
     1     +12*(1-2*b1**2)*DITN(1,i))
        YTN2(2,i)=1/b2**3*(4*b2**2*(29-2*b2**2)
     1     -17*(1+4*b2**2)*dlog(1.d0+4*b2**2)
     1     +12*(1-2*b2**2)*DITN(2,i))
        YLS3(1,i)=1/a1**5*(12*a1**2-596*a1**4-344*a1**6
     1     +896*a1**5*datan(2*a1)
     1     +(-3+83*a1**2+168*a1**4+48*a1**6)*dlog(1.d0+4*a1**2)
     2     +12*a1**2*(-5+24*a1**2)*DILS(1,i))
        YLS3(2,i)=1/a2**5*(12*a2**2-596*a2**4-344*a2**6
     1     +896*a2**5*datan(2*a2)
     1     +(-3+83*a2**2+168*a2**4+48*a2**6)*dlog(1.d0+4*a2**2)
     2     +12*a2**2*(-5+24*a2**2)*DILS(2,i))
        YTN3(1,i)=1/b1**5*(-4*b1**2*(15-176*b1**2+4*b1**4)
     1     +(15-26*b1**2-344*b1**4)*dlog(1.d0+4*b1**2)
     1     +24*b1**2*(5-4*b1**2)*DITN(1,i))
        YTN3(2,i)=1/b2**5*(-4*b2**2*(15-176*b2**2+4*b2**4)
     1     +(15-26*b2**2-344*b2**4)*dlog(1.d0+4*b2**2)
     1     +24*b2**2*(5-4*b2**2)*DITN(2,i))
c        write(20,*) xkf(i),YLS1(1,i),YLS1(2,i),YTN1(1,i),YTN1(2,i)
c        write(20,*) xkf(i),YLS2(1,i),YLS2(2,i),YTN2(1,i),YTN2(2,i)
c        write(20,*) xkf(i),YLS3(1,i),YLS3(2,i),YTN3(1,i),YTN3(2,i)
        enddo

ccc    the differences
        do i=1,16
        dpCAL=-3.d0/(128*pi)*(tLSO(1)*YLS1(1,i)+tLSO(2)*YLS1(2,i))
     1    -27.d0/(80*pi)*(tTNO(1)/xmTN(1)**2*YTN1(1,i)
     1                    +tTNO(2)/xmTN(2)**2*YTN1(2,i))
        ddCAL=1.d0/(128*pi)*(tLSE(1)*YLS2(1,i)+tLSE(2)*YLS2(2,i))
     1    -27.d0/(560*pi)*(tTNE(1)/xmTN(1)**2*YTN2(1,i)
     1                    +tTNE(2)/xmTN(2)**2*YTN2(2,i))
        dfCAL=1.d0/(32*pi)*(tLSO(1)*YLS3(1,i)+tLSO(2)*YLS3(2,i))
     1    -3.d0/(40*pi)*(tTNO(1)/xmTN(1)**2*YTN3(1,i)
     1                    +tTNO(2)/xmTN(2)**2*YTN3(2,i))
        write(20,*) xkf(i),dpB(i),dpCAL,ddB(i),ddCAL,dfB(i),dfCAL
        enddo

ccc     fitting tensor parameters
c       chose number of points
        write(*,*) 'points'
        read(*,*) npoints
c       p waves
        SpAA=0.d0
        SpAB=0.d0
        SpBB=0.d0
        SpyA=0.d0
        SpyB=0.d0
c       d waves
        SdAA=0.d0
        SdAB=0.d0
        SdBB=0.d0
        SdyA=0.d0
        SdyB=0.d0
        do i=1,npoints
c       p waves
        yp=dpB(i)+3.d0/(128*pi)*(tLSO(1)*YLS1(1,i)+tLSO(2)*YLS1(2,i))
        Ap=-27.d0/(80*pi)/xmTN(1)**2*YTN1(1,i)
        Bp=-27.d0/(80*pi)/xmTN(2)**2*YTN1(2,i)
        SpAA=SpAA+Ap**2
        SpAB=SpAB+Ap*Bp
        SpBB=SpBB+Bp**2
        SpyA=SpyA+yp*Ap
        SpyB=SpyB+yp*Bp
c       dwaves
        yd=ddB(i)-1.d0/(128*pi)*(tLSE(1)*YLS2(1,i)+tLSE(2)*YLS2(2,i))
        Ad=-27.d0/(560*pi)/xmTN(1)**2*YTN2(1,i)
        Bd=-27.d0/(560*pi)/xmTN(2)**2*YTN2(2,i)
        SdAA=SdAA+Ad**2
        SdAB=SdAB+Ad*Bd
        SdBB=SdBB+Bd**2
        SdyA=SdyA+yd*Ad
        SdyB=SdyB+yd*Bd
        enddo
        DDp=SpAA*SpBB-SpAB**2
        AAp=SpyA*SpBB-SpyB*SpAB
        BBp=SpAA*SpyB-SpAB*SpyA
c        write(*,*) DDp,AAp,BBp
        DDd=SdAA*SdBB-SdAB**2
        AAd=SdyA*SdBB-SdyB*SdAB
        BBd=SdAA*SdyB-SdAB*SdyA
c        write(*,*) DDd,AAd,BBd
        ttTNO1=AAp/DDp
        ttTNO2=BBp/DDp
        ttTNE1=AAd/DDd
        ttTNE2=BBd/DDd
        write(*,*) ttTNE1,'//',tTNE(1)
        write(*,*) ttTNE2,'//',tTNE(2)
        write(*,*) ttTNO1,'//',tTNO(1)
        write(*,*) ttTNO2,'//',tTNO(2)

        write(20,*)
        errp=0.d0
        errd=0.d0
        errf=0.d0
        do i=1,16
        dpCAL=-3.d0/(128*pi)*(tLSO(1)*YLS1(1,i)+tLSO(2)*YLS1(2,i))
     1    -27.d0/(80*pi)*(ttTNO1/xmTN(1)**2*YTN1(1,i)
     1                    +ttTNO2/xmTN(2)**2*YTN1(2,i))
        ddCAL=1.d0/(128*pi)*(tLSE(1)*YLS2(1,i)+tLSE(2)*YLS2(2,i))
     1    -27.d0/(560*pi)*(ttTNE1/xmTN(1)**2*YTN2(1,i)
     1                    +ttTNE2/xmTN(2)**2*YTN2(2,i))
        dfCAL=1.d0/(32*pi)*(tLSO(1)*YLS3(1,i)+tLSO(2)*YLS3(2,i))
     1    -3.d0/(40*pi)*(ttTNO1/xmTN(1)**2*YTN3(1,i)
     1                    +ttTNO2/xmTN(2)**2*YTN3(2,i))
        write(20,*) xkf(i),dpCAL,ddCAL,dfCAL
        errp=errp+(1.d0-dpCAL/dpB(i))**2
        errd=errd+(1.d0-ddCAL/ddB(i))**2
        errf=errf+(1.d0-dfCAL/dfB(i))**2
        enddo
        write(*,*) 'chi**2  ',errp,errd,errf
        write(*,*) 'chi**2/16  ',errp/16.d0,errd/16.d0,errf/16.d0



        end

