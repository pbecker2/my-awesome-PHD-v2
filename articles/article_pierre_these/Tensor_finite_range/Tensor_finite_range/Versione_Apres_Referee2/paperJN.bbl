\begin{thebibliography}{38}
\expandafter\ifx\csname natexlab\endcsname\relax\def\natexlab#1{#1}\fi
\expandafter\ifx\csname bibnamefont\endcsname\relax
  \def\bibnamefont#1{#1}\fi
\expandafter\ifx\csname bibfnamefont\endcsname\relax
  \def\bibfnamefont#1{#1}\fi
\expandafter\ifx\csname citenamefont\endcsname\relax
  \def\citenamefont#1{#1}\fi
\expandafter\ifx\csname url\endcsname\relax
  \def\url#1{\texttt{#1}}\fi
\expandafter\ifx\csname urlprefix\endcsname\relax\def\urlprefix{URL }\fi
\providecommand{\bibinfo}[2]{#2}
\providecommand{\eprint}[2][]{\url{#2}}

\bibitem[{\citenamefont{Brown et~al.}(2006)\citenamefont{Brown, Duguet, Otsuka,
  Abe, and Suzuki}}]{bro06}
\bibinfo{author}{\bibfnamefont{B.~A.} \bibnamefont{Brown}},
  \bibinfo{author}{\bibfnamefont{T.}~\bibnamefont{Duguet}},
  \bibinfo{author}{\bibfnamefont{T.}~\bibnamefont{Otsuka}},
  \bibinfo{author}{\bibfnamefont{D.}~\bibnamefont{Abe}}, \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{T.}~\bibnamefont{Suzuki}},
  \bibinfo{journal}{Phys. Rev. C} \textbf{\bibinfo{volume}{74}},
  \bibinfo{pages}{061303} (\bibinfo{year}{2006}).

\bibitem[{\citenamefont{Lesinski et~al.}(2007)\citenamefont{Lesinski, Bender,
  Bennaceur, Duguet, and Meyer}}]{les07}
\bibinfo{author}{\bibfnamefont{T.}~\bibnamefont{Lesinski}},
  \bibinfo{author}{\bibfnamefont{M.}~\bibnamefont{Bender}},
  \bibinfo{author}{\bibfnamefont{K.}~\bibnamefont{Bennaceur}},
  \bibinfo{author}{\bibfnamefont{T.}~\bibnamefont{Duguet}}, \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Meyer}},
  \bibinfo{journal}{Phys. Rev. C} \textbf{\bibinfo{volume}{76}},
  \bibinfo{pages}{014312} (\bibinfo{year}{2007}).

\bibitem[{\citenamefont{Bai et~al.}(2009)\citenamefont{Bai, Sagawa, Zhang,
  Zhang, Col\`o, and Xu}}]{bai09}
\bibinfo{author}{\bibfnamefont{C.}~\bibnamefont{Bai}},
  \bibinfo{author}{\bibfnamefont{H.}~\bibnamefont{Sagawa}},
  \bibinfo{author}{\bibfnamefont{H.}~\bibnamefont{Zhang}},
  \bibinfo{author}{\bibfnamefont{X.}~\bibnamefont{Zhang}},
  \bibinfo{author}{\bibfnamefont{G.}~\bibnamefont{Col\`o}}, \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{F.}~\bibnamefont{Xu}}, \bibinfo{journal}{Phys.
  Lett. B} \textbf{\bibinfo{volume}{675}}, \bibinfo{pages}{28}
  (\bibinfo{year}{2009}).

\bibitem[{\citenamefont{Minato and Bai}(2013)}]{min13}
\bibinfo{author}{\bibfnamefont{F.}~\bibnamefont{Minato}} \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{C.}~\bibnamefont{Bai}},
  \bibinfo{journal}{Phys. Rev. Lett.} \textbf{\bibinfo{volume}{110}},
  \bibinfo{pages}{122501} (\bibinfo{year}{2013}).

\bibitem[{\citenamefont{Arima and Terasawa}(1960)}]{ari60}
\bibinfo{author}{\bibfnamefont{A.}~\bibnamefont{Arima}} \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{T.}~\bibnamefont{Terasawa}},
  \bibinfo{journal}{Progr. Theor. Phys.} \textbf{\bibinfo{volume}{23}},
  \bibinfo{pages}{115} (\bibinfo{year}{1960}).

\bibitem[{\citenamefont{Otsuka et~al.}(2005)\citenamefont{Otsuka, Suzuki,
  Fujimoto, Grawe, and Akaishi}}]{Ots05}
\bibinfo{author}{\bibfnamefont{T.}~\bibnamefont{Otsuka}},
  \bibinfo{author}{\bibfnamefont{T.}~\bibnamefont{Suzuki}},
  \bibinfo{author}{\bibfnamefont{R.}~\bibnamefont{Fujimoto}},
  \bibinfo{author}{\bibfnamefont{H.}~\bibnamefont{Grawe}}, \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{Y.}~\bibnamefont{Akaishi}},
  \bibinfo{journal}{Phys. Rev. Lett.} \textbf{\bibinfo{volume}{95}},
  \bibinfo{pages}{232502} (\bibinfo{year}{2005}).

\bibitem[{\citenamefont{Zalewski et~al.}(2008)\citenamefont{Zalewski,
  Dobaczewski, Satu\l{}a, and Werner}}]{zal08}
\bibinfo{author}{\bibfnamefont{M.}~\bibnamefont{Zalewski}},
  \bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Dobaczewski}},
  \bibinfo{author}{\bibfnamefont{W.}~\bibnamefont{Satu\l{}a}},
  \bibnamefont{and} \bibinfo{author}{\bibfnamefont{T.~R.}
  \bibnamefont{Werner}}, \bibinfo{journal}{Phys. Rev. C}
  \textbf{\bibinfo{volume}{77}}, \bibinfo{pages}{024316}
  (\bibinfo{year}{2008}).

\bibitem[{\citenamefont{Zou et~al.}(2008)\citenamefont{Zou, Col\`o, Ma, Sagawa,
  and Bortignon}}]{zou08}
\bibinfo{author}{\bibfnamefont{W.}~\bibnamefont{Zou}},
  \bibinfo{author}{\bibfnamefont{G.}~\bibnamefont{Col\`o}},
  \bibinfo{author}{\bibfnamefont{Z.}~\bibnamefont{Ma}},
  \bibinfo{author}{\bibfnamefont{H.}~\bibnamefont{Sagawa}}, \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{P.~F.} \bibnamefont{Bortignon}},
  \bibinfo{journal}{Phys. Rev. C} \textbf{\bibinfo{volume}{77}},
  \bibinfo{pages}{014314} (\bibinfo{year}{2008}),
  \urlprefix\url{http://link.aps.org/doi/10.1103/PhysRevC.77.014314}.

\bibitem[{\citenamefont{Bender et~al.}(2009)\citenamefont{Bender, Bennaceur,
  Duguet, Heenen, Lesinski, and Meyer}}]{ben09}
\bibinfo{author}{\bibfnamefont{M.}~\bibnamefont{Bender}},
  \bibinfo{author}{\bibfnamefont{K.}~\bibnamefont{Bennaceur}},
  \bibinfo{author}{\bibfnamefont{T.}~\bibnamefont{Duguet}},
  \bibinfo{author}{\bibfnamefont{P.~H.} \bibnamefont{Heenen}},
  \bibinfo{author}{\bibfnamefont{T.}~\bibnamefont{Lesinski}}, \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Meyer}},
  \bibinfo{journal}{Phys. Rev. C} \textbf{\bibinfo{volume}{80}},
  \bibinfo{pages}{064302} (\bibinfo{year}{2009}).

\bibitem[{\citenamefont{Sagawa and Col{\`o}}(2014)}]{sag14}
\bibinfo{author}{\bibfnamefont{H.}~\bibnamefont{Sagawa}} \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{G.}~\bibnamefont{Col{\`o}}},
  \bibinfo{journal}{Progr. Part. Nucl. Phys.} \textbf{\bibinfo{volume}{76}},
  \bibinfo{pages}{76} (\bibinfo{year}{2014}).

\bibitem[{\citenamefont{Stancu et~al.}(1977)\citenamefont{Stancu, Brink, and
  Flocard}}]{sta77}
\bibinfo{author}{\bibfnamefont{F.}~\bibnamefont{Stancu}},
  \bibinfo{author}{\bibfnamefont{D.}~\bibnamefont{Brink}}, \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{H.}~\bibnamefont{Flocard}},
  \bibinfo{journal}{Phys. Lett. B} \textbf{\bibinfo{volume}{68}},
  \bibinfo{pages}{108} (\bibinfo{year}{1977}).

\bibitem[{\citenamefont{Onishi and Negele}(1978)}]{oni78}
\bibinfo{author}{\bibfnamefont{N.}~\bibnamefont{Onishi}} \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Negele}},
  \bibinfo{journal}{Nucl. Phys. A} \textbf{\bibinfo{volume}{301}},
  \bibinfo{pages}{336} (\bibinfo{year}{1978}).

\bibitem[{\citenamefont{Vautherin and Brink}(1972)}]{vau72}
\bibinfo{author}{\bibfnamefont{D.}~\bibnamefont{Vautherin}} \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{D.}~\bibnamefont{Brink}},
  \bibinfo{journal}{Phys. Rev. C} \textbf{\bibinfo{volume}{5}},
  \bibinfo{pages}{626} (\bibinfo{year}{1972}).

\bibitem[{\citenamefont{Decharg{\'e} and Gogny}(1980)}]{dec80}
\bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Decharg{\'e}}}
  \bibnamefont{and} \bibinfo{author}{\bibfnamefont{D.}~\bibnamefont{Gogny}},
  \bibinfo{journal}{Phys. Rev. C} \textbf{\bibinfo{volume}{21}},
  \bibinfo{pages}{1568} (\bibinfo{year}{1980}).

\bibitem[{\citenamefont{Bertsch et~al.}(1997)\citenamefont{Bertsch, Borysowicz,
  McManus, and Love}}]{ber97}
\bibinfo{author}{\bibfnamefont{G.}~\bibnamefont{Bertsch}},
  \bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Borysowicz}},
  \bibinfo{author}{\bibfnamefont{H.}~\bibnamefont{McManus}}, \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{W.}~\bibnamefont{Love}},
  \bibinfo{journal}{Nucl. Phys. A} \textbf{\bibinfo{volume}{284}},
  \bibinfo{pages}{399} (\bibinfo{year}{1997}).

\bibitem[{\citenamefont{Cao et~al.}(2010)\citenamefont{Cao, Col\`o, and
  Sagawa}}]{cao10}
\bibinfo{author}{\bibfnamefont{L.-G.} \bibnamefont{Cao}},
  \bibinfo{author}{\bibfnamefont{G.}~\bibnamefont{Col\`o}}, \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{H.}~\bibnamefont{Sagawa}},
  \bibinfo{journal}{Phys. Rev. C} \textbf{\bibinfo{volume}{81}},
  \bibinfo{pages}{044302} (\bibinfo{year}{2010}).

\bibitem[{\citenamefont{Navarro and Polls}(2013)}]{nav13}
\bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Navarro}} \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{A.}~\bibnamefont{Polls}},
  \bibinfo{journal}{Phys. Rev. C} \textbf{\bibinfo{volume}{87}},
  \bibinfo{pages}{044329} (\bibinfo{year}{2013}).

\bibitem[{\citenamefont{Pastore et~al.}(2015)\citenamefont{Pastore, Davesne,
  and Navarro}}]{pas15}
\bibinfo{author}{\bibfnamefont{A.}~\bibnamefont{Pastore}},
  \bibinfo{author}{\bibfnamefont{D.}~\bibnamefont{Davesne}}, \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Navarro}},
  \bibinfo{journal}{Phys. Reports} \textbf{\bibinfo{volume}{563}},
  \bibinfo{pages}{1} (\bibinfo{year}{2015}).

\bibitem[{\citenamefont{Davesne
  et~al.}(2015{\natexlab{a}})\citenamefont{Davesne, Meyer, Pastore, and
  Navarro}}]{dav14P}
\bibinfo{author}{\bibfnamefont{D.}~\bibnamefont{Davesne}},
  \bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Meyer}},
  \bibinfo{author}{\bibfnamefont{A.}~\bibnamefont{Pastore}}, \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Navarro}},
  \bibinfo{journal}{Phys. Scr.} \textbf{\bibinfo{volume}{90}},
  \bibinfo{pages}{114002} (\bibinfo{year}{2015}{\natexlab{a}}).

\bibitem[{\citenamefont{Davesne
  et~al.}(2015{\natexlab{b}})\citenamefont{Davesne, Navarro, Becker, and
  Pastore}}]{dav15prep}
\bibinfo{author}{\bibfnamefont{D.}~\bibnamefont{Davesne}},
  \bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Navarro}},
  \bibinfo{author}{\bibfnamefont{P.}~\bibnamefont{Becker}}, \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{A.}~\bibnamefont{Pastore}},
  \bibinfo{journal}{in preparation}  (\bibinfo{year}{2015}{\natexlab{b}}).

\bibitem[{\citenamefont{Pastore et~al.}(2014)\citenamefont{Pastore, Davesne,
  and Navarro}}]{pas14L}
\bibinfo{author}{\bibfnamefont{A.}~\bibnamefont{Pastore}},
  \bibinfo{author}{\bibfnamefont{D.}~\bibnamefont{Davesne}}, \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Navarro}},
  \bibinfo{journal}{J. Phys. G: Nucl. Part. Phys.}
  \textbf{\bibinfo{volume}{41}}, \bibinfo{pages}{055103}
  (\bibinfo{year}{2014}).

\bibitem[{\citenamefont{Davesne
  et~al.}(2015{\natexlab{c}})\citenamefont{Davesne, Navarro, Becker, Jodon,
  Meyer, and Pastore}}]{Dav15}
\bibinfo{author}{\bibfnamefont{D.}~\bibnamefont{Davesne}},
  \bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Navarro}},
  \bibinfo{author}{\bibfnamefont{P.}~\bibnamefont{Becker}},
  \bibinfo{author}{\bibfnamefont{R.}~\bibnamefont{Jodon}},
  \bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Meyer}}, \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{A.}~\bibnamefont{Pastore}},
  \bibinfo{journal}{Phys. Rev. C} \textbf{\bibinfo{volume}{91}},
  \bibinfo{pages}{064303} (\bibinfo{year}{2015}{\natexlab{c}}).

\bibitem[{\citenamefont{Carlsson et~al.}(2008)\citenamefont{Carlsson,
  Dobaczewski, and Kortelainen}}]{car08}
\bibinfo{author}{\bibfnamefont{B.~G.} \bibnamefont{Carlsson}},
  \bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Dobaczewski}},
  \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{M.}~\bibnamefont{Kortelainen}},
  \bibinfo{journal}{Phys. Rev. C} \textbf{\bibinfo{volume}{78}},
  \bibinfo{pages}{044326} (\bibinfo{year}{2008}).

\bibitem[{\citenamefont{Raimondi et~al.}(2011)\citenamefont{Raimondi, Carlsson,
  and Dobaczewski}}]{rai11}
\bibinfo{author}{\bibfnamefont{F.}~\bibnamefont{Raimondi}},
  \bibinfo{author}{\bibfnamefont{B.~G.} \bibnamefont{Carlsson}},
  \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Dobaczewski}},
  \bibinfo{journal}{Phys. Rev. C} \textbf{\bibinfo{volume}{83}},
  \bibinfo{pages}{054311} (\bibinfo{year}{2011}).

\bibitem[{\citenamefont{Davesne
  et~al.}(2014{\natexlab{a}})\citenamefont{Davesne, Pastore, and
  Navarro}}]{dav14a}
\bibinfo{author}{\bibfnamefont{D.}~\bibnamefont{Davesne}},
  \bibinfo{author}{\bibfnamefont{A.}~\bibnamefont{Pastore}}, \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Navarro}},
  \bibinfo{journal}{J. Phys. G: Nucl. Part. Phys.}
  \textbf{\bibinfo{volume}{40}}, \bibinfo{pages}{095104}
  (\bibinfo{year}{2014}{\natexlab{a}}).

\bibitem[{\citenamefont{Davesne
  et~al.}(2014{\natexlab{b}})\citenamefont{Davesne, Pastore, and
  Navarro}}]{dav14b}
\bibinfo{author}{\bibfnamefont{D.}~\bibnamefont{Davesne}},
  \bibinfo{author}{\bibfnamefont{A.}~\bibnamefont{Pastore}}, \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Navarro}},
  \bibinfo{journal}{J. Phys. G: Nucl. Part. Phys.}
  \textbf{\bibinfo{volume}{41}}, \bibinfo{pages}{065104}
  (\bibinfo{year}{2014}{\natexlab{b}}).

\bibitem[{\citenamefont{Grasso and Anguiano}(2013)}]{gra13}
\bibinfo{author}{\bibfnamefont{M.}~\bibnamefont{Grasso}} \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{M.}~\bibnamefont{Anguiano}},
  \bibinfo{journal}{Phys. Rev. C} \textbf{\bibinfo{volume}{88}},
  \bibinfo{pages}{054328} (\bibinfo{year}{2013}).

\bibitem[{\citenamefont{Nakada}(2003)}]{nak03}
\bibinfo{author}{\bibfnamefont{H.}~\bibnamefont{Nakada}},
  \bibinfo{journal}{Phys. Rev. C} \textbf{\bibinfo{volume}{68}},
  \bibinfo{pages}{014316} (\bibinfo{year}{2003}).

\bibitem[{\citenamefont{Baldo et~al.}(1997)\citenamefont{Baldo, Bombaci, and
  Burgio}}]{bal97}
\bibinfo{author}{\bibfnamefont{M.}~\bibnamefont{Baldo}},
  \bibinfo{author}{\bibfnamefont{I.}~\bibnamefont{Bombaci}}, \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{G.}~\bibnamefont{Burgio}},
  \bibinfo{journal}{Astron. Astrophys.} \textbf{\bibinfo{volume}{328}},
  \bibinfo{pages}{274} (\bibinfo{year}{1997}).

\bibitem[{\citenamefont{Hebeler et~al.}(2011)\citenamefont{Hebeler, Bogner,
  Furnstahl, Nogga, and Schwenk}}]{heb11}
\bibinfo{author}{\bibfnamefont{K.}~\bibnamefont{Hebeler}},
  \bibinfo{author}{\bibfnamefont{S.}~\bibnamefont{Bogner}},
  \bibinfo{author}{\bibfnamefont{R.}~\bibnamefont{Furnstahl}},
  \bibinfo{author}{\bibfnamefont{A.}~\bibnamefont{Nogga}}, \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{A.}~\bibnamefont{Schwenk}},
  \bibinfo{journal}{Phys. Rev. C} \textbf{\bibinfo{volume}{83}},
  \bibinfo{pages}{031301 (R)} (\bibinfo{year}{2011}).

\bibitem[{\citenamefont{Bogner et~al.}(2005)\citenamefont{Bogner, Schwenk,
  Furnstahl, and Nogga}}]{bog05}
\bibinfo{author}{\bibfnamefont{S.}~\bibnamefont{Bogner}},
  \bibinfo{author}{\bibfnamefont{A.}~\bibnamefont{Schwenk}},
  \bibinfo{author}{\bibfnamefont{R.}~\bibnamefont{Furnstahl}},
  \bibnamefont{and} \bibinfo{author}{\bibfnamefont{A.}~\bibnamefont{Nogga}},
  \bibinfo{journal}{Nucl. Phys. A} \textbf{\bibinfo{volume}{763}},
  \bibinfo{pages}{59} (\bibinfo{year}{2005}).

\bibitem[{\citenamefont{Rotival}(2008)}]{rot08}
\bibinfo{author}{\bibfnamefont{V.}~\bibnamefont{Rotival}}, Ph.D. thesis,
  \bibinfo{school}{Universit\'e Paris-Diderot - Paris VII}
  (\bibinfo{year}{2008}).

\bibitem[{\citenamefont{Sadoudi et~al.}(2013)\citenamefont{Sadoudi, Duguet,
  Meyer, and Bender}}]{sad13}
\bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Sadoudi}},
  \bibinfo{author}{\bibfnamefont{T.}~\bibnamefont{Duguet}},
  \bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Meyer}}, \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{M.}~\bibnamefont{Bender}},
  \bibinfo{journal}{Phys. Rev. C} \textbf{\bibinfo{volume}{88}},
  \bibinfo{pages}{064326} (\bibinfo{year}{2013}),
  \urlprefix\url{http://link.aps.org/doi/10.1103/PhysRevC.88.064326}.

\bibitem[{\citenamefont{Benhar et~al.}(2013)\citenamefont{Benhar, Cipollone,
  and Loreti}}]{ben13}
\bibinfo{author}{\bibfnamefont{O.}~\bibnamefont{Benhar}},
  \bibinfo{author}{\bibfnamefont{A.}~\bibnamefont{Cipollone}},
  \bibnamefont{and} \bibinfo{author}{\bibfnamefont{A.}~\bibnamefont{Loreti}},
  \bibinfo{journal}{Phys. Rev. C} \textbf{\bibinfo{volume}{87}},
  \bibinfo{pages}{014601} (\bibinfo{year}{2013}).

\bibitem[{\citenamefont{Holt et~al.}(2013)\citenamefont{Holt, Kaiser, and
  Weise}}]{hol13}
\bibinfo{author}{\bibfnamefont{J.~W.} \bibnamefont{Holt}},
  \bibinfo{author}{\bibfnamefont{N.}~\bibnamefont{Kaiser}}, \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{W.}~\bibnamefont{Weise}},
  \bibinfo{journal}{Phys. Rev. C} \textbf{\bibinfo{volume}{87}},
  \bibinfo{pages}{014338} (\bibinfo{year}{2013}).

\bibitem[{\citenamefont{Dobaczewski et~al.}(2010)\citenamefont{Dobaczewski,
  Carlsson, and Kortelainen}}]{dob10}
\bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Dobaczewski}},
  \bibinfo{author}{\bibfnamefont{B.~G.} \bibnamefont{Carlsson}},
  \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{M.}~\bibnamefont{Kortelainen}},
  \bibinfo{journal}{J. Phys. G: Nucl. Part. Phys.}
  \textbf{\bibinfo{volume}{37}}, \bibinfo{pages}{075106}
  (\bibinfo{year}{2010}).

\bibitem[{\citenamefont{Davesne et~al.}(2016)\citenamefont{Davesne, Pastore,
  and Navarro}}]{dav16}
\bibinfo{author}{\bibfnamefont{D.}~\bibnamefont{Davesne}},
  \bibinfo{author}{\bibfnamefont{A.}~\bibnamefont{Pastore}}, \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{J.}~\bibnamefont{Navarro}},
  \bibinfo{journal}{Astronomy \& Astrophysics} \textbf{\bibinfo{volume}{585}},
  \bibinfo{pages}{A83} (\bibinfo{year}{2016}).

\bibitem[{\citenamefont{Abramowitz and Stegun}(1964)}]{abr64}
\bibinfo{author}{\bibfnamefont{M.}~\bibnamefont{Abramowitz}} \bibnamefont{and}
  \bibinfo{author}{\bibfnamefont{I.~A.} \bibnamefont{Stegun}},
  \emph{\bibinfo{title}{Handbook of mathematical functions: with formulas,
  graphs, and mathematical tables}}, \bibinfo{number}{55}
  (\bibinfo{publisher}{Courier Corporation}, \bibinfo{year}{1964}).

\end{thebibliography}
