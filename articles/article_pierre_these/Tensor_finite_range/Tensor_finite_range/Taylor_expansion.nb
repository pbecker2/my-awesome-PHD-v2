(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     26052,        912]
NotebookOptionsPosition[     23501,        820]
NotebookOutlinePosition[     23859,        836]
CellTagsIndexPosition[     23816,        833]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{
  RowBox[{"v", "[", "k_", "]"}], ":=", " ", 
  RowBox[{
   SuperscriptBox[
    RowBox[{"(", 
     RowBox[{
      RowBox[{"Sqrt", "[", "\[Pi]", "]"}], " ", "\[Mu]"}], ")"}], "3"], 
   RowBox[{"Exp", "[", 
    RowBox[{
     RowBox[{"-", 
      SuperscriptBox["\[Mu]", "2"]}], 
     RowBox[{
      SuperscriptBox[
       RowBox[{"(", "k", ")"}], "2"], "/", "4"}]}], "]"}]}]}]], "Input",
 CellChangeTimes->{{3.656396356234326*^9, 3.656396421686689*^9}, {
  3.656396740082073*^9, 3.656396740668593*^9}, {3.657516756156725*^9, 
  3.657516772466475*^9}, {3.657516808511146*^9, 3.657516816993042*^9}}],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.6575167744821577`*^9, 3.657516804621194*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"expansion", "=", 
  RowBox[{"Series", "[", 
   RowBox[{
    RowBox[{"v", "[", "k", "]"}], ",", 
    RowBox[{"{", 
     RowBox[{"k", ",", "0", ",", "6"}], "}"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.657516821850132*^9, 3.657516842352275*^9}, {
  3.657516878256988*^9, 3.657516880145234*^9}}],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
   RowBox[{
    SuperscriptBox["\[Pi]", 
     RowBox[{"3", "/", "2"}]], " ", 
    SuperscriptBox["\[Mu]", "3"]}], "-", 
   RowBox[{
    FractionBox["1", "4"], " ", 
    RowBox[{"(", 
     RowBox[{
      SuperscriptBox["\[Pi]", 
       RowBox[{"3", "/", "2"}]], " ", 
      SuperscriptBox["\[Mu]", "5"]}], ")"}], " ", 
    SuperscriptBox["k", "2"]}], "+", 
   RowBox[{
    FractionBox["1", "32"], " ", 
    SuperscriptBox["\[Pi]", 
     RowBox[{"3", "/", "2"}]], " ", 
    SuperscriptBox["\[Mu]", "7"], " ", 
    SuperscriptBox["k", "4"]}], "-", 
   RowBox[{
    FractionBox["1", "384"], " ", 
    RowBox[{"(", 
     RowBox[{
      SuperscriptBox["\[Pi]", 
       RowBox[{"3", "/", "2"}]], " ", 
      SuperscriptBox["\[Mu]", "9"]}], ")"}], " ", 
    SuperscriptBox["k", "6"]}], "+", 
   InterpretationBox[
    SuperscriptBox[
     RowBox[{"O", "[", "k", "]"}], "7"],
    SeriesData[$CellContext`k, 0, {}, 0, 7, 1],
    Editable->False]}],
  SeriesData[$CellContext`k, 0, {
   Pi^Rational[3, 2] $CellContext`\[Mu]^3, 0, Rational[-1, 4] 
    Pi^Rational[3, 2] $CellContext`\[Mu]^5, 0, Rational[1, 32] 
    Pi^Rational[3, 2] $CellContext`\[Mu]^7, 0, Rational[-1, 384] 
    Pi^Rational[3, 2] $CellContext`\[Mu]^9}, 0, 7, 1],
  Editable->False]], "Output",
 CellChangeTimes->{{3.657516830245173*^9, 3.657516842861824*^9}, 
   3.6575168806523542`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData["expansion"], "Input",
 CellChangeTimes->{{3.657516882264118*^9, 3.6575169019529448`*^9}, 
   3.657517998471991*^9}],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
   RowBox[{
    SuperscriptBox["\[Pi]", 
     RowBox[{"3", "/", "2"}]], " ", 
    SuperscriptBox["\[Mu]", "3"]}], "-", 
   RowBox[{
    FractionBox["1", "4"], " ", 
    RowBox[{"(", 
     RowBox[{
      SuperscriptBox["\[Pi]", 
       RowBox[{"3", "/", "2"}]], " ", 
      SuperscriptBox["\[Mu]", "5"]}], ")"}], " ", 
    SuperscriptBox["k", "2"]}], "+", 
   RowBox[{
    FractionBox["1", "32"], " ", 
    SuperscriptBox["\[Pi]", 
     RowBox[{"3", "/", "2"}]], " ", 
    SuperscriptBox["\[Mu]", "7"], " ", 
    SuperscriptBox["k", "4"]}], "-", 
   RowBox[{
    FractionBox["1", "384"], " ", 
    RowBox[{"(", 
     RowBox[{
      SuperscriptBox["\[Pi]", 
       RowBox[{"3", "/", "2"}]], " ", 
      SuperscriptBox["\[Mu]", "9"]}], ")"}], " ", 
    SuperscriptBox["k", "6"]}], "+", 
   InterpretationBox[
    SuperscriptBox[
     RowBox[{"O", "[", "k", "]"}], "7"],
    SeriesData[$CellContext`k, 0, {}, 0, 7, 1],
    Editable->False]}],
  SeriesData[$CellContext`k, 0, {
   Pi^Rational[3, 2] $CellContext`\[Mu]^3, 0, Rational[-1, 4] 
    Pi^Rational[3, 2] $CellContext`\[Mu]^5, 0, Rational[1, 32] 
    Pi^Rational[3, 2] $CellContext`\[Mu]^7, 0, Rational[-1, 384] 
    Pi^Rational[3, 2] $CellContext`\[Mu]^9}, 0, 7, 1],
  Editable->False]], "Output",
 CellChangeTimes->{3.657516902477675*^9, 3.657517998869524*^9}]
}, Open  ]],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.6575176365869427`*^9, 3.6575176615169067`*^9}}],

Cell[BoxData[
 RowBox[{"(*", " ", 
  RowBox[{"first", " ", "term"}]}]], "Input",
 CellChangeTimes->{{3.6575169093228693`*^9, 3.6575169140611553`*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Expand", "[", 
  SuperscriptBox[
   RowBox[{"(", 
    RowBox[{"k", "-", "kp"}], ")"}], "2"], "]"}]], "Input",
 CellChangeTimes->{{3.657516917811558*^9, 3.657516940799687*^9}}],

Cell[BoxData[
 RowBox[{
  SuperscriptBox["k", "2"], "-", 
  RowBox[{"2", " ", "k", " ", "kp"}], "+", 
  SuperscriptBox["kp", "2"]}]], "Output",
 CellChangeTimes->{{3.657516934755249*^9, 3.657516941097102*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"FullSimplify", "[", 
  RowBox[{
   RowBox[{"Expand", "[", 
    SuperscriptBox[
     RowBox[{"(", 
      RowBox[{"k", "-", "kp"}], ")"}], "4"], "]"}], "-", 
   SuperscriptBox[
    RowBox[{"(", 
     RowBox[{
      SuperscriptBox["k", "2"], "+", 
      SuperscriptBox["kp", "2"]}], ")"}], "2"], "-", 
   RowBox[{"4", " ", 
    SuperscriptBox[
     RowBox[{"(", 
      RowBox[{"k", " ", "kp"}], ")"}], "2"]}], "+", 
   RowBox[{"4", " ", 
    RowBox[{"(", 
     RowBox[{"k", " ", "kp"}], ")"}], " ", 
    RowBox[{"(", 
     RowBox[{
      SuperscriptBox["k", "2"], "+", 
      SuperscriptBox["kp", "2"]}], ")"}]}]}], "]"}]], "Input",
 CellChangeTimes->{{3.657517042756727*^9, 3.65751711471834*^9}}],

Cell[BoxData["0"], "Output",
 CellChangeTimes->{
  3.657517055461091*^9, {3.657517085839381*^9, 3.657517115148726*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"FullSimplify", "[", 
  RowBox[{
   RowBox[{"Expand", "[", 
    SuperscriptBox[
     RowBox[{"(", 
      RowBox[{"k", "-", "kp"}], ")"}], "6"], "]"}], "-", 
   RowBox[{
    RowBox[{"(", 
     RowBox[{
      SuperscriptBox["kp", "2"], "+", 
      SuperscriptBox["k", "2"]}], ")"}], 
    RowBox[{"(", 
     RowBox[{
      SuperscriptBox[
       RowBox[{"(", 
        RowBox[{
         SuperscriptBox["kp", "2"], "+", 
         SuperscriptBox["k", "2"]}], ")"}], "2"], "+", 
      RowBox[{"12", 
       SuperscriptBox[
        RowBox[{"(", 
         RowBox[{"k", " ", "kp"}], ")"}], "2"]}]}], ")"}]}], "+", 
   RowBox[{"2", " ", "kp", " ", "k", 
    RowBox[{"(", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"3", 
        SuperscriptBox[
         RowBox[{"(", 
          RowBox[{
           SuperscriptBox["kp", "2"], "+", 
           SuperscriptBox["k", "2"]}], ")"}], "2"]}], "+", 
       RowBox[{"4", 
        SuperscriptBox[
         RowBox[{"(", 
          RowBox[{"k", " ", "kp"}], ")"}], "2"]}]}], ")"}], ")"}]}]}], 
  "]"}]], "Input",
 CellChangeTimes->{{3.657517225326025*^9, 3.657517291905353*^9}, {
  3.65751732407834*^9, 3.657517351373926*^9}}],

Cell[BoxData["0"], "Output",
 CellChangeTimes->{3.657517293661615*^9, 3.657517352094095*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"aaa", "=", 
  RowBox[{
   RowBox[{
    RowBox[{
     SuperscriptBox["\[Pi]", 
      RowBox[{"3", "/", "2"}]], " ", 
     SuperscriptBox["\[Mu]", "3"]}], "-", 
    RowBox[{
     FractionBox["1", "4"], " ", 
     RowBox[{"(", 
      RowBox[{
       SuperscriptBox["\[Pi]", 
        RowBox[{"3", "/", "2"}]], " ", 
       SuperscriptBox["\[Mu]", "5"]}], ")"}], " ", 
     SuperscriptBox["k", "2"]}], "+", 
    RowBox[{
     FractionBox["1", "32"], " ", 
     SuperscriptBox["\[Pi]", 
      RowBox[{"3", "/", "2"}]], " ", 
     SuperscriptBox["\[Mu]", "7"], " ", 
     SuperscriptBox["k", "4"]}], "-", 
    RowBox[{
     FractionBox["1", "384"], " ", 
     RowBox[{"(", 
      RowBox[{
       SuperscriptBox["\[Pi]", 
        RowBox[{"3", "/", "2"}]], " ", 
       SuperscriptBox["\[Mu]", "9"]}], ")"}], " ", 
     SuperscriptBox["k", "6"]}]}], "/.", 
   RowBox[{"k", "\[Rule]", 
    RowBox[{"k", "-", "kp"}]}]}]}]], "Input",
 CellChangeTimes->{{3.657517954191868*^9, 3.657517965292617*^9}, 
   3.657518009951707*^9}],

Cell[BoxData[
 RowBox[{
  RowBox[{
   SuperscriptBox["\[Pi]", 
    RowBox[{"3", "/", "2"}]], " ", 
   SuperscriptBox["\[Mu]", "3"]}], "-", 
  RowBox[{
   FractionBox["1", "4"], " ", 
   SuperscriptBox[
    RowBox[{"(", 
     RowBox[{"k", "-", "kp"}], ")"}], "2"], " ", 
   SuperscriptBox["\[Pi]", 
    RowBox[{"3", "/", "2"}]], " ", 
   SuperscriptBox["\[Mu]", "5"]}], "+", 
  RowBox[{
   FractionBox["1", "32"], " ", 
   SuperscriptBox[
    RowBox[{"(", 
     RowBox[{"k", "-", "kp"}], ")"}], "4"], " ", 
   SuperscriptBox["\[Pi]", 
    RowBox[{"3", "/", "2"}]], " ", 
   SuperscriptBox["\[Mu]", "7"]}], "-", 
  RowBox[{
   FractionBox["1", "384"], " ", 
   SuperscriptBox[
    RowBox[{"(", 
     RowBox[{"k", "-", "kp"}], ")"}], "6"], " ", 
   SuperscriptBox["\[Pi]", 
    RowBox[{"3", "/", "2"}]], " ", 
   SuperscriptBox["\[Mu]", "9"]}]}]], "Output",
 CellChangeTimes->{3.65751796618392*^9, 3.657518010416883*^9}]
}, Open  ]],

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{"(*", " ", "t0"}]}]], "Input",
 CellChangeTimes->{{3.657518515611718*^9, 3.657518517186001*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"t0", "=", 
  RowBox[{
   SuperscriptBox["\[Pi]", 
    RowBox[{"3", "/", "2"}]], " ", 
   SuperscriptBox["\[Mu]", "3"]}]}]], "Input",
 CellChangeTimes->{{3.657517986404046*^9, 3.657518019499199*^9}}],

Cell[BoxData[
 RowBox[{
  SuperscriptBox["\[Pi]", 
   RowBox[{"3", "/", "2"}]], " ", 
  SuperscriptBox["\[Mu]", "3"]}]], "Output",
 CellChangeTimes->{3.657518020121436*^9}]
}, Open  ]],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.6575181732726917`*^9, 3.657518176388344*^9}}],

Cell[BoxData[
 RowBox[{"(*", 
  RowBox[{"t1", " ", "t2"}]}]], "Input",
 CellChangeTimes->{{3.657518521950433*^9, 3.657518527298877*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Collect", "[", 
  RowBox[{
   RowBox[{"Expand", "[", 
    RowBox[{
     RowBox[{"-", 
      FractionBox["1", "4"]}], " ", 
     SuperscriptBox[
      RowBox[{"(", 
       RowBox[{"k", "-", "kp"}], ")"}], "2"], " ", 
     SuperscriptBox["\[Pi]", 
      RowBox[{"3", "/", "2"}]], " ", "\[Mu]"}], "]"}], ",", 
   RowBox[{"{", 
    RowBox[{
     SuperscriptBox["k", "2"], ",", 
     SuperscriptBox["kp", "2"]}], "}"}], ",", "Simplify"}], "]"}]], "Input",
 CellChangeTimes->{{3.657518021963204*^9, 3.6575180726336813`*^9}, {
  3.65751814050069*^9, 3.657518141972603*^9}, {3.657518260971191*^9, 
  3.657518348607381*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"-", 
    FractionBox["1", "4"]}], " ", 
   SuperscriptBox["k", "2"], " ", 
   SuperscriptBox["\[Pi]", 
    RowBox[{"3", "/", "2"}]], " ", "\[Mu]"}], "+", 
  RowBox[{
   FractionBox["1", "2"], " ", "k", " ", "kp", " ", 
   SuperscriptBox["\[Pi]", 
    RowBox[{"3", "/", "2"}]], " ", "\[Mu]"}], "-", 
  RowBox[{
   FractionBox["1", "4"], " ", 
   SuperscriptBox["kp", "2"], " ", 
   SuperscriptBox["\[Pi]", 
    RowBox[{"3", "/", "2"}]], " ", "\[Mu]"}]}]], "Output",
 CellChangeTimes->{{3.657518320427638*^9, 3.65751834888521*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Solve", "[", " ", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{"t1", "/", "2"}], " ", 
     RowBox[{"(", 
      RowBox[{
       SuperscriptBox["k", "2"], "+", 
       SuperscriptBox["kp", "2"]}], ")"}]}], "==", 
    RowBox[{
     RowBox[{
      RowBox[{"-", 
       FractionBox["1", "4"]}], " ", 
      SuperscriptBox["k", "2"], " ", 
      SuperscriptBox["\[Pi]", 
       RowBox[{"3", "/", "2"}]], " ", "\[Mu]"}], "-", 
     RowBox[{
      FractionBox["1", "4"], " ", 
      SuperscriptBox["kp", "2"], " ", 
      SuperscriptBox["\[Pi]", 
       RowBox[{"3", "/", "2"}]], " ", "\[Mu]"}]}]}], ",", "t1"}], 
  "]"}]], "Input",
 CellChangeTimes->{{3.657518415910264*^9, 3.657518469263646*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"{", 
   RowBox[{"t1", "\[Rule]", 
    RowBox[{
     RowBox[{"-", 
      FractionBox["1", "2"]}], " ", 
     SuperscriptBox["\[Pi]", 
      RowBox[{"3", "/", "2"}]], " ", "\[Mu]"}]}], "}"}], "}"}]], "Output",
 CellChangeTimes->{{3.6575184447525053`*^9, 3.657518469843782*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Solve", "[", " ", 
  RowBox[{
   RowBox[{
    RowBox[{"t2", 
     RowBox[{"(", 
      RowBox[{"k", " ", "kp"}], " ", ")"}]}], "==", 
    RowBox[{
     RowBox[{"+", 
      FractionBox["1", "2"]}], " ", "k", " ", "kp", " ", 
     SuperscriptBox["\[Pi]", 
      RowBox[{"3", "/", "2"}]], " ", "\[Mu]"}]}], ",", "t2"}], "]"}]], "Input",\

 CellChangeTimes->{{3.657518476005595*^9, 3.657518500341927*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"{", 
   RowBox[{"t2", "\[Rule]", 
    RowBox[{
     FractionBox["1", "2"], " ", 
     SuperscriptBox["\[Pi]", 
      RowBox[{"3", "/", "2"}]], " ", "\[Mu]"}]}], "}"}], "}"}]], "Output",
 CellChangeTimes->{3.6575185008003607`*^9}]
}, Open  ]],

Cell[BoxData[
 RowBox[{"(*", " ", 
  RowBox[{"t14", " ", "t24"}]}]], "Input",
 CellChangeTimes->{{3.657518530512719*^9, 3.657518538790453*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Simplify", "[", 
  RowBox[{
   RowBox[{"Expand", "[", 
    RowBox[{
     RowBox[{
      RowBox[{"+", 
       FractionBox["1", "32"]}], " ", 
      SuperscriptBox["\[Pi]", 
       RowBox[{"3", "/", "2"}]], " ", 
      SuperscriptBox["\[Mu]", "7"], " ", 
      SuperscriptBox["k", "4"]}], "/.", 
     RowBox[{"k", "\[Rule]", " ", 
      RowBox[{"k", "-", "kp"}]}]}], "]"}], "-", 
   RowBox[{
    FractionBox["1", "32"], " ", 
    SuperscriptBox["\[Pi]", 
     RowBox[{"3", "/", "2"}]], " ", 
    SuperscriptBox["\[Mu]", "7"], 
    RowBox[{"(", 
     RowBox[{
      SuperscriptBox[
       RowBox[{"(", 
        RowBox[{
         SuperscriptBox["k", "2"], "+", 
         SuperscriptBox["kp", "2"]}], ")"}], "2"], "-", 
      RowBox[{"4", " ", 
       SuperscriptBox[
        RowBox[{"(", 
         RowBox[{"k", " ", "kp"}], ")"}], "2"]}]}], ")"}]}]}], "]"}]], "Input",\

 CellChangeTimes->{{3.6575185540097523`*^9, 3.6575185673055162`*^9}, {
  3.657518621501871*^9, 3.657518657259864*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"-", 
   FractionBox["1", "8"]}], " ", "k", " ", 
  SuperscriptBox[
   RowBox[{"(", 
    RowBox[{"k", "-", "kp"}], ")"}], "2"], " ", "kp", " ", 
  SuperscriptBox["\[Pi]", 
   RowBox[{"3", "/", "2"}]], " ", 
  SuperscriptBox["\[Mu]", "7"]}]], "Output",
 CellChangeTimes->{
  3.65751856773009*^9, {3.657518651459888*^9, 3.6575186575569057`*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Solve", "[", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{"t14", "/", "4"}], " ", 
     RowBox[{"(", 
      RowBox[{
       SuperscriptBox[
        RowBox[{"(", 
         RowBox[{
          SuperscriptBox["k", "2"], "+", 
          SuperscriptBox["kp", "2"]}], ")"}], "2"], "-", 
       RowBox[{"4", " ", 
        SuperscriptBox[
         RowBox[{"(", 
          RowBox[{"k", " ", "kp"}], ")"}], "2"]}]}], ")"}]}], "==", 
    RowBox[{
     FractionBox["1", "32"], " ", 
     SuperscriptBox["\[Pi]", 
      RowBox[{"3", "/", "2"}]], " ", 
     SuperscriptBox["\[Mu]", "7"], 
     RowBox[{"(", 
      RowBox[{
       SuperscriptBox[
        RowBox[{"(", 
         RowBox[{
          SuperscriptBox["k", "2"], "+", 
          SuperscriptBox["kp", "2"]}], ")"}], "2"], "-", 
       RowBox[{"4", " ", 
        SuperscriptBox[
         RowBox[{"(", 
          RowBox[{"k", " ", "kp"}], ")"}], "2"]}]}], ")"}]}]}], ",", "t14"}], 
  "]"}]], "Input",
 CellChangeTimes->{{3.657518571093535*^9, 3.657518579050044*^9}, {
  3.657518673405025*^9, 3.657518682244521*^9}, {3.657518777683855*^9, 
  3.6575187796091003`*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"{", 
   RowBox[{"t14", "\[Rule]", 
    RowBox[{
     FractionBox["1", "8"], " ", 
     SuperscriptBox["\[Pi]", 
      RowBox[{"3", "/", "2"}]], " ", 
     SuperscriptBox["\[Mu]", "7"]}]}], "}"}], "}"}]], "Output",
 CellChangeTimes->{3.6575187799708023`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Solve", "[", 
  RowBox[{
   RowBox[{
    RowBox[{"t24", " ", 
     RowBox[{"(", " ", 
      RowBox[{"k", " ", 
       SuperscriptBox[
        RowBox[{"(", 
         RowBox[{"k", "-", "kp"}], ")"}], "2"], " ", "kp"}], " ", ")"}]}], "==", 
    RowBox[{
     RowBox[{"-", 
      FractionBox["1", "8"]}], " ", "k", " ", 
     SuperscriptBox[
      RowBox[{"(", 
       RowBox[{"k", "-", "kp"}], ")"}], "2"], " ", "kp", " ", 
     SuperscriptBox["\[Pi]", 
      RowBox[{"3", "/", "2"}]], " ", 
     SuperscriptBox["\[Mu]", "7"]}]}], ",", "t24"}], "]"}]], "Input",
 CellChangeTimes->{{3.6575187969936657`*^9, 3.657518798357836*^9}, {
  3.657518829676814*^9, 3.657518859527874*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"{", 
   RowBox[{"t24", "\[Rule]", 
    RowBox[{
     RowBox[{"-", 
      FractionBox["1", "8"]}], " ", 
     SuperscriptBox["\[Pi]", 
      RowBox[{"3", "/", "2"}]], " ", 
     SuperscriptBox["\[Mu]", "7"]}]}], "}"}], "}"}]], "Output",
 CellChangeTimes->{3.65751885990208*^9}]
}, Open  ]],

Cell[BoxData[
 RowBox[{"(*", " ", 
  RowBox[{
  "t16", " ", "t26", "\[IndentingNewLine]", 
   "\[IndentingNewLine]"}]}]], "Input",
 CellChangeTimes->{{3.657518901846717*^9, 3.6575189070066423`*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Simplify", "[", 
  RowBox[{
   RowBox[{"Expand", "[", 
    RowBox[{
     RowBox[{"-", 
      FractionBox["1", "384"]}], " ", 
     SuperscriptBox[
      RowBox[{"(", 
       RowBox[{"k", "-", "kp"}], ")"}], "6"], " ", 
     SuperscriptBox["\[Pi]", 
      RowBox[{"3", "/", "2"}]], " ", 
     SuperscriptBox["\[Mu]", "9"]}], "]"}], "+", 
   RowBox[{
    FractionBox["1", "384"], "  ", 
    SuperscriptBox["\[Pi]", 
     RowBox[{"3", "/", "2"}]], " ", 
    SuperscriptBox["\[Mu]", "9"], 
    RowBox[{"(", 
     RowBox[{
      SuperscriptBox["kp", "2"], "+", 
      SuperscriptBox["k", "2"]}], ")"}], 
    RowBox[{"(", 
     RowBox[{
      SuperscriptBox[
       RowBox[{"(", 
        RowBox[{
         SuperscriptBox["kp", "2"], "+", 
         SuperscriptBox["k", "2"]}], ")"}], "2"], "+", 
      RowBox[{"12", 
       SuperscriptBox[
        RowBox[{"(", 
         RowBox[{"k", " ", "kp"}], ")"}], "2"]}]}], ")"}]}]}], "]"}]], "Input",\

 CellChangeTimes->{{3.6575189118230667`*^9, 3.6575189771977*^9}}],

Cell[BoxData[
 RowBox[{
  FractionBox["1", "192"], " ", "k", " ", "kp", " ", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"3", " ", 
     SuperscriptBox["k", "4"]}], "+", 
    RowBox[{"10", " ", 
     SuperscriptBox["k", "2"], " ", 
     SuperscriptBox["kp", "2"]}], "+", 
    RowBox[{"3", " ", 
     SuperscriptBox["kp", "4"]}]}], ")"}], " ", 
  SuperscriptBox["\[Pi]", 
   RowBox[{"3", "/", "2"}]], " ", 
  SuperscriptBox["\[Mu]", "9"]}]], "Output",
 CellChangeTimes->{3.6575189775154247`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Simplify", "[", 
  RowBox[{
   RowBox[{"Expand", "[", 
    RowBox[{"2", " ", "kp", " ", "k", 
     RowBox[{"(", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"3", 
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{
            SuperscriptBox["kp", "2"], "+", 
            SuperscriptBox["k", "2"]}], ")"}], "2"]}], "+", 
        RowBox[{"4", 
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{"k", " ", "kp"}], ")"}], "2"]}]}], ")"}], ")"}]}], "]"}], 
   "/", 
   RowBox[{"(", 
    RowBox[{"2", " ", "k", " ", "kp"}], ")"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.657518996419222*^9, 3.65751902714277*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"3", " ", 
   SuperscriptBox["k", "4"]}], "+", 
  RowBox[{"10", " ", 
   SuperscriptBox["k", "2"], " ", 
   SuperscriptBox["kp", "2"]}], "+", 
  RowBox[{"3", " ", 
   SuperscriptBox["kp", "4"]}]}]], "Output",
 CellChangeTimes->{{3.657519002882522*^9, 3.6575190274154863`*^9}}]
}, Open  ]],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.657519033700286*^9, 3.657519034239148*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Solve", "[", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{"t16", "/", "2"}], 
     RowBox[{"(", 
      RowBox[{
       SuperscriptBox["kp", "2"], "+", 
       SuperscriptBox["k", "2"]}], ")"}], 
     RowBox[{"(", 
      RowBox[{
       SuperscriptBox[
        RowBox[{"(", 
         RowBox[{
          SuperscriptBox["kp", "2"], "+", 
          SuperscriptBox["k", "2"]}], ")"}], "2"], "+", 
       RowBox[{"12", 
        SuperscriptBox[
         RowBox[{"(", 
          RowBox[{"k", " ", "kp"}], ")"}], "2"]}]}], ")"}]}], "\[Equal]", 
    RowBox[{
     RowBox[{"-", 
      FractionBox["1", "384"]}], "  ", 
     SuperscriptBox["\[Pi]", 
      RowBox[{"3", "/", "2"}]], " ", 
     SuperscriptBox["\[Mu]", "9"], 
     RowBox[{"(", 
      RowBox[{
       SuperscriptBox["kp", "2"], "+", 
       SuperscriptBox["k", "2"]}], ")"}], 
     RowBox[{"(", 
      RowBox[{
       SuperscriptBox[
        RowBox[{"(", 
         RowBox[{
          SuperscriptBox["kp", "2"], "+", 
          SuperscriptBox["k", "2"]}], ")"}], "2"], "+", 
       RowBox[{"12", 
        SuperscriptBox[
         RowBox[{"(", 
          RowBox[{"k", " ", "kp"}], ")"}], "2"]}]}], ")"}]}]}], ",", "t16"}], 
  "]"}]], "Input",
 CellChangeTimes->{{3.657519039943446*^9, 3.657519061936861*^9}, 
   3.6575191181974277`*^9}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"{", 
   RowBox[{"t16", "\[Rule]", 
    RowBox[{
     RowBox[{"-", 
      FractionBox["1", "192"]}], " ", 
     SuperscriptBox["\[Pi]", 
      RowBox[{"3", "/", "2"}]], " ", 
     SuperscriptBox["\[Mu]", "9"]}]}], "}"}], "}"}]], "Output",
 CellChangeTimes->{3.657519062212532*^9, 3.6575191210147953`*^9}]
}, Open  ]],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.657519144471602*^9, 3.657519145884226*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Solve", "[", 
  RowBox[{
   RowBox[{
    RowBox[{"t26", " ", "kp", " ", "k", 
     RowBox[{"(", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"3", 
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{
            SuperscriptBox["kp", "2"], "+", 
            SuperscriptBox["k", "2"]}], ")"}], "2"]}], "+", 
        RowBox[{"4", 
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{"k", " ", "kp"}], ")"}], "2"]}]}], ")"}], ")"}]}], "==", 
    RowBox[{
     FractionBox["1", "192"], " ", "k", " ", "kp", " ", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"3", " ", 
        SuperscriptBox["k", "4"]}], "+", 
       RowBox[{"10", " ", 
        SuperscriptBox["k", "2"], " ", 
        SuperscriptBox["kp", "2"]}], "+", 
       RowBox[{"3", " ", 
        SuperscriptBox["kp", "4"]}]}], ")"}], " ", 
     SuperscriptBox["\[Pi]", 
      RowBox[{"3", "/", "2"}]], " ", 
     SuperscriptBox["\[Mu]", "9"]}]}], ",", "t26"}], "]"}]], "Input",
 CellChangeTimes->{{3.657519068681478*^9, 3.657519101083441*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"{", 
   RowBox[{"t26", "\[Rule]", 
    RowBox[{
     FractionBox["1", "192"], " ", 
     SuperscriptBox["\[Pi]", 
      RowBox[{"3", "/", "2"}]], " ", 
     SuperscriptBox["\[Mu]", "9"]}]}], "}"}], "}"}]], "Output",
 CellChangeTimes->{3.657519101527018*^9}]
}, Open  ]]
},
WindowSize->{Full, Full},
WindowMargins->{{228, Automatic}, {Automatic, 314}},
FrontEndVersion->"10.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (June 27, \
2014)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 617, 17, 35, "Input"],
Cell[1178, 39, 94, 1, 28, "Input"],
Cell[CellGroupData[{
Cell[1297, 44, 320, 8, 28, "Input"],
Cell[1620, 54, 1390, 41, 48, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3047, 100, 129, 2, 28, "Input"],
Cell[3179, 104, 1360, 40, 48, "Output"]
}, Open  ]],
Cell[4554, 147, 96, 1, 28, "Input"],
Cell[4653, 150, 150, 3, 28, "Input"],
Cell[CellGroupData[{
Cell[4828, 157, 199, 5, 35, "Input"],
Cell[5030, 164, 209, 5, 32, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[5276, 174, 718, 23, 36, "Input"],
Cell[5997, 199, 119, 2, 28, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6153, 206, 1181, 39, 36, "Input"],
Cell[7337, 247, 92, 1, 28, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[7466, 253, 1037, 33, 49, "Input"],
Cell[8506, 288, 917, 30, 48, "Output"]
}, Open  ]],
Cell[9438, 321, 153, 3, 46, "Input"],
Cell[CellGroupData[{
Cell[9616, 328, 222, 6, 32, "Input"],
Cell[9841, 336, 172, 5, 32, "Output"]
}, Open  ]],
Cell[10028, 344, 94, 1, 28, "Input"],
Cell[10125, 347, 136, 3, 28, "Input"],
Cell[CellGroupData[{
Cell[10286, 354, 639, 18, 48, "Input"],
Cell[10928, 374, 576, 17, 47, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[11541, 396, 720, 23, 48, "Input"],
Cell[12264, 421, 315, 9, 47, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[12616, 435, 424, 13, 48, "Input"],
Cell[13043, 450, 269, 8, 47, "Output"]
}, Open  ]],
Cell[13327, 461, 143, 3, 28, "Input"],
Cell[CellGroupData[{
Cell[13495, 468, 1009, 32, 49, "Input"],
Cell[14507, 502, 377, 11, 48, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[14921, 518, 1136, 36, 49, "Input"],
Cell[16060, 556, 297, 9, 48, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[16394, 570, 699, 20, 49, "Input"],
Cell[17096, 592, 316, 10, 48, "Output"]
}, Open  ]],
Cell[17427, 605, 198, 5, 63, "Input"],
Cell[CellGroupData[{
Cell[17650, 614, 1026, 34, 49, "Input"],
Cell[18679, 650, 491, 15, 48, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[19207, 670, 675, 21, 36, "Input"],
Cell[19885, 693, 310, 9, 32, "Output"]
}, Open  ]],
Cell[20210, 705, 92, 1, 28, "Input"],
Cell[CellGroupData[{
Cell[20327, 710, 1315, 44, 49, "Input"],
Cell[21645, 756, 343, 10, 48, "Output"]
}, Open  ]],
Cell[22003, 769, 92, 1, 28, "Input"],
Cell[CellGroupData[{
Cell[22120, 774, 1065, 32, 49, "Input"],
Cell[23188, 808, 297, 9, 48, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
