\begin{thebibliography}{10}
\expandafter\ifx\csname url\endcsname\relax
  \def\url#1{\texttt{#1}}\fi
\expandafter\ifx\csname urlprefix\endcsname\relax\def\urlprefix{URL }\fi
\expandafter\ifx\csname href\endcsname\relax
  \def\href#1#2{#2} \def\path#1{#1}\fi

\bibitem{ben03}
M.~Bender, P.-H. Heenen, P.-G. Reinhard, Rev. Mod. Phys. 75 (2003) 121.

\bibitem{sky59}
T.~H.~R. Skyrme, Nucl. Phys. 9 (1959) 615.

\bibitem{vau72}
D.~Vautherin, D.~M. Brink, Phys. Rev. C 5 (1972) 626.

\bibitem{gog75}
D.~Gogny, in {\it Nuclear self-consistent fields}, eds. G. Ripka and M.
  Porneuf, North Holland, 1975.

\bibitem{dec80}
J.~Decharg{\'e}, D.~Gogny, Phys. Rev. C 21 (1980) 1568.

\bibitem{ber77}
G.~Bertsch, J.~Borysowicz, H.~McManus, W.~Love, Nucl. Phys. A 284 (1977) 399.

\bibitem{nak03}
H.~Nakada, Phys. Rev. C 68 (2003) 014316.

\bibitem{gor09}
S.~Goriely, N.~Chamel, J.~M. Pearson, Phys. Rev. Lett. 102 (2009) 152503.

\bibitem{gor09b}
S.~Goriely, S.~Hilaire, M.~Girod, S.~P\'eru, Phys. Rev. Lett. 102 (2009)
  242501.

\bibitem{ber07}
G.~F. Bertsch, M.~Girod, S.~Hilaire, J.-P. Delaroche, H.~Goutte, S.~P\'eru,
  Phys. Rev. Lett. 99 (2007) 032502.

\bibitem{ter08}
J.~Terasaki, J.~Engel, G.~F. Bertsch, Phys. Rev. C 78 (2008) 044311.

\bibitem{dut12}
M.~Dutra, O.~Louren\ifmmode~\mbox{\c{c}}\else \c{c}\fi{}o, J.~S. S\'a~Martins,
  A.~Delfino, J.~R. Stone, P.~D. Stevenson, Phys. Rev. C 85 (2012) 035201.

\bibitem{bal97}
M.~Baldo, I.~Bombaci, G.~Burgio, Astron. Astrophys. 328 (1997) 274.

\bibitem{heb11}
K.~Hebeler, S.~K. Bogner, R.~J. Furnstahl, A.~Nogga, A.~Schwenk, Phys. Rev. C
  83 (2011) 031301.

\bibitem{gez13}
A.~Gezerlis, I.~Tews, E.~Epelbaum, S.~Gandolfi, K.~Hebeler, A.~Nogga,
  A.~Schwenk, Phys. Rev. Lett. 111 (2013) 032501.

\bibitem{bog05}
S.~Bogner, A.~Schwenk, R.~Furnstahl, A.~Nogga, Nucl. Phys. A 763 (2005) 59.

\bibitem{rot08}
V.~Rotival, {\it Fonctionnelles d'Energie non-empiriques pour la structure
  nucl\'eaire}, Ph.D. thesis, Universit\'e Paris-Diderot - Paris VII (2008).

\bibitem{Dav15}
D.~Davesne, J.~Navarro, P.~Becker, R.~Jodon, J.~Meyer, A.~Pastore, Phys. Rev. C
  91 (2015) 064303.

\bibitem{les07}
T.~Lesinski, M.~Bender, K.~Bennaceur, T.~Duguet, J.~Meyer, Phys. Rev. C 76
  (2007) 014312.

\bibitem{Ang11}
M.~Anguiano, G.~Co', V.~De~Donno, A.~M. Lallena, Phys. Rev. C 83 (2011) 064306.

\bibitem{fia02}
J.~O. Fiase, K.~R.~S. Devan, A.~Hosaka, Phys. Rev. C 66 (2002) 014004.

\bibitem{bal12}
M.~Baldo, A.~Polls, A.~Rios, H.-J. Schulze, I.~Vida\~na, Phys. Rev. C 86 (2012)
  064001.

\bibitem{sta77}
F.~Stancu, D.~Brink, H.~Flocard, Phys. Lett. B 68 (1977) 108.

\bibitem{oni78}
N.~Onishi, J.~Negele, Nucl. Phys. A 301 (1978) 336.

\bibitem{ots06}
T.~Otsuka, T.~Matsuo, D.~Abe, Phys. Rev. Lett. 97 (2006) 162501.

\bibitem{nak08}
H.~Nakada, Phys. Rev. C 78 (2008) 054301.

\bibitem{nak13}
H.~Nakada, Phys. Rev. C 87 (2013) 014336.

\bibitem{Ang12}
M.~Anguiano, M.~Grasso, G.~Co', V.~De~Donno, A.~M. Lallena, Phys. Rev. C 86
  (2012) 054302.

\bibitem{sag14}
H.~Sagawa, G.~Col\`o, Progr. Part. Nucl. Phys. 76 (2014) 76.

\bibitem{car08}
B.~G. Carlsson, J.~Dobaczewski, M.~Kortelainen, Phys. Rev. C 78 (2008) 044326.

\bibitem{rai11}
F.~Raimondi, B.~G. Carlsson, J.~Dobaczewski, Phys. Rev. C 83 (2011) 054311.

\bibitem{kor13}
M.~Kortelainen, J.~McDonnell, W.~Nazarewicz, E.~Olsen, P.-G. Reinhard,
  J.~Sarich, N.~Schunck, S.~M. Wild, D.~Davesne, J.~Erler, A.~Pastore, Phys.
  Rev. C 89 (2014) 054314.

\bibitem{car10}
B.~G. Carlsson, J.~Dobaczewski, Phys. Rev. Lett. 105 (2010) 122501.

\bibitem{Dav16PRC}
D.~Davesne, P.~Becker, A.~Pastore, J.~Navarro, Phys. Rev. C 93 (2016) 064001.

\bibitem{Gra13}
M.~Grasso, M.~Anguiano, Phys. Rev. C 88 (2013) 054328.

\bibitem{ber16}
R.~N. Bernard, M.~Anguiano, Nucl. Phys. A 953 (2016) 32.

\bibitem{cha08}
F.~Chappert, M.~Girod, S.~Hilaire, Phys. Lett. B 668 (2008) 420.

\bibitem{cha15}
F.~Chappert, N.~Pillet, M.~Girod, J.-F. Berger, Phys. Rev. C 91 (2015) 034312.

\bibitem{ana83}
N.~Anantaraman, H.~Toki, G.~Bertsch, Nucl. Phys. A 398 (1983) 269.

\bibitem{Nak15}
H.~Nakada, Phys. Rev. C 92 (2015) 044307.

\bibitem{dav14c}
D.~Davesne, A.~Pastore, J.~Navarro, J. Phys. G: Nucl. Part. Phys. 41 (2014)
  065104.

\bibitem{dob95}
J.~Dobaczewski, J.~Dudek, Phys. Rev. C 52 (1995) 1827.

\bibitem{cha97}
E.~Chabanat, P.~Bonche, P.~Haensel, J.~Meyer, R.~Schaeffer, Nucl. Phys. A 627
  (1997) 710.

\bibitem{Dav15AA}
D.~Davesne, A.~Pastore, J.~Navarro, Astron. Astrophys. 585 (2015) A83.

\bibitem{Dav16}
D.~Davesne, J.~Holt, A.~Pastore, J.~Navarro, Phys. Rev. C 91 (2016) 014323.

\bibitem{hol13}
J.~W. Holt, N.~Kaiser, W.~Weise, Phys. Rev. C 87 (2013) 014338.

\bibitem{sel14}
R.~Sellahewa, A.~Rios, Phys. Rev. C 90 (2014) 054327.

\bibitem{vid11}
I.~Vida\~na, A.~Polls, C.~m.~c. Provid\^encia, Phys. Rev. C 84 (2011) 062801.

\bibitem{dan02}
P.~Danielewicz, R.~Lacey, W.~G. Lynch, Science 298 (2002) 1592.

\bibitem{lyn09}
W.~Lynch, M.~Tsang, Y.~Zhang, P.~Danielewicz, M.~Famiano, Z.~Li, A.~Steiner,
  Progr. Part. Nucl. Phys. 62 (2009) 427.

\bibitem{bur07}
G.~Burgio, J. Phys. G: Nucl. Part. Phys. 35 (2007) 014048.

\bibitem{zho04}
X.~R. Zhou, G.~F. Burgio, U.~Lombardo, H.-J. Schulze, W.~Zuo, Phys. Rev. C 69
  (2004) 018801.

\bibitem{dem10}
P.~Demorest, T.~Pennucci, S.~Ransom, M.~Roberts, J.~Hessels, Nature 467 (2010)
  1081.

\bibitem{car03}
J.~Carlson, S.-Y. Chang, V.~R. Pandharipande, K.~E. Schmidt, Phys. Rev. Lett.
  91 (2003) 050401.

\bibitem{gor10}
S.~Goriely, N.~Chamel, J.~M. Pearson, Phys. Rev. C 82 (2010) 035804.

\bibitem{les06}
T.~Lesinski, K.~Bennaceur, T.~Duguet, J.~Meyer, Phys. Rev. C 74 (2006) 044315.

\bibitem{cha07}
F.~Chappert, {\it Nouvelles param\'etrisations de l'interaction nucl\'eaire
  effective de Gogny}, Ph.D. thesis, Universit\'e de Paris-Sud XI (2007).

\bibitem{dav14k}
D.~Davesne, J.~Meyer, A.~Pastore, J.~Navarro, Phys. Scr. 90 (2015) 114002.

\bibitem{hol72}
K.~Holinde, K.~Erkelenz, R.~Alzetta, Nuclear Physics A 194 (1972) 161.

\bibitem{hol10}
J.~Holt, N.~Kaiser, W.~Weise, Phys. Rev. C 81 (2010) 024002.

\bibitem{Pas14L}
A.~Pastore, D.~Davesne, J.~Navarro, J. Phys. G: Nucl. Part. Phys. 41~(5) (2014)
  055103.

\bibitem{prep}
A.~Pastore, D.~Davesne, J.~Navarro, Phys. Rep. 563 (2014) 1.

\bibitem{rai11b}
F.~Raimondi, B.~G. Carlsson, J.~Dobaczewski, J.~Toivanen, Phys. Rev. C 84
  (2011) 064303.

\bibitem{bla86}
J.-P. Blaizot, G.~Ripka, {\it Quantum theory of finite systems}, MIT press
  Cambridge, 1986.

\bibitem{bel59}
J.~Bell, T.~Skyrme, Phil. Mag. 1 (1956) 1055.

\bibitem{abr64}
M.~Abramowitz, I.~A. Stegun, {\it Handbook of Mathematical Functions}, Courier
  Corporation, 1964.

\end{thebibliography}
