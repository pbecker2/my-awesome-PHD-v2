(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      9211,        311]
NotebookOptionsPosition[      8218,        271]
NotebookOutlinePosition[      8573,        287]
CellTagsIndexPosition[      8530,        284]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{
  RowBox[{"v", "[", 
   RowBox[{"k_", ",", "kp_", ",", "x_"}], "]"}], ":=", " ", 
  RowBox[{
   SuperscriptBox[
    RowBox[{"(", 
     RowBox[{
      RowBox[{"Sqrt", "[", "\[Pi]", "]"}], " ", "\[Mu]"}], ")"}], "3"], 
   RowBox[{"Exp", "[", 
    RowBox[{
     RowBox[{"-", 
      SuperscriptBox["\[Mu]", "2"]}], 
     RowBox[{
      RowBox[{"(", 
       RowBox[{
        SuperscriptBox["k", "2"], "+", 
        SuperscriptBox["kp", "2"], "-", 
        RowBox[{"2", "k", " ", "kp", " ", "x"}]}], ")"}], "/", "4"}]}], 
    "]"}]}]}]], "Input",
 CellChangeTimes->{{3.656396356234326*^9, 3.656396421686689*^9}, {
  3.656396740082073*^9, 3.656396740668593*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"V1s0", "=", 
  RowBox[{"Simplify", "[", 
   RowBox[{
    RowBox[{"1", "/", "2"}], " ", 
    RowBox[{"Integrate", "[", 
     RowBox[{
      RowBox[{"v", "[", 
       RowBox[{"k", ",", "kp", ",", "x"}], "]"}], ",", 
      RowBox[{"{", 
       RowBox[{"x", ",", 
        RowBox[{"-", "1"}], ",", "1"}], "}"}]}], "]"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.656396708262724*^9, 3.656396759008092*^9}}],

Cell[BoxData[
 FractionBox[
  RowBox[{
   SuperscriptBox["\[ExponentialE]", 
    RowBox[{
     RowBox[{"-", 
      FractionBox["1", "4"]}], " ", 
     SuperscriptBox[
      RowBox[{"(", 
       RowBox[{"k", "+", "kp"}], ")"}], "2"], " ", 
     SuperscriptBox["\[Mu]", "2"]}]], " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"-", "1"}], "+", 
     SuperscriptBox["\[ExponentialE]", 
      RowBox[{"k", " ", "kp", " ", 
       SuperscriptBox["\[Mu]", "2"]}]]}], ")"}], " ", 
   SuperscriptBox["\[Pi]", 
    RowBox[{"3", "/", "2"}]], " ", "\[Mu]"}], 
  RowBox[{"k", " ", "kp"}]]], "Output",
 CellChangeTimes->{{3.656396733149741*^9, 3.656396759381097*^9}, 
   3.65639790107165*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"V1S0read", "=", 
  RowBox[{
   RowBox[{"\[Mu]", "/", 
    RowBox[{"(", " ", 
     RowBox[{"k", " ", "kp", " ", 
      RowBox[{"Sqrt", "[", "\[Pi]", "]"}]}], ")"}]}], " ", 
   RowBox[{"Exp", "[", 
    RowBox[{
     RowBox[{"-", 
      SuperscriptBox["\[Mu]", "2"]}], 
     RowBox[{
      RowBox[{"(", 
       RowBox[{
        SuperscriptBox["k", "2"], "+", 
        SuperscriptBox["kp", "2"]}], ")"}], "/", "4"}]}], "]"}], " ", 
   RowBox[{"Sinh", "[", 
    RowBox[{
     SuperscriptBox["\[Mu]", "2"], " ", "k", " ", 
     RowBox[{"kp", " ", "/", "2"}]}], "]"}]}]}]], "Input",
 CellChangeTimes->{{3.6563974229809713`*^9, 3.656397426524989*^9}, {
  3.656397648081419*^9, 3.656397716483378*^9}, {3.6563978948667727`*^9, 
  3.656397895590354*^9}}],

Cell[BoxData[
 FractionBox[
  RowBox[{
   SuperscriptBox["\[ExponentialE]", 
    RowBox[{
     RowBox[{"-", 
      FractionBox["1", "4"]}], " ", 
     RowBox[{"(", 
      RowBox[{
       SuperscriptBox["k", "2"], "+", 
       SuperscriptBox["kp", "2"]}], ")"}], " ", 
     SuperscriptBox["\[Mu]", "2"]}]], " ", "\[Mu]", " ", 
   RowBox[{"Sinh", "[", 
    RowBox[{
     FractionBox["1", "2"], " ", "k", " ", "kp", " ", 
     SuperscriptBox["\[Mu]", "2"]}], "]"}]}], 
  RowBox[{"k", " ", "kp", " ", 
   SqrtBox["\[Pi]"]}]]], "Output",
 CellChangeTimes->{3.656397675626458*^9, 3.6563977168044643`*^9, 
  3.6563979010836687`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"FullSimplify", "[", 
  RowBox[{
   RowBox[{"V1s0", "/", 
    RowBox[{"(", 
     RowBox[{"2", 
      SuperscriptBox["\[Pi]", "2"]}], ")"}]}], "-", "V1S0read"}], 
  "]"}]], "Input",
 CellChangeTimes->{{3.656397720513633*^9, 3.656397785901466*^9}, {
  3.656397878288082*^9, 3.6563978982021933`*^9}}],

Cell[BoxData["0"], "Output",
 CellChangeTimes->{{3.656397728502268*^9, 3.6563977864017153`*^9}, {
  3.656397879015294*^9, 3.6563979011308727`*^9}}]
}, Open  ]],

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{"(*", " ", "Nakada"}]}]], "Input",
 CellChangeTimes->{{3.656405089721026*^9, 3.656405092393284*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"vn", "[", 
   RowBox[{"k_", ",", "kp_", ",", "x_"}], "]"}], ":=", " ", 
  RowBox[{
   RowBox[{"(", 
    RowBox[{"4", " ", "\[Pi]"}], ")"}], " ", "/", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"\[Mu]", 
      RowBox[{"(", 
       RowBox[{
        SuperscriptBox["k", "2"], "+", 
        SuperscriptBox["kp", "2"], "-", 
        RowBox[{"2", "k", " ", "kp", " ", "x"}]}], ")"}]}], "+", 
     SuperscriptBox["\[Mu]", "3"]}], ")"}]}]}]], "Input",
 CellChangeTimes->{
  3.656405094330851*^9, {3.656405170834064*^9, 3.656405201760043*^9}, {
   3.6568697425435343`*^9, 3.65686976142526*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"V1s0n", "=", 
  RowBox[{"Simplify", "[", 
   RowBox[{
    RowBox[{"Simplify", "[", 
     RowBox[{
      RowBox[{
       RowBox[{"1", "/", "2"}], " ", 
       RowBox[{"Integrate", "[", 
        RowBox[{
         RowBox[{"vn", "[", 
          RowBox[{"k", ",", "kp", ",", "x"}], "]"}], ",", 
         RowBox[{"{", 
          RowBox[{"x", ",", 
           RowBox[{"-", "1"}], ",", "1"}], "}"}], ",", 
         RowBox[{"Assumptions", "\[Rule]", 
          RowBox[{"kp", ">", "0"}]}]}], "]"}]}], ",", 
      RowBox[{
       RowBox[{"\[Mu]", ">", "0"}], " ", "&&", " ", 
       RowBox[{"k", " ", ">", "0"}], "&&", 
       RowBox[{"k", " ", "\[Element]", " ", "Reals"}], "&&", 
       RowBox[{"kp", " ", "\[Element]", " ", "Reals"}]}]}], "]"}], "/", 
    RowBox[{"(", 
     RowBox[{"2", 
      SuperscriptBox["\[Pi]", "2"]}], ")"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.656405211934333*^9, 3.656405215584844*^9}, {
  3.6564097642266817`*^9, 3.656409782826097*^9}, {3.656409820064055*^9, 
  3.656409830201558*^9}, {3.6564098984068613`*^9, 3.6564099443800497`*^9}, {
  3.656410566989859*^9, 3.656410575674918*^9}}],

Cell[BoxData[
 FractionBox[
  RowBox[{"ArcTanh", "[", 
   FractionBox[
    RowBox[{"2", " ", "k", " ", "kp"}], 
    RowBox[{
     SuperscriptBox["k", "2"], "+", 
     SuperscriptBox["kp", "2"], "+", 
     SuperscriptBox["\[Mu]", "2"]}]], "]"}], 
  RowBox[{"k", " ", "kp", " ", "\[Pi]", " ", "\[Mu]"}]]], "Output",
 CellChangeTimes->{
  3.656405277013629*^9, 3.6564098031305523`*^9, 3.656409847807156*^9, {
   3.656409946161915*^9, 3.6564099620532846`*^9}, {3.656410584182724*^9, 
   3.656410601755484*^9}, 3.656869508082451*^9, 3.656869779699769*^9, 
   3.656922067289213*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Limit", "[", 
  RowBox[{
   FractionBox[
    RowBox[{"ArcTanh", "[", 
     FractionBox[
      RowBox[{"2", " ", "k", " ", "kp"}], 
      RowBox[{
       SuperscriptBox["k", "2"], "+", 
       SuperscriptBox["kp", "2"], "+", 
       SuperscriptBox["\[Mu]", "2"]}]], "]"}], 
    RowBox[{"k", " ", "kp", " ", "\[Pi]", " ", "\[Mu]"}]], ",", 
   RowBox[{"k", "\[Rule]", "Infinity"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.656870946184782*^9, 3.656870980545478*^9}}],

Cell[BoxData["0"], "Output",
 CellChangeTimes->{3.6568709812634773`*^9, 3.656922067492385*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Limit", "[", 
  RowBox[{
   FractionBox[
    RowBox[{"ArcTanh", "[", 
     FractionBox[
      RowBox[{"2", " ", "k", " ", "kp"}], 
      RowBox[{
       SuperscriptBox["k", "2"], "+", 
       SuperscriptBox["kp", "2"], "+", 
       SuperscriptBox["\[Mu]", "2"]}]], "]"}], 
    RowBox[{"k", " ", "kp", " ", "\[Pi]", " ", "\[Mu]"}]], ",", 
   RowBox[{"k", "\[Rule]", "0"}]}], "]"}]], "Input",
 CellChangeTimes->{3.656922059723983*^9}],

Cell[BoxData[
 FractionBox["2", 
  RowBox[{
   RowBox[{
    SuperscriptBox["kp", "2"], " ", "\[Pi]", " ", "\[Mu]"}], "+", 
   RowBox[{"\[Pi]", " ", 
    SuperscriptBox["\[Mu]", "3"]}]}]]], "Output",
 CellChangeTimes->{3.656922067652446*^9}]
}, Open  ]]
},
WindowSize->{808, 911},
WindowMargins->{{Automatic, 190}, {Automatic, 42}},
FrontEndVersion->"10.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (June 27, \
2014)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 679, 21, 35, "Input"],
Cell[CellGroupData[{
Cell[1262, 45, 426, 12, 28, "Input"],
Cell[1691, 59, 680, 21, 64, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2408, 85, 767, 22, 35, "Input"],
Cell[3178, 109, 625, 19, 67, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3840, 133, 320, 9, 35, "Input"],
Cell[4163, 144, 147, 2, 28, "Output"]
}, Open  ]],
Cell[4325, 149, 157, 3, 46, "Input"],
Cell[4485, 154, 621, 18, 35, "Input"],
Cell[CellGroupData[{
Cell[5131, 176, 1135, 28, 73, "Input"],
Cell[6269, 206, 576, 14, 63, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6882, 225, 487, 13, 64, "Input"],
Cell[7372, 240, 94, 1, 28, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[7503, 246, 456, 13, 64, "Input"],
Cell[7962, 261, 240, 7, 51, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
