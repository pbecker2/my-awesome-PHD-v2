\begin{thebibliography}{10}
\expandafter\ifx\csname url\endcsname\relax
  \def\url#1{\texttt{#1}}\fi
\expandafter\ifx\csname urlprefix\endcsname\relax\def\urlprefix{URL }\fi
\expandafter\ifx\csname href\endcsname\relax
  \def\href#1#2{#2} \def\path#1{#1}\fi

\bibitem{ben03}
M.~Bender, P.-H. Heenen, P.-G. Reinhard, Rev. Mod. Phys. 75 (2003) 121--180.

\bibitem{sky59}
T.~H.~R. Skyrme, Nucl. Phys. 9 (1959) 615.

\bibitem{dec80}
J.~Decharg{\'e}, D.~Gogny, Phys. Rev. C 21~(4) (1980) 1568.

\bibitem{ber77}
G.~Bertsch, J.~Borysowicz, H.~McManus, W.~Love, Nucl. Phys. A 284~(3) (1977)
  399 -- 419.

\bibitem{gor09}
S.~Goriely, N.~Chamel, J.~M. Pearson, Phys. Rev. Lett. 102 (2009) 152503.

\bibitem{gor09b}
S.~Goriely, S.~Hilaire, M.~Girod, S.~P\'eru, Phys. Rev. Lett. 102 (2009)
  242501.

\bibitem{ber07}
G.~F. Bertsch, M.~Girod, S.~Hilaire, J.-P. Delaroche, H.~Goutte, S.~P\'eru,
  Phys. Rev. Lett. 99 (2007) 032502.

\bibitem{ter08}
J.~Terasaki, J.~Engel, G.~F. Bertsch, Phys. Rev. C 78 (2008) 044311.

\bibitem{dut12}
M.~Dutra, O.~Louren\ifmmode~\mbox{\c{c}}\else \c{c}\fi{}o, J.~S. S\'a~Martins,
  A.~Delfino, J.~R. Stone, P.~D. Stevenson, Phys. Rev. C 85 (2012) 035201.

\bibitem{bal97}
M.~Baldo, I.~Bombaci, G.~Burgio, Astron. Astrophys. 328 (1997) 274--282.

\bibitem{heb11}
K.~Hebeler, S.~K. Bogner, R.~J. Furnstahl, A.~Nogga, A.~Schwenk, Phys. Rev. C
  83 (2011) 031301.

\bibitem{gez13}
A.~Gezerlis, I.~Tews, E.~Epelbaum, S.~Gandolfi, K.~Hebeler, A.~Nogga,
  A.~Schwenk, Phys. Rev. Lett. 111 (2013) 032501.

\bibitem{bog05}
S.~Bogner, A.~Schwenk, R.~Furnstahl, A.~Nogga, Nucl. Phys. A 763 (2005) 59--79.

\bibitem{rot08}
V.~Rotival, Fonctionnelles d'energie non-empiriques pour la structure
  nucleaire, Ph.D. thesis, Universit\'e Paris-Diderot - Paris VII (2008).

\bibitem{les07}
T.~Lesinski, M.~Bender, K.~Bennaceur, T.~Duguet, J.~Meyer, Phys. Rev. C 76
  (2007) 014312.

\bibitem{Ang11}
M.~Anguiano, G.~Co', V.~De~Donno, A.~M. Lallena, Phys. Rev. C 83 (2011) 064306.

\bibitem{fia02}
J.~O. Fiase, K.~R.~S. Devan, A.~Hosaka, Phys. Rev. C 66 (2002) 014004.

\bibitem{bal12}
M.~Baldo, A.~Polls, A.~Rios, H.-J. Schulze, I.~Vida\~na, Phys. Rev. C 86 (2012)
  064001.

\bibitem{ots06}
T.~Otsuka, T.~Matsuo, D.~Abe, Phys. Rev. Lett. 97 (2006) 162501.

\bibitem{car08}
B.~G. Carlsson, J.~Dobaczewski, M.~Kortelainen, Phys. Rev. C 78 (2008) 044326.

\bibitem{rai11}
F.~Raimondi, B.~G. Carlsson, J.~Dobaczewski, Phys. Rev. C 83 (2011) 054311.

\bibitem{kor13}
M.~Kortelainen, J.~McDonnell, W.~Nazarewicz, E.~Olsen, P.-G. Reinhard,
  J.~Sarich, N.~Schunck, S.~M. Wild, D.~Davesne, J.~Erler, A.~Pastore, Phys.
  Rev. C 89 (2014) 054314.

\bibitem{car10}
B.~G. Carlsson, J.~Dobaczewski, Phys. Rev. Lett. 105 (2010) 122501.

\bibitem{nak03}
H.~Nakada, Phys. Rev. C 68 (2003) 014316.

\bibitem{ana83}
N.~Anantaraman, H.~Toki, G.~Bertsch, Nucl. Phys. A 398 (1983) 269.

\bibitem{vau72}
D.~Vautherin, D.~M. Brink, Phys. Rev. C 5 (1972) 626--647.

\bibitem{nak08}
H.~Nakada, Phys. Rev. C 78 (2008) 054301.

\bibitem{Nak13}
H.~Nakada, Phys. Rev. C 87 (2013) 014336.

\bibitem{Ang12}
M.~Anguiano, M.~Grasso, G.~Co', V.~De~Donno, A.~M. Lallena, Phys. Rev. C 86
  (2012) 054302.

\bibitem{Gra13}
M.~Grasso, M.~Anguiano, Phys. Rev. C 88 (2013) 054328.

\bibitem{cha08}
F.~Chappert, M.~Girod, S.~Hilaire 668~(5) (2008) 420 -- 424.

\bibitem{cha15}
F.~Chappert, N.~Pillet, M.~Girod, J.-F. Berger, Phys. Rev. C 91 (2015) 034312.

\bibitem{dav14c}
D.~Davesne, A.~Pastore, J.~Navarro, J. Phys. G: Nucl. Part. Phys. 41 (2014)
  065104.

\bibitem{Dav15}
D.~Davesne, J.~Navarro, P.~Becker, R.~Jodon, J.~Meyer, A.~Pastore, Phys. Rev. C
  91 (2015) 064303.

\bibitem{Dav15AA}
D.~Davesne, A.~Pastore, J.~Navarro, arXiv preprint arXiv:1509.05744.

\bibitem{sel14}
R.~Sellahewa, A.~Rios, Phys. Rev. C 90 (2014) 054327.

\bibitem{cha97}
E.~Chabanat, P.~Bonche, P.~Haensel, J.~Meyer, R.~Schaeffer, Nucl. Phys. A 627
  (1997) 710.

\bibitem{les06}
T.~Lesinski, K.~Bennaceur, T.~Duguet, J.~Meyer, Phys. Rev. C 74 (2006) 044315.

\bibitem{dav14k}
D.~Davesne, J.~Meyer, A.~Pastore, J.~Navarro, Phys. Scripta, in press\href
  {http://arxiv.org/abs/arXiv:1412.1934} {\path{arXiv:arXiv:1412.1934}}.

\bibitem{Dav15PRL}
D.~Davesne, P.~Becker, A.~Pastore, J.~Navarro, arXiv.

\bibitem{Pas14L}
A.~Pastore, D.~Davesne, J.~Navarro, J. Phys. G: Nucl. Part. Phys. 41~(5) (2014)
  055103.

\bibitem{prep}
A.~Pastore, D.~Davesne, J.~Navarro, Phys. Rep. 563~(0) (2014) 1 -- 67.

\bibitem{rai11b}
F.~Raimondi, B.~G. Carlsson, J.~Dobaczewski, J.~Toivanen, Phys. Rev. C 84
  (2011) 064303.

\bibitem{bel59}
J.~Bell, T.~Skyrme, Phil. Mag. 1~(11) (1956) 1055--1068.

\bibitem{abr64}
M.~Abramowitz, I.~A. Stegun, Handbook of Mathematical Functions, Courier
  Corporation, 1964.

\end{thebibliography}
