\contentsline {section}{\numberline {1}Introduction}{2}
\contentsline {section}{\numberline {2}Effective NN interactions}{3}
\contentsline {subsection}{\numberline {2.1}M3Y}{3}
\contentsline {subsection}{\numberline {2.2}Gogny}{4}
\contentsline {subsection}{\numberline {2.3}N3LO}{5}
\contentsline {section}{\numberline {3}Equation of state and its decompositions}{6}
\contentsline {subsection}{\numberline {3.1}Decomposition in spin-isospin channels}{7}
\contentsline {subsection}{\numberline {3.2}Decomposition in partial waves}{9}
\contentsline {section}{\numberline {4}From finite-range to zero-range}{11}
\contentsline {subsection}{\numberline {4.1}Local gauge invariance}{12}
\contentsline {subsection}{\numberline {4.2}M3Y}{14}
\contentsline {subsection}{\numberline {4.3}Gogny interaction}{16}
\contentsline {section}{\numberline {5}Conclusion}{17}
\let \numberline \tmptocnumberline 
\contentsline {section}{\numberline {Appendix \nobreakspace {}A}Symmetric nuclear matter and pure neutron matter EoS}{18}
\contentsline {subsection}{\numberline {Appendix \nobreakspace {}A.1}Nakada's interaction}{18}
\contentsline {subsection}{\numberline {Appendix \nobreakspace {}A.2}Gogny's interaction}{19}
\contentsline {subsection}{\numberline {Appendix \nobreakspace {}A.3}N3LO interaction}{19}
\contentsline {section}{\numberline {Appendix \nobreakspace {}B}Contributions to the $(S,T)$ channels}{19}
\contentsline {subsection}{\numberline {Appendix \nobreakspace {}B.1}M3Y interaction}{19}
\contentsline {subsection}{\numberline {Appendix \nobreakspace {}B.2}Gogny's interaction}{20}
\contentsline {subsection}{\numberline {Appendix \nobreakspace {}B.3}N3LO}{20}
\contentsline {section}{\numberline {Appendix \nobreakspace {}C}Partial wave decomposition}{21}
\contentsline {subsection}{\numberline {Appendix \nobreakspace {}C.1}Nakada's interaction}{21}
\contentsline {subsection}{\numberline {Appendix \nobreakspace {}C.2}Gogny's interaction}{22}
\contentsline {subsection}{\numberline {Appendix \nobreakspace {}C.3}N3LO interaction}{24}
