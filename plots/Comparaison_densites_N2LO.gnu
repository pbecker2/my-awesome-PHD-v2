 
#load line style definitions
load 'parula.pal'


set style line 101 lc rgb '#808080' lt 1
set border 3 back ls 101


# Grid
set style line 102 lc rgb'#808080' lt 0 lw 1
set grid back ls 102
set key font ",20" width 5 at 10, 0.16 
 set output "rho_dens.eps" 
 set xrange [0:12.0]
 #set yrange [0:0.5] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
set termoption dash
#set terminal pngcairo size 1000,900 enhanced font 'Helvetica,20'
set colorsequence podo
 set xlabel "r [fm]" 
 set ylabel "{/Symbol r} (r) [fm^{-3}]" 
 #set title "yes" 
 pl "./Sly4/densités_Sly4_Z= 6._N= 6..dat" u 1:2 w l  ls 2 lw 5  lt rgb "#E9967A" title "^{12} C" , "./Sly4/densités_Sly4_Z= 8._N= 8..dat"  u 1:2 with lines  ls 1 lw 5 lt rgb "#8B0000" title "^{16} O", "./Sly4/densités_Sly4_Z=20._N=20..dat"  u 1:2 with lines ls 1 lw 5 lt rgb "#556B2F" title "^{40} Ca", "./Sly4/densités_Sly4_Z=20._N=28..dat"  u 1:2 with lines ls 1 lw 5 lt rgb "#00008B"  title "^{48} Ca", "./Sly4/densités_Sly4_Z=82._N=126.dat"  u 1:2 with lines ls 1 lw 5 lt rgb "#FF8C00" title "^{208} Pb" 

 set output "tau_dens.eps"
set key font ",20" width 5 at 10, 0.22 
 set xrange [0:12.0]
 #set yrange [0:0.5] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
#set terminal pngcairo size 1000,900 enhanced font 'Helvetica,20'
set termoption dash
set colorsequence podo
 set xlabel "r [fm]" 
 set ylabel "{/Symbol t} (r) [fm^{-5}]" 
 #set title "yes" 
 pl "./Sly4/densités_Sly4_Z= 6._N= 6..dat" u 1:7 w l  ls 2 lw 5  lt rgb "#E9967A" title "^{12} C" , "./Sly4/densités_Sly4_Z= 8._N= 8..dat"  u 1:7 with lines  ls 1 lw 5 lt rgb "#8B0000" title "^{16} O", "./Sly4/densités_Sly4_Z=20._N=20..dat"  u 1:7 with lines ls 1 lw 5 lt rgb "#556B2F" title "^{40} Ca", "./Sly4/densités_Sly4_Z=20._N=28..dat"  u 1:7 with lines ls 1 lw 5 lt rgb "#00008B"  title "^{48} Ca", "./Sly4/densités_Sly4_Z=82._N=126.dat"  u 1:7 with lines ls 1 lw 5 lt rgb "#FF8C00" title "^{208} Pb" 


 set output "J_dens.eps"
set key font ",20" width 5 at 10, 0.05 
 set xrange [0:12.0]
 #set yrange [0:0.5] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
#set terminal pngcairo size 1000,900 enhanced font 'Helvetica,20'
set termoption dash
set colorsequence podo
 set xlabel "r [fm]" 
 set ylabel "J (r) [fm^{-4}]" 
 #set title "yes" 
 pl "./Sly4/densités_Sly4_Z= 6._N= 6..dat" u 1:15 w l  ls 2 lw 5  lt rgb "#E9967A" title "^{12} C" , "./Sly4/densités_Sly4_Z= 8._N= 8..dat"  u 1:15 with lines  ls 1 lw 5 lt rgb "#8B0000" title "^{16} O", "./Sly4/densités_Sly4_Z=20._N=20..dat"  u 1:15 with lines ls 1 lw 5 lt rgb "#556B2F" title "^{40} Ca", "./Sly4/densités_Sly4_Z=20._N=28..dat"  u 1:15 with lines ls 1 lw 5 lt rgb "#00008B"  title "^{48} Ca", "./Sly4/densités_Sly4_Z=82._N=126.dat"  u 1:15 with lines ls 1 lw 5 lt rgb "#FF8C00" title "^{208} Pb"



 set output "tauC_dens.eps"
set key font ",20" width 5 at 10, 0.13 
 set xrange [0:12.0]
 #set yrange [0:0.5] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
#set terminal pngcairo size 1000,900 enhanced font 'Helvetica,20'
set termoption dash
set colorsequence podo
 set xlabel "r [fm]" 
 set ylabel "{/Symbol t}_C (r) [fm^{-5}]" 
 #set title "yes" 
 pl "./Sly4/densités_Sly4_Z= 6._N= 6..dat" u 1:11 w l  ls 2 lw 5  lt rgb "#E9967A" title "^{12} C" , "./Sly4/densités_Sly4_Z= 8._N= 8..dat"  u 1:11 with lines  ls 1 lw 5 lt rgb "#8B0000" title "^{16} O", "./Sly4/densités_Sly4_Z=20._N=20..dat"  u 1:11 with lines ls 1 lw 5 lt rgb "#556B2F" title "^{40} Ca", "./Sly4/densités_Sly4_Z=20._N=28..dat"  u 1:11 with lines ls 1 lw 5 lt rgb "#00008B"  title "^{48} Ca", "./Sly4/densités_Sly4_Z=82._N=126.dat"  u 1:11 with lines ls 1 lw 5 lt rgb "#FF8C00" title "^{208} Pb"

set output "tauR_dens.eps"
set key font ",20" width 5 at 10, 0.065 
 set xrange [0:12.0]
 #set yrange [0:0.5] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
#set terminal pngcairo size 1000,900 enhanced font 'Helvetica,20'
set termoption dash
set colorsequence podo
 set xlabel "r [fm]" 
 set ylabel "{/Symbol t}_R (r) [fm^{-5}]" 
 #set title "yes" 
 pl "./Sly4/densités_Sly4_Z= 6._N= 6..dat" u 1:8 w l  ls 2 lw 5  lt rgb "#E9967A" title "^{12} C" , "./Sly4/densités_Sly4_Z= 8._N= 8..dat"  u 1:8 with lines  ls 1 lw 5 lt rgb "#8B0000" title "^{16} O", "./Sly4/densités_Sly4_Z=20._N=20..dat"  u 1:8 with lines ls 1 lw 5 lt rgb "#556B2F" title "^{40} Ca", "./Sly4/densités_Sly4_Z=20._N=28..dat"  u 1:8 with lines ls 1 lw 5 lt rgb "#00008B"  title "^{48} Ca", "./Sly4/densités_Sly4_Z=82._N=126.dat"  u 1:8 with lines ls 1 lw 5 lt rgb "#FF8C00" title "^{208} Pb"


set output "Q_dens.eps"
set key font ",20" width 5 at 10, 0.35 
 set xrange [0:12.0]
 #set yrange [0:0.5] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
#set terminal pngcairo size 1000,900 enhanced font 'Helvetica,20'
set termoption dash
set colorsequence podo
 set xlabel "r [fm]" 
 set ylabel "Q (r) [fm^{-7}]" 
 #set title "yes" 
 pl "./Sly4/densités_Sly4_Z= 6._N= 6..dat" u 1:14 w l  ls 2 lw 5  lt rgb "#E9967A" title "^{12} C" , "./Sly4/densités_Sly4_Z= 8._N= 8..dat"  u 1:14 with lines  ls 1 lw 5 lt rgb "#8B0000" title "^{16} O", "./Sly4/densités_Sly4_Z=20._N=20..dat"  u 1:14 with lines ls 1 lw 5 lt rgb "#556B2F" title "^{40} Ca", "./Sly4/densités_Sly4_Z=20._N=28..dat"  u 1:14 with lines ls 1 lw 5 lt rgb "#00008B"  title "^{48} Ca", "./Sly4/densités_Sly4_Z=82._N=126.dat"  u 1:14 with lines ls 1 lw 5 lt rgb "#FF8C00" title "^{208} Pb"


set output "V_dens.eps"
set key font ",20" width 5 at 10, 0.02
 set xrange [0:12.0]
 #set yrange [0:0.5] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
#set terminal pngcairo size 1000,900 enhanced font 'Helvetica,20'
set termoption dash
set colorsequence podo
 set xlabel "r [fm]" 
 set ylabel "V (r) [fm^{-6}]" 
 #set title "yes" 
 pl "./Sly4/densités_Sly4_Z= 6._N= 6..dat" u 1:17 w l  ls 2 lw 5  lt rgb "#E9967A" title "^{12} C" , "./Sly4/densités_Sly4_Z= 8._N= 8..dat"  u 1:17 with lines  ls 1 lw 5 lt rgb "#8B0000" title "^{16} O", "./Sly4/densités_Sly4_Z=20._N=20..dat"  u 1:17 with lines ls 1 lw 5 lt rgb "#556B2F" title "^{40} Ca", "./Sly4/densités_Sly4_Z=20._N=28..dat"  u 1:17 with lines ls 1 lw 5 lt rgb "#00008B"  title "^{48} Ca", "./Sly4/densités_Sly4_Z=82._N=126.dat"  u 1:17 with lines ls 1 lw 5 lt rgb "#FF8C00" title "^{208} Pb"


set output "VR_dens.eps"
set key font ",20" width 5 at 10, -4
 set xrange [0:12.0]
 #set yrange [0:0.5] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
#set terminal pngcairo size 1000,900 enhanced font 'Helvetica,20'
set termoption dash
set colorsequence podo
 set xlabel "r [fm]" 
 set ylabel "V_R (r) [fm^{-6}]" 
 #set title "yes" 
 pl "./Sly4/densités_Sly4_Z= 6._N= 6..dat" u 1:18 w l  ls 2 lw 5  lt rgb "#E9967A" title "^{12} C" , "./Sly4/densités_Sly4_Z= 8._N= 8..dat"  u 1:18 with lines  ls 1 lw 5 lt rgb "#8B0000" title "^{16} O", "./Sly4/densités_Sly4_Z=20._N=20..dat"  u 1:18 with lines ls 1 lw 5 lt rgb "#556B2F" title "^{40} Ca", "./Sly4/densités_Sly4_Z=20._N=28..dat"  u 1:18 with lines ls 1 lw 5 lt rgb "#00008B"  title "^{48} Ca", "./Sly4/densités_Sly4_Z=82._N=126.dat"  u 1:18 with lines ls 1 lw 5 lt rgb "#FF8C00" title "^{208} Pb"

set output "VR2_dens.eps"
set key font ",20" width 5 at 10, 0.16 
 set xrange [0:12.0]
 #set yrange [0:0.5] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
#set terminal pngcairo size 1000,900 enhanced font 'Helvetica,20'
set termoption dash
set colorsequence podo
 set xlabel "r [fm]" 
 set ylabel "V_R (r) [fm^{-6}]" 
 #set title "yes" 
 pl "./Sly4/densités_Sly4_Z= 8._N= 8..dat"  u 1:18 with lines  ls 1 lw 5 lt rgb "#8B0000" title "^{16} O", "./Sly4/densités_Sly4_Z=20._N=20..dat"  u 1:18 with lines ls 1 lw 5 lt rgb "#556B2F" title "^{40} Ca", "./Sly4/densités_Sly4_Z=20._N=28..dat"  u 1:18 with lines ls 1 lw 5 lt rgb "#00008B"  title "^{48} Ca", "./Sly4/densités_Sly4_Z=82._N=126.dat"  u 1:18 with lines ls 1 lw 5 lt rgb "#FF8C00" title "^{208} Pb"


set output "VC_dens.eps"
set key font ",20" width 5 at 10, 4 
 set xrange [0:12.0]
 #set yrange [0:0.5] 
  set terminal postscript eps enhanced color font 'Helvetica,20'
#set terminal pngcairo size 1000,900 enhanced font 'Helvetica,20'
set termoption dash
set colorsequence podo
 set xlabel "r [fm]" 
 set ylabel "V_C(r) [fm^{-6}]" 
 #set title "yes" 
 pl "./Sly4/densités_Sly4_Z= 6._N= 6..dat" u 1:19 w l  ls 2 lw 5  lt rgb "#E9967A" title "^{12} C" , "./Sly4/densités_Sly4_Z= 8._N= 8..dat"  u 1:19 with lines  ls 1 lw 5 lt rgb "#8B0000" title "^{16} O", "./Sly4/densités_Sly4_Z=20._N=20..dat"  u 1:19 with lines ls 1 lw 5 lt rgb "#556B2F" title "^{40} Ca", "./Sly4/densités_Sly4_Z=20._N=28..dat"  u 1:19 with lines ls 1 lw 5 lt rgb "#00008B"  title "^{48} Ca", "./Sly4/densités_Sly4_Z=82._N=126.dat"  u 1:19 with lines ls 1 lw 5 lt rgb "#FF8C00" title "^{208} Pb"

set output "VC2_dens.eps"
set key font ",20" width 5 at 10, -0.16 
 set xrange [0:12.0]
 #set yrange [0:0.5] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
#set terminal pngcairo size 1000,900 enhanced font 'Helvetica,20'
set termoption dash
set colorsequence podo
 set xlabel "r [fm]" 
 set ylabel "V_C (r) [fm^{-6}]" 
 #set title "yes" 
 pl "./Sly4/densités_Sly4_Z= 8._N= 8..dat"  u 1:19 with lines  ls 1 lw 5 lt rgb "#8B0000" title "^{16} O", "./Sly4/densités_Sly4_Z=20._N=20..dat"  u 1:19 with lines ls 1 lw 5 lt rgb "#556B2F" title "^{40} Ca", "./Sly4/densités_Sly4_Z=20._N=28..dat"  u 1:19 with lines ls 1 lw 5 lt rgb "#00008B"  title "^{48} Ca", "./Sly4/densités_Sly4_Z=82._N=126.dat"  u 1:19 with lines ls 1 lw 5 lt rgb "#FF8C00" title "^{208} Pb"

set output "Stau_dens.eps"
set key font ",20" width 5 at 10, 0.25 
 set xrange [0:12.0]
 #set yrange [0:0.5] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
#set terminal pngcairo size 1000,900 enhanced font 'Helvetica,20'
set termoption dash
set colorsequence podo
 set xlabel "r [fm]" 
 set ylabel " S {/Symbol t} (r) [fm^{-7}]" 
 #set title "yes" 
 pl "./Sly4/densités_Sly4_Z= 6._N= 6..dat" u 1:20 w l  ls 2 lw 5  lt rgb "#E9967A" title "^{12} C" , "./Sly4/densités_Sly4_Z= 8._N= 8..dat"  u 1:20 with lines  ls 1 lw 5 lt rgb "#8B0000" title "^{16} O", "./Sly4/densités_Sly4_Z=20._N=20..dat"  u 1:20 with lines ls 1 lw 5 lt rgb "#556B2F" title "^{40} Ca", "./Sly4/densités_Sly4_Z=20._N=28..dat"  u 1:20 with lines ls 1 lw 5 lt rgb "#00008B"  title "^{48} Ca", "./Sly4/densités_Sly4_Z=82._N=126.dat"  u 1:20 with lines ls 1 lw 5 lt rgb "#FF8C00" title "^{208} Pb"

set output "Kmunu_dens.eps"
set key font ",20" width 5 at 10, -0.02 
 set xrange [0:12.0]
 #set yrange [0:0.5] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
#set terminal pngcairo size 1000,900 enhanced font 'Helvetica,20'
set termoption dash
set colorsequence podo
 set xlabel "r [fm]" 
 set ylabel " K (r) [fm^{-5}]" 
 #set title "yes" 
 pl "./Sly4/densités_Sly4_Z= 6._N= 6..dat" u 1:23 w l  ls 2 lw 5  lt rgb "#E9967A" title "^{12} C" , "./Sly4/densités_Sly4_Z= 8._N= 8..dat"  u 1:23 with lines  ls 1 lw 5 lt rgb "#8B0000" title "^{16} O", "./Sly4/densités_Sly4_Z=20._N=20..dat"  u 1:23 with lines ls 1 lw 5 lt rgb "#556B2F" title "^{40} Ca", "./Sly4/densités_Sly4_Z=20._N=28..dat"  u 1:23 with lines ls 1 lw 5 lt rgb "#00008B"  title "^{48} Ca", "./Sly4/densités_Sly4_Z=82._N=126.dat"  u 1:23 with lines ls 1 lw 5 lt rgb "#FF8C00" title "^{208} Pb"

set output "Kmunu_dens1.eps"
set key font ",20" width 5 at 6, 0.015 
 set xrange [0:12.0]
 #set yrange [0:0.5] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
#set terminal pngcairo size 1000,900 enhanced font 'Helvetica,20'
set termoption dash
set colorsequence podo
 set xlabel "r [fm]" 
 set ylabel " K1(r) [fm^{-5}]" 
 #set title "yes" 
 pl "./Sly4/densités_Sly4_Z= 6._N= 6..dat" u 1:24 w l  ls 2 lw 5  lt rgb "#E9967A" title "^{12} C" , "./Sly4/densités_Sly4_Z= 8._N= 8..dat"  u 1:24 with lines  ls 1 lw 5 lt rgb "#8B0000" title "^{16} O", "./Sly4/densités_Sly4_Z=20._N=20..dat"  u 1:24 with lines ls 1 lw 5 lt rgb "#556B2F" title "^{40} Ca", "./Sly4/densités_Sly4_Z=20._N=28..dat"  u 1:24 with lines ls 1 lw 5 lt rgb "#00008B"  title "^{48} Ca", "./Sly4/densités_Sly4_Z=82._N=126.dat"  u 1:24 with lines ls 1 lw 5 lt rgb "#FF8C00" title "^{208} Pb"

set output "Kmunu_dens2.eps"
set key font ",20" width 5 at 7, 0.005 
 set xrange [0:12.0]
 #set yrange [0:0.5] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
#set terminal pngcairo size 1000,900 enhanced font 'Helvetica,20'
set termoption dash
set colorsequence podo
 set xlabel "r [fm]" 
 set ylabel " K2(r) [fm^{-5}]" 
 #set title "yes" 
 pl "./Sly4/densités_Sly4_Z= 6._N= 6..dat" u 1:25 w l  ls 2 lw 5  lt rgb "#E9967A" title "^{12} C" , "./Sly4/densités_Sly4_Z= 8._N= 8..dat"  u 1:25 with lines  ls 1 lw 5 lt rgb "#8B0000" title "^{16} O", "./Sly4/densités_Sly4_Z=20._N=20..dat"  u 1:25 with lines ls 1 lw 5 lt rgb "#556B2F" title "^{40} Ca", "./Sly4/densités_Sly4_Z=20._N=28..dat"  u 1:25 with lines ls 1 lw 5 lt rgb "#00008B"  title "^{48} Ca", "./Sly4/densités_Sly4_Z=82._N=126.dat"  u 1:25 with lines ls 1 lw 5 lt rgb "#FF8C00" title "^{208} Pb" 

set output "SStau_dens.eps"
set key font ",20" width 5 at 10, 2 
 set xrange [0:12.0]
 #set yrange [0:0.5] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
#set terminal pngcairo size 1000,900 enhanced font 'Helvetica,20'
set termoption dash
set colorsequence podo
 set xlabel "r [fm]" 
 set ylabel " SS{/Symbol t} (r) [fm^{-9}]" 
 #set title "yes" 
 pl "./Sly4/densités_Sly4_Z= 6._N= 6..dat" u 1:22 w l  ls 2 lw 5  lt rgb "#E9967A" title "^{12} C" , "./Sly4/densités_Sly4_Z= 8._N= 8..dat"  u 1:22 with lines  ls 1 lw 5 lt rgb "#8B0000" title "^{16} O", "./Sly4/densités_Sly4_Z=20._N=20..dat"  u 1:22 with lines ls 1 lw 5 lt rgb "#556B2F" title "^{40} Ca", "./Sly4/densités_Sly4_Z=20._N=28..dat"  u 1:22 with lines ls 1 lw 5 lt rgb "#00008B"  title "^{48} Ca", "./Sly4/densités_Sly4_Z=82._N=126.dat"  u 1:22 with lines ls 1 lw 5 lt rgb "#FF8C00" title "^{208} Pb"

set output "N2LOeffect.eps"
set key font ",20" width 5 at 10, 0.16 
 set xrange [0:12.0]
 #set yrange [0:0.5] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
#set terminal pngcairo size 1000,900 enhanced font 'Helvetica,20'
set termoption dash
set colorsequence podo
 set xlabel "r [fm]" 
 set ylabel " {/Symbol r} (r) [fm^{-3}]" 
 #set title "yes" 
 pl "./Sly4/densités_Sly4_Z=20._N=28..dat"  u 1:2 with lines ls 1 lw 5 lt rgb "#00008B"  title "Sly4", "./Sly4/densités_Sly4N2LO5_Z=20._N=28..dat"  u 1:2 with lines ls 1 lw 5 lt rgb "#FF8C00" title "Sly4N2LO5"

set output "rho_PB.eps"
set key font ",20" width 5 at 10, 0.16 
 set xrange [0:12.0]
 #set yrange [0:0.5] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
#set terminal pngcairo size 1000,900 enhanced font 'Helvetica,20'
set termoption dash
set colorsequence podo
 set xlabel "r [fm]" 
 set ylabel " {/Symbol r} (r) [fm^{-3}]" 
 #set title "yes" 
 pl "./Pb/densites_SN2LO1_Z=82._N=126.dat"  u 1:2 with lines ls 1 lw 5 lt rgb "#00008B"  title "Sly4", "./Pb/densites_Sly5_Z=82._N=126.dat"  u 1:2 with lines ls 1 lw 5 lt rgb "#FF8C00" title "SN2LO1"

set output "rho_PBq.eps"
set key font ",20" width 5 at 10, 0.1 
 set xrange [0:12.0]
 #set yrange [0:0.5] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
#set terminal pngcairo size 1000,900 enhanced font 'Helvetica,20'
set termoption dash
set colorsequence podo
 set xlabel "r [fm]" 
 set ylabel " {/Symbol r} (r) [fm^{-3}]" 
 set label "{/Symbol r}_n}" font "Helvetica,19" at 4, 0.10
 set label "{/Symbol r}_p}" font "Helvetica,19" at 4, 0.05
 #set title "yes" 
 pl "./Pb/densitesrho_SN2LO1_Z=82._N=126.dat"  u 1:2 with lines ls 1 lw 5 lt rgb "#FF8C00"  title "Sly4 n", "" u 1:3 with lines  dt 2 lw 5 lt rgb "#FF8C00"  title "Sly4 p",   "./Pb/densitesrho_Sly5_Z=82._N=126.dat"  u 1:2  with lines ls 1 lw 5 lt rgb "#00008B" title "SN2LO1 n", ""  u 1:3 with lines  dt 2  lw 5 lt rgb "#00008B" title "SN2LO1 p"

unset label

set output "rho_pairing.eps"
set key font ",20" width 5 at 9, 0.1 
 set xrange [0:12.0]
 #set yrange [0:0.5] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
#set terminal pngcairo size 1000,900 enhanced font 'Helvetica,20'
set termoption dash
set colorsequence podo
 set xlabel "r [fm]" 
 set ylabel " {/Symbol r} (r) [fm^{-3}]" 
 set label "{/Symbol r}}" font "Helvetica,19" at 4, 0.14
 set label "{{/Symbol r}_{pair}}" font "Helvetica,19" at 4, 0.03
 #set title "yes" 
 pl "./pairing/densities.gfx"  u 1:2 with lines ls 1 lw 5 lt rgb "#FF8C00"  title "{/Symbol r}", "" u 1:(-$22) with lines  dt 2 lw 5 lt rgb "#FF8C00"  title "{/Symbol r}_{pair}"

unset label

set output "rho_pairing2.eps"
set key font ",20" width 5 at 9, 0.1 
 set xrange [0:12.0]
 #set yrange [0:0.5] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
#set terminal pngcairo size 1000,900 enhanced font 'Helvetica,20'
set termoption dash
set colorsequence podo
 set xlabel "r [fm]" 
 set ylabel " {/Symbol r} (r) [fm^{-3}]" 
 set label "{/Symbol r}}" font "Helvetica,19" at 4, 0.14
 set label "{{/Symbol r}_{pair}}" font "Helvetica,19" at 4, 0.03
 #set title "yes" 
 pl "./pairing/densities.gfx"  u 1:2 with lines ls 1 lw 5 lt rgb "#FF8C00"  title "{/Symbol r}", "" u 1:22 with lines  dt 2 lw 5 lt rgb "#FF8C00"  title "{/Symbol r}_{pair}"

exit
