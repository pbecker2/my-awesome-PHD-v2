set term postscript enhanced color font 'Helvetica,14'

#Format d'affichage


#Format du fichier de sortie

set output "isot_rad_param_2.eps"




#######Début des plots
### Start multiplot (2x2 layout)

# color definitions
#set style line  1 lc rgb '#0060ad' pt 5 ps 0.2 lt 1 lw 2    # blue


# load line style definitions
#load 'parula.pal'

# Axes
set style line 101 lc rgb '#808080' lt 1
#set border 3 back ls 101
#set tics nomirror out scale 0.75
# Grid
set style line 102 lc rgb'#808080' lt 0 lw 1
set grid back ls 102







			#####################################
			#Multiplot1: Isot#####
			#####################################



set multiplot layout 2,2 rowsfirst #title 'Ajustement des canaux nucléaires'

unset key
set macros
set termopt enhanced
#set key samplen 2 spacing .9 font ",9"

#Print the informations on the top

#set label 2 @VAL  at screen 0.69, 0.97 font "Helvetica,7" 





# Label position
POS = "at graph 0.45,0.92 font ',12'"
POS2 = "at graph 0.55,0.92 font ',8'"
# Axes Numeration
NOXTICS = "set xlabel 'Nombre de neutrons'"
XTICS = "set xlabel 'Nombre de neutrons'"
NOYTICS = "set format y ''; unset ylabel"
YTICS = "set format y '%.2f'; set ylabel 'y'"
# Margins for each row resp. column
TMARGIN = "set tmargin at screen 0.95; set bmargin at screen 0.60"
BMARGIN = "set tmargin at screen 0.50; set bmargin at screen 0.15"
LMARGIN = "set lmargin at screen 0.12; set rmargin at screen 0.48"
RMARGIN = "set lmargin at screen 0.52; set rmargin at screen 0.88"

# Axes ranges
#set xrange [0:16]
#set yrange [-0.05:0.05]



# --- GRAPH a
@TMARGIN; @LMARGIN
@NOXTICS; @YTICS
# set xlabel '{/symbol r} (fm)' font ',12'
set ylabel 'r_p' font ',14'
LABEL="Ca"
set obj 10 rect at graph 0.9,0.10 size char strlen(LABEL)+2, char 2 fc rgb "white"
set label 10 LABEL at graph 0.9,0.10 front center font ",18"

#set xrange [0:1]
#set yrange [0:40]

plot "tot_isot_SN2LO1.dat" i 0 u 1:6 with points pt 7 ps 1 lt rgb "#E9967A" title 'SN2LO1',\
     "tot_isot_SN2LO1.dat" i 0 u 1:6 notitle w l  ls 12 lw 2 lt rgb "#E9967A"  ,\
     "tot_isot_Sly5.dat" i 0 u 1:6  with points pt 9 ps 1 lt rgb "#8B0000" title '{/Symbol c}-EFT' ,\
     "tot_isot_Sly5.dat" i 0 u 1:6 w l notitle ls 11 lw 2 lt rgb "#8B0000" ,\
     "Carad" u 2:8  with points pt 13 ps 1 lt rgb "#556B2F" title '{/Symbol c}-EFT' ,\
     "Carad" u 2:8 w l notitle ls 15 lw 3 lt rgb "#556B2F"
unset ylabel      
#unset xlabel  


# --- GRAPH b
@TMARGIN; @RMARGIN
@NOXTICS; @NOYTICS 
#set xrange [0:1]
#set yrange [0:40]
set key box width +2 @POS
unset ylabel
set y2tics
set y2label 'r_p' font ',14' 
LABEL="Sn"
set obj 10 rect at graph 0.9,0.10 size char strlen(LABEL)+2, char 2 fc rgb "white"
set label 10 LABEL at graph 0.9,0.10 front center font ",18"
plot "tot_isot_SN2LO1.dat" i 1 u 1:6 with points pt 7 ps 1 lt rgb "#E9967A" title 'SN2LO1',\
     "tot_isot_SN2LO1.dat" i 1 u 1:6 notitle w l  ls 12 lw 2 lt rgb "#E9967A"  ,\
     "tot_isot_Sly5.dat" i 1 u 1:6  with points pt 9 ps 1 lt rgb "#8B0000" title 'SLy5' ,\
     "tot_isot_Sly5.dat" i 1 u 1:6 w l notitle ls 11 lw 2 lt rgb "#8B0000" ,\
     "Snrad" u 2:8  with points pt 13 ps 1 lt rgb "#556B2F" title 'Exp' ,\
     "Snrad" u 2:8 w l notitle ls 15 lw 3 lt rgb "#556B2F"
#unset ylabel      
#unset xlabel 
unset ylabel  
unset y2label
unset y2tics
unset key
     
     
# --- GRAPH c
@BMARGIN; @LMARGIN
@XTICS; @YTICS
#set xrange [0:1]
#set yrange [-44:0]
set ylabel ' r_p'  font ',14'
LABEL="Ni"
set obj 10 rect at graph 0.9,0.10 size char strlen(LABEL)+2, char 2 fc rgb "white"
set label 10 LABEL at graph 0.9,0.10 front center font ",18"

plot "tot_isot_SN2LO1.dat" i 2 u 1:6 with points pt 7 ps 1 lt rgb "#E9967A" title 'SN2LO1',\
     "tot_isot_SN2LO1.dat" i 2 u 1:6 notitle w l  ls 12 lw 2 lt rgb "#E9967A"  ,\
     "tot_isot_Sly5.dat" i 2 u 1:6  with points pt 9 ps 1 lt rgb "#8B0000" title 'SLy4' ,\
     "tot_isot_Sly5.dat" i 2 u 1:6 w l notitle ls 11 lw 2 lt rgb "#8B0000" ,\
     "Nirad" u 2:8  with points pt 13 ps 1 lt rgb "#556B2F" title '{/Symbol c}-EFT' ,\
     "Nirad" u 2:8 w l notitle ls 15 lw 3 lt rgb "#556B2F"
unset ylabel      
#unset xlabel     
     
     
# --- GRAPH d
@BMARGIN; @RMARGIN
@XTICS; @NOYTICS
#set xrange [0:1]
#set yrange [-4:18]
unset ylabel
set y2tics
#set y2range [-4:18]
set y2label 'r_p' font ',14' 
LABEL="Pb"
set obj 10 rect at graph 0.9,0.10 size char strlen(LABEL)+2, char 2 fc rgb "white"
set label 10 LABEL at graph 0.9,0.10 front center font ",18"

plot "tot_isot_SN2LO1.dat" i 3 u 1:6 with points pt 7 ps 1 lt rgb "#E9967A" title 'SN2LO1',\
     "tot_isot_SN2LO1.dat" i 3 u 1:6 notitle w l  ls 12 lw 2 lt rgb "#E9967A"  ,\
     "tot_isot_Sly5.dat" i 3 u 1:6  with points pt 9 ps 1 lt rgb "#8B0000" title 'SLy4' ,\
     "tot_isot_Sly5.dat" i 3 u 1:6 w l notitle ls 11 lw 2 lt rgb "#8B0000" ,\
     "Pbrad" u 2:8  with points pt 13 ps 1 lt rgb "#556B2F" title '{/Symbol c}-EFT' ,\
     "Pbrad" u 2:8 w l notitle ls 15 lw 3 lt rgb "#556B2F"
#unset ylabel      
#unset xlabel  

unset multiplot
 reset

exit
