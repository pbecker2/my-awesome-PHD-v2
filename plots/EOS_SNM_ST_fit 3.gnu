set term postscript eps enhanced font 'Helvetica,18'

#Format d'affichage


#Format du fichier de sortie

set output "EOS_SNM_ST_fit3.eps"




#######Début des plots
### Start multiplot (2x2 layout)

# color definitions
#set style line  1 lc rgb '#0060ad' pt 5 ps 0.2 lt 1 lw 2    # blue


# load line style definitions
#load 'parula.pal'

# Axes
set style line 101 lc rgb '#808080' lt 1
#set border 3 back ls 101
#set tics nomirror out scale 0.75
# Grid
set style line 102 lc rgb'#808080' lt 0 lw 1
set grid back ls 102







			#####################################
			#Multiplot1: Rho and derivatives#####
			#####################################



set multiplot layout 2,2 rowsfirst #title 'Ajustement des canaux nucléaires'

unset key
set macros
set termopt enhanced
#set key samplen 2 spacing .9 font ",9"

#Print the informations on the top

#set label 2 @VAL  at screen 0.69, 0.97 font "Helvetica,7" 





# Label position
POS = "at graph 0.40,0.9 font ',14'"
POS2 = "at graph 0.55,0.92 font ',8'"
# Axes Numeration
NOXTICS = "set xtics ('' 0,'' 0.2,'' 0.4,'' 0.6,'' 0.8); \
          unset xlabel"
XTICS = "set xtics ('0' 0,'0.2' 0.2,'0.4' 0.4,'0.6' 0.6,'0.8' 0.8);\
          set xlabel '{/Symbol r} (fm)'"
NOYTICS = "set format y ''; unset ylabel"
YTICS = "set format y '%.0f'; set ylabel 'y'"
# Margins for each row resp. column
TMARGIN = "set tmargin at screen 0.90; set bmargin at screen 0.55"
BMARGIN = "set tmargin at screen 0.55; set bmargin at screen 0.20"
LMARGIN = "set lmargin at screen 0.10; set rmargin at screen 0.50"
RMARGIN = "set lmargin at screen 0.50; set rmargin at screen 0.90"

# Axes ranges
#set xrange [0:16]
#set yrange [-0.05:0.05]



# --- GRAPH a
@TMARGIN; @LMARGIN
@NOXTICS; @YTICS
# set xlabel '{/symbol r} (fm)' font ',12'
set ylabel ' (S=0,T=0)' font ',15'

set xrange [0:1]
set yrange [0:40]

plot "EOS_SNM_ST_fit.dat" i 0 u 1:2 with points pt 7 ps 1 lt rgb "#E9967A" title 'HFB',\
     "EOS_SNM_ST_fit.dat" i 1 u 1:2 notitle w l  ls 12 lw 2 lt rgb "#E9967A"  ,\
     "N3LO_fit_conv1.dat" i 1 u 1:2  with points pt 9 ps 1 lt rgb "#8B0000" title '{/Symbol c}-EFT' ,\
     "N3LO_fit_conv1.dat" i 2 u 1:2 w l notitle ls 11 lw 2 lt rgb "#8B0000" ,\
     "N3LO_fit_conv1.dat" i 3 u 1:2  with points pt 13 ps 1 lt rgb "#556B2F" title 'MBTP'
unset ylabel      
#unset xlabel  


# --- GRAPH b
@TMARGIN; @RMARGIN
@NOXTICS; @NOYTICS 
set xrange [0:1]
set yrange [0:40]
set key box @POS
unset ylabel
set y2tics
set y2label '(S=1,T=1)' font ',15' 
plot "EOS_SNM_ST_fit.dat" i 6 u 1:2 with points pt 7 ps 1 lt rgb "#E9967A" title 'HFB',\
     "EOS_SNM_ST_fit.dat" i 7 u 1:2 notitle w l  ls 12 lw 2 lt rgb "#E9967A"  ,\
     "EOS_SNM_ST_fit.dat" i 8 u 1:2  with points pt 9 ps 1 lt rgb "#8B0000" title '{/Symbol c}-EFT' ,\
     "EOS_SNM_ST_fit.dat" i 9 u 1:2 w l notitle ls 11 lw 2 lt rgb "#8B0000" ,\
     "EOS_SNM_ST_fit.dat" i 10 u 1:2  with points pt 13 ps 1 lt rgb "#556B2F" title 'MBTP' ,\
     "EOS_SNM_ST_fit.dat" i 11 u 1:2 w l notitle  ls 15 lw 2 lt rgb "#556B2F"
#unset ylabel      
#unset xlabel 
unset ylabel  
unset y2label
unset y2tics
unset key
     
     
# --- GRAPH c
@BMARGIN; @LMARGIN
@XTICS; @YTICS
set xrange [0:1]
set yrange [-44:0]
set ylabel ' (S=1,T=0)'  font ',15'

plot "EOS_SNM_ST_fit.dat" i 12 u 1:2 with points pt 7 ps 1 lt rgb "#E9967A" title 'HFB',\
     "EOS_SNM_ST_fit.dat" i 13 u 1:2 notitle w l  ls 12 lw 2 lt rgb "#E9967A"  ,\
     "EOS_SNM_ST_fit.dat" i 14 u 1:2  with points pt 9 ps 1 lt rgb "#8B0000" title '{/Symbol c}-EFT' ,\
     "EOS_SNM_ST_fit.dat" i 17 u 1:2 w l notitle ls 11 lw 2 lt rgb "#8B0000" ,\
     "EOS_SNM_ST_fit.dat" i 16 u 1:2  with points pt 13 ps 1 lt rgb "#556B2F" title 'MBTP' ,\
     "EOS_SNM_ST_fit.dat" i 15 u 1:2 w l notitle  ls 15 lw 2 lt rgb "#556B2F"
unset ylabel      
#unset xlabel     
     
     
# --- GRAPH d
@BMARGIN; @RMARGIN
@XTICS; @NOYTICS
set xrange [0:1]
#set yrange [-44:0]
unset ylabel
set y2tics
set y2range [-44:0]
set y2label '(S=0,T=1)' font ',15' 

plot "EOS_SNM_ST_fit.dat" i 18 u 1:2 with points pt 7 ps 1 lt rgb "#E9967A" title 'HFB',\
     "EOS_SNM_ST_fit.dat" i 19 u 1:2 notitle w l  ls 12 lw 2 lt rgb "#E9967A"  ,\
     "EOS_SNM_ST_fit.dat" i 20 u 1:2  with points pt 9 ps 1 lt rgb "#8B0000" title '{/Symbol c}-EFT' ,\
     "EOS_SNM_ST_fit.dat" i 21 u 1:2 w l notitle ls 11 lw 2 lt rgb "#8B0000" ,\
     "EOS_SNM_ST_fit.dat" i 22 u 1:2  with points pt 13 ps 1 lt rgb "#556B2F" title 'MBTP' ,\
     "EOS_SNM_ST_fit.dat" i 23 u 1:2 w l notitle  ls 15 lw 2 lt rgb "#556B2F"
#unset ylabel      
#unset xlabel  

unset multiplot
 reset

exit
