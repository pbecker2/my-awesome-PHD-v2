 
# load line style definitions
#load 'parula.pal'

set style line 101 lc rgb '#808080' lt 1
set border 3 back ls 101

# Grid
set style line 102 lc rgb'#808080' lt 0 lw 1
set grid back ls 102
set key font ",18" width -7.5 at 1.35, 70 
 set output "Comparaison_PW.eps" 
 #set xrange [0:9.0]
 #set yrange [0:0.5] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
set colorsequence podo
 set xlabel "k_F [fm^{-1}]" 
 set ylabel "{E / A} [Mev]" 
 #set title "yes" 
 pl "canaux.out" u 2:8 w l  ls 1 lw 7 lt rgb "#E9967A" title "Energie totale " , "Baldo_Ondes_partielles.dat"  u 1:($2+$3) with points pt 7 ps 1 lt rgb "#8B0000"  title "Contribution de l'onde S","Baldo_Ondes_partielles.dat"  u 1:($2+$3) with lines lw 3 lt rgb "#8B0000"  notitle,"Baldo_Ondes_partielles.dat"  u 1:($4+$5+$6+$9) with points pt 7 ps 1 lt rgb "#556B2F" title "Contribution de l'onde P",""  u 1:($4+$5+$6+$9) with lines lw 3 lt rgb "#556B2F" notitle, "Baldo_Ondes_partielles.dat"  u 1:($7+$8+$14) with points pt 7 ps 1 lt rgb "#00008B"  title "Contribution de l'onde D","Baldo_Ondes_partielles.dat"  u 1:($7+$8+$14) with lines lw 3 lt rgb "#00008B"  notitle, "Baldo_Ondes_partielles.dat"  u 1:($10+$11+$15) with points pt 7 ps 1 lt rgb "#FF8C00" title "Contribution de l'onde F", "Baldo_Ondes_partielles.dat"  u 1:($10+$11+$15) with lines lw 3 lt rgb "#FF8C00" notitle 

 set output "Comparaison_PW_bis.eps"
set key font ",18" width -7.5 at 1.7, 150  
 #set xrange [0:9.0]
 #set yrange [0:0.5] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
set colorsequence podo
 set xlabel "k_F [fm^{-1}]" 
 set ylabel "{E / A} [Mev]" 
 #set title "yes"
 #Ecin = $40, V = $41, Ecin+V = $42
 set multiplot
#Big plot 
# set white rectangle as a place holder for the small plot
set object 1 rect from 0.5,10 to 1.5,105
set object 1 rect fc rgb 'white' fillstyle solid 0.0 noborder
 pl "canaux.out" u 2:8  with points ls 4 lw 9 lt rgb "#E9967A" title "Energie totale" , "Baldo_Ondes_partielles.dat"  u 1:($2+$3+$40) with lines lw 3 lt rgb "#8B0000"  title "Contribution de l'onde S","Baldo_Ondes_partielles.dat"  u 1:($2+$3+$4+$5+$6+$9+$40) with lines lw 3 lt rgb "#556B2F" title "Contribution des ondes S et P", "Baldo_Ondes_partielles.dat"  u 1:($2+$3+$4+$5+$6+$9+$7+$8+$14+$40) with lines lw 3 lt rgb "#00008B"  title "Contribution des ondes S,P et D", "Baldo_Ondes_partielles.dat"  u 1:($2+$3+$4+$5+$6+$9+$7+$8+$14+$10+$11+$15+ $40) with lines lw 3 lt rgb "#FF8C00" title "Contribution jusqu'aux ondes F"

#Small plot
set origin 0.12,0.25
set size 0.5,0.5
set xrange [1.2:1.6]
set yrange [-20:0.1]
unset xlabel
unset ylabel
unset label
unset object 1
set tics scale 0.5 front
set xtics 0.1 offset 1,0.5
set ytics 10 offset 0.5,0
plot "canaux.out" u 2:8   ls 4 lw 9 lt rgb "#E9967A"  title "Energie totale","Baldo_Ondes_partielles.dat"  u 1:($2+$3+$40) with lines lw 4 lt rgb "#8B0000"  notitle,"Baldo_Ondes_partielles.dat"  u 1:($2+$3+$4+$5+$6+$9+$40) with lines lw 4 lt rgb "#556B2F" notitle, "Baldo_Ondes_partielles.dat"  u 1:($2+$3+$4+$5+$6+$9+$7+$8+$14+$40) with lines lw 4 lt rgb "#00008B"  notitle, "Baldo_Ondes_partielles.dat"  u 1:($2+$3+$4+$5+$6+$9+$7+$8+$14+$10+$11+$15+ $40) with lines lw 4 lt rgb "#FF8C00" notitle  

unset multiplot
