
# load line style definitions
#load 'parula.pal'

set style line 101 lc rgb '#808080' lt 1
set border 3 back ls 101

# Grid
set style line 102 lc rgb'#808080' lt 0 lw 1
set grid back ls 102
set key font ",20" width -7.5 spacing 2 at 2.5,0.2 
 set output "Portees.eps" 
 set xrange [0:3]
 #set yrange [0.55:1.1] 
 set terminal postscript eps enhanced color font 'Helvetica,22'
set colorsequence podo
 set xlabel "x" 
 set ylabel "R_{F/H}" 
 #set title "yes" 
 plot  3/(16*x**4)*(2*x*(x-atan(2*x)) + atanh(2*x**2/(1+2*x**2))) lw 3 title 'R_{F/H} Yukawa', 3/(4*x**4)*(-2+2*exp(-x**2)+sqrt(pi)*x*erf(x)) lw 3 title 'R_{F/H} Gogny'

unset multiplot

