set term postscript enhanced color font 'Helvetica,12'

#Format d'affichage


#Format du fichier de sortie

set output "isoton_param2.eps"




#######Début des plots
### Start multiplot (2x2 layout)

# color definitions
#set style line  1 lc rgb '#0060ad' pt 5 ps 0.2 lt 1 lw 2    # blue


# load line style definitions
#load 'parula.pal'

# Axes
set style line 101 lc rgb '#808080' lt 1
#set border 3 back ls 101
#set tics nomirror out scale 0.75
# Grid
set style line 102 lc rgb'#808080' lt 0 lw 1
set grid back ls 102







			#####################################
			#Multiplot1: Isot#####
			#####################################



set multiplot layout 2,2 rowsfirst #title 'Ajustement des canaux nucléaires'

unset key
set macros
set termopt enhanced
#set key samplen 2 spacing .9 font ",9"

#Print the informations on the top

#set label 2 @VAL  at screen 0.69, 0.97 font "Helvetica,7" 





# Label position
POS = "at graph 0.92,0.92 font ',12'"
POS2 = "at graph 0.55,0.92 font ',8'"
# Axes Numeration
NOXTICS = "set xlabel 'Nombre de protons'"
XTICS = "set xlabel 'Nombre de protons'"
NOYTICS = "set format y ''; unset ylabel"
YTICS = "set format y '%.0f'; set ylabel 'y'"
# Margins for each row resp. column
TMARGIN = "set tmargin at screen 0.95; set bmargin at screen 0.60"
BMARGIN = "set tmargin at screen 0.50; set bmargin at screen 0.15"
LMARGIN = "set lmargin at screen 0.08; set rmargin at screen 0.48"
RMARGIN = "set lmargin at screen 0.52; set rmargin at screen 0.92"

# Axes ranges
#set xrange [0:16]
#set yrange [-0.05:0.05]



# --- GRAPH a
@TMARGIN; @LMARGIN
@NOXTICS; @YTICS
# set xlabel '{/symbol r} (fm)' font ',12'
set ylabel '{/Symbol D} E_{tot}' font ',12'
LABEL="N=28"
set obj 10 rect at graph 0.9,0.10 size char strlen(LABEL)+2, char 2 fc rgb "white"
set label 10 LABEL at graph 0.9,0.10 front center font ",18"

#set xrange [0:1]
#set yrange [0:40]

plot "tot_isoton_SN2LO1.dat" every 2 i 0  u 2:($4-$5) with points pt 7 ps 1 lt rgb "#E9967A" title 'SN2LO1',\
     "tot_isoton_SN2LO1.dat" every 2 i 0 u 2:($4-$5) notitle w l  ls 12 lw 2 lt rgb "#E9967A"  ,\
     "tot_isoton_Sly5.dat" every 2 i 0 u 2:($4-$5)  with points pt 9 ps 1 lt rgb "#8B0000" title '{/Symbol c}-EFT' ,\
     "tot_isoton_Sly5.dat" every 2 i 0 u 2:($4-$5) w l notitle ls 11 lw 2 lt rgb "#8B0000" ,\
      0 w l lw 3 lt 3 dt 3 notitle
     #"EOS_SNM_ST_fit.dat" i 4 u 1:2  with points pt 13 ps 1 lt rgb "#556B2F" title 'MBTP' ,\
     #"EOS_SNM_ST_fit.dat" i 5 u 1:2 w l notitle  ls 15 lw 2 lt rgb "#556B2F"
unset ylabel      
#unset xlabel  


# --- GRAPH b
@TMARGIN; @RMARGIN
@NOXTICS; @NOYTICS 
#set xrange [0:1]
#set yrange [0:40]
set key box width +2 @POS
unset ylabel
set y2tics
set y2label '{/Symbol D} E_{tot}' font ',12' 
LABEL="N=50"
set obj 10 rect at graph 0.9,0.10 size char strlen(LABEL)+2, char 2 fc rgb "white"
set label 10 LABEL at graph 0.9,0.10 front center font ",18"
plot "tot_isoton_SN2LO1.dat" every 2 i 1 u 2:($4-$5) with points pt 7 ps 1 lt rgb "#E9967A" title 'SN2LO1',\
     "tot_isoton_SN2LO1.dat" every 2 i 1 u 2:($4-$5) notitle w l  ls 12 lw 2 lt rgb "#E9967A"  ,\
     "tot_isoton_Sly5.dat" every 2 i 1 u 2:($4-$5)  with points pt 9 ps 1 lt rgb "#8B0000" title 'SLy5' ,\
     "tot_isoton_Sly5.dat" every 2 i 1 u 2:($4-$5) w l notitle ls 11 lw 2 lt rgb "#8B0000" ,\
      0 w l lw 3 lt 3 dt 3 notitle
     #"EOS_SNM_ST_fit.dat" i 10 u 1:2  with points pt 13 ps 1 lt rgb "#556B2F" title 'MBTP' ,\
     #"EOS_SNM_ST_fit.dat" i 11 u 1:2 w l notitle  ls 15 lw 2 lt rgb "#556B2F"
#unset ylabel      
#unset xlabel 
unset ylabel  
unset y2label
unset y2tics
unset key
     
     
# --- GRAPH c
@BMARGIN; @LMARGIN
@XTICS; @YTICS
#set xrange [0:1]
#set yrange [-44:0]
set ylabel ' {/Symbol D} E_{tot}'  font ',12'
LABEL="N=82"
set obj 10 rect at graph 0.9,0.10 size char strlen(LABEL)+2, char 2 fc rgb "white"
set label 10 LABEL at graph 0.9,0.10 front center font ",18"

plot "tot_isoton_SN2LO1.dat" every 2 i 2 u 2:($4-$5) with points pt 7 ps 1 lt rgb "#E9967A" title 'SN2LO1',\
     "tot_isoton_SN2LO1.dat" every 2 i 2 u 2:($4-$5) notitle w l  ls 12 lw 2 lt rgb "#E9967A"  ,\
     "tot_isoton_Sly5.dat" every 2  i 2 u 2:($4-$5)  with points pt 9 ps 1 lt rgb "#8B0000" title 'SLy5' ,\
     "tot_isoton_Sly5.dat" every 2 i 2 u 2:($4-$5) w l notitle ls 11 lw 2 lt rgb "#8B0000" ,\
      0 w l lw 3 lt 3 dt 3 notitle
     #"EOS_SNM_ST_fit.dat" i 16 u 1:2  with points pt 13 ps 1 lt rgb "#556B2F" title 'MBTP' ,\
     #"EOS_SNM_ST_fit.dat" i 15 u 1:2 w l notitle  ls 15 lw 2 lt rgb "#556B2F"
unset ylabel      
#unset xlabel     
     
     
# --- GRAPH d
@BMARGIN; @RMARGIN
@XTICS; @NOYTICS
#set xrange [0:1]
set yrange [-4:18]
unset ylabel
set y2tics
set y2range [-4:18]
set y2label '{/Symbol D} E_{tot}' font ',12' 
LABEL="N=126"
set obj 10 rect at graph 0.9,0.10 size char strlen(LABEL)+2, char 2 fc rgb "white"
set label 10 LABEL at graph 0.9,0.10 front center font ",18"

plot "tot_isoton_SN2LO1.dat" every 2 i 3 u 2:($4-$5) with points pt 7 ps 1 lt rgb "#E9967A" title 'SN2LO1',\
     "tot_isoton_SN2LO1.dat" every 2 i 3 u 2:($4-$5) notitle w l  ls 12 lw 2 lt rgb "#E9967A"  ,\
     "tot_isoton_Sly5.dat" every 2 i 3 u 2:($4-$5)  with points pt 9 ps 1 lt rgb "#8B0000" title 'SLy5' ,\
     "tot_isoton_Sly5.dat"  every 2 i 3 u 2:($4-$5) w l notitle ls 11 lw 2 lt rgb "#8B0000" ,\
      0 w l lw 3 lt 3 dt 3 notitle
     #"EOS_SNM_ST_fit.dat" i 22 u 1:2  with points pt 13 ps 1 lt rgb "#556B2F" title 'MBTP' ,\
     #"EOS_SNM_ST_fit.dat" i 23 u 1:2 w l notitle  ls 15 lw 2 lt rgb "#556B2F"
#unset ylabel      
#unset xlabel  

unset multiplot
 reset

exit
