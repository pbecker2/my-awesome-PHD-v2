
# load line style definitions
#load 'parula.pal'

set style line 101 lc rgb '#808080' lt 1
set border 3 back ls 101

# Grid
set style line 102 lc rgb'#808080' lt 0 lw 1
set grid back ls 102
set key font ",18" width -7.5 at 340, 0.85 
 set output "CorrelationKm.eps" 
 set xrange [190:380]
 set yrange [0.55:1.1] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
set colorsequence podo
 set xlabel "K_{/Symbol \245} [MeV]" 
 set ylabel "m^*/m" 
 #set title "yes" 
 plot 'corr_nonN2LO.dat' u 2:1 w l lw 5 title '{/Symbol a} = 1/6', '' u 3:1 w l lw 5 title '{/Symbol a} = 1/3', '' u 4:1 w l lw 5 title '{/Symbol a} = 1/2', '' u 5:1 w l lw 5 title '{/Symbol a} = 1'





 
# load line style definitions
#load 'parula.pal'

set style line 101 lc rgb '#808080' lt 1
set border 3 back ls 101

# Grid
set style line 102 lc rgb'#808080' lt 0 lw 1
set grid back ls 102
set key font ",18" width -7.5 at 340, 0.85 
 set output "CorrelationKmN2LO.eps" 
 set xrange [190:380]
 set yrange [0.55:1.1] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
set colorsequence podo
 set xlabel "K_{/Symbol \245} [MeV]" 
 set ylabel "m^*/m" 
 #set title "yes" 
 plot 'corr_nonN2LO.dat' u 2:1 w l lw 5 title '{/Symbol a} = 1/6', '' u 3:1 w l lw 5 title '{/Symbol a} = 1/3', '' u 4:1 w l lw 5 title '{/Symbol a} = 1/2', '' u 5:1 w l lw 5 title '{/Symbol a} = 1', 'corr_param' u 1:2 ps 3 lc 6 title 'SN2LO1'


# load line style definitions
#load 'parula.pal'

set style line 101 lc rgb '#808080' lt 1
set border 3 back ls 101

# Grid
set style line 102 lc rgb'#808080' lt 0 lw 1
set grid back ls 102
set key font ",18" width -7.5 at 340, 0.85 
 set output "CorrelationKmSN2LO1.eps" 
 set xrange [190:380]
 set yrange [0.55:1.1] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
set colorsequence podo
 set xlabel "K_{/Symbol \245} [MeV]" 
 set ylabel "m^*/m" 
 #set title "yes" 
 plot 'corr_N2LO.dat' u 2:1 w l lw 5 title '{/Symbol a} = 1/6', '' u 3:1 w l lw 5 title '{/Symbol a} = 1/3', '' u 4:1 w l lw 5 title '{/Symbol a} = 1/2', '' u 5:1 w l lw 5 title '{/Symbol a} = 1', 'corr_param' u 1:2 ps 3 lc 6 title 'SN2LO1'

