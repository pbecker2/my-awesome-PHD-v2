 
#load line style definitions
load 'parula.pal'


set style line 101 lc rgb '#808080' lt 1
set border 3 back ls 101


# Grid
set style line 102 lc rgb'#808080' lt 0 lw 1
set grid back ls 102
 set output "a4.eps" 
set key font ",20" width 5 at 11.5, -0.25 
 set xrange [0:12.0]
 #set yrange [0:0.5] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
#set terminal pngcairo size 1000,900 enhanced font 'Helvetica,20'
set termoption dash
set colorsequence podo
 set xlabel "r [fm]" 
 set ylabel "A4(r) [MeV fm^4]" 
 #set title "yes" 
#LABEL="SN2LO1"
#set obj 10 rect at 2,-0.05 size char strlen(LABEL)+2, char 2 fc rgb "white"
#set label 10 LABEL at 2,-0.05 front center font ",24"
 pl "./champs/fieldsp_SN2LO1_Z=82._N=126.dat"  u 1:2 with lines ls 1 lw 6 lt rgb "#8B0000" title "SN2LO1 protons","./champs/fieldsn_SN2LO1_Z=82._N=126.dat"  u 1:2 with lines dt 2 lw 5 lc rgb "#8B0000" title "neutrons", "./champs/fieldsp_Sly5_Z=82._N=126.dat"  u 1:2 with lines ls 1 lw 5 lt rgb "#E9967A" title "SLy5 protons","./champs/fieldsn_Sly5_Z=82._N=126.dat"  u 1:2 with lines dt 2 lw 5 lt rgb "#E9967A" title "SLy5 neutrons" 

 set output "a3.eps"
set key font ",20" width 5 at 5, 0.27 
 set xrange [0:12.0]
 #set yrange [0:0.5] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
#set terminal pngcairo size 1000,900 enhanced font 'Helvetica,20'
set termoption dash
set colorsequence podo
 set xlabel "r [fm]" 
 set ylabel "A3(r) [MeV fm^3]" 
 #set title "yes" 
#LABEL="SN2LO1"
#set obj 10 rect at 2,0.25 size char strlen(LABEL)+2, char 2 fc rgb "white"
#set label 10 LABEL at 2,0.25 front center font ",24"
 pl "./champs/fieldsp_SN2LO1_Z=82._N=126.dat"  u 1:3 with lines ls 1 lw 6 lt rgb "#8B0000" title "SN2LO1 protons","./champs/fieldsn_SN2LO1_Z=82._N=126.dat"  u 1:3 with lines dt 2 lw 5 lc rgb "#8B0000" title "neutrons","./champs/fieldsp_Sly5_Z=82._N=126.dat"  u 1:3 with lines ls 1 lw 5 lt rgb "#FF8C00" title "SLy5 protons","./champs/fieldsn_Sly5_Z=82._N=126.dat"  u 1:3 with lines dt 2 lw 5 lt rgb "#FF8C00" title "SLy5 neutrons" 


 set output "a2r.eps"
set key font ",20" width 5 at 5, -23 
 set xrange [0:12.0]
 #set yrange [0:0.5] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
#set terminal pngcairo size 1000,900 enhanced font 'Helvetica,20'
set termoption dash
set colorsequence podo
 set xlabel "r [fm]" 
 set ylabel "A2R(r) [MeV fm^2]" 
 #set title "yes" 
#LABEL="SN2LO1"
#set obj 10 rect at 2,0.25 size char strlen(LABEL)+2, char 2 fc rgb "white"
#set label 10 LABEL at 2,0.25 front center font ",24"
 pl "./champs/fieldsp_SN2LO1_Z=82._N=126.dat"  u 1:4 with lines ls 1 lw 6 lt rgb "#8B0000" title "SN2LO1 protons","./champs/fieldsn_SN2LO1_Z=82._N=126.dat"  u 1:4 with lines dt 2 lw 5 lc rgb "#8B0000" title "neutrons", "./champs/fieldsp_Sly5_Z=82._N=126.dat"  u 1:4 with lines ls 1 lw 5 lt rgb "#E9967A" title "Sly5 protons","./champs/fieldsn_Sly5_Z=82._N=126.dat"  u 1:4 with lines dt 2 lw 5 lt rgb "#E9967A" title "neutrons"



 set output "a2c.eps"
set key font ",20" width 5 at 11, 0.6
 set xrange [0:12.0]
 #set yrange [0:0.5] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
#set terminal pngcairo size 1000,900 enhanced font 'Helvetica,20'
set termoption dash
set colorsequence podo
 set xlabel "r [fm]" 
 set ylabel "A2C(r) [MeV fm^4]" 
 #set title "yes" 
#LABEL="SN2LO1"
#set obj 10 rect at 2,0.25 size char strlen(LABEL)+2, char 2 fc rgb "white"
#set label 10 LABEL at 2,0.25 front center font ",24"
 pl "./champs/fieldsp_SN2LO1_Z=82._N=126.dat"  u 1:5 with lines ls 1 lw 6 lt rgb "#8B0000" title "SN2LO1 protons","./champs/fieldsn_SN2LO1_Z=82._N=126.dat"  u 1:5 with lines dt 2 lw 5 lc rgb "#8B0000" title "neutrons", "./champs/fieldsp_Sly5_Z=82._N=126.dat"  u 1:5 with lines ls 1 lw 5 lt rgb "#E9967A" title "Sly5 protons","./champs/fieldsn_Sly5_Z=82._N=126.dat"  u 1:5 with lines dt 2 lw 5 lt rgb "#E9967A" title "neutrons"

set output "a1r.eps"
set key font ",20" width 5 at 5,4
 set xrange [0:12.0]
 #set yrange [0:0.5] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
#set terminal pngcairo size 1000,900 enhanced font 'Helvetica,20'
set termoption dash
set colorsequence podo
 set xlabel "r [fm]" 
 set ylabel "A1R(r) [MeV fm]" 
 #set title "yes" 
#LABEL="SN2LO1"
#set obj 10 rect at 2,0.25 size char strlen(LABEL)+2, char 2 fc rgb "white"
#set label 10 LABEL at 2,0.25 front center font ",24"
 pl "./champs/fieldsp_SN2LO1_Z=82._N=126.dat"  u 1:6 with lines ls 1 lw 6 lt rgb "#8B0000" title "SN2LO1 protons","./champs/fieldsn_SN2LO1_Z=82._N=126.dat"  u 1:6 with lines dt 2 lw 5 lc rgb "#8B0000" title "neutrons", "./champs/fieldsp_Sly5_Z=82._N=126.dat"  u 1:6 with lines ls 1 lw 5 lt rgb "#E9967A" title "Sly5 protons","./champs/fieldsn_Sly5_Z=82._N=126.dat"  u 1:6 with lines dt 2 lw 5 lt rgb "#E9967A" title "neutrons"


set output "a1c.eps"
set key font ",20" width 5 at 11, -20
 set xrange [0:12.0]
 #set yrange [0:0.5] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
#set terminal pngcairo size 1000,900 enhanced font 'Helvetica,20'
set termoption dash
set colorsequence podo
 set xlabel "r [fm]" 
 set ylabel "A1C(r) [MeV fm^3]" 
 #set title "yes" 
#LABEL="SN2LO1"
#set obj 10 rect at 2,0.25 size char strlen(LABEL)+2, char 2 fc rgb "white"
#set label 10 LABEL at 2,0.25 front center font ",24"
 pl "./champs/fieldsp_SN2LO1_Z=82._N=126.dat"  u 1:7 with lines ls 1 lw 6 lt rgb "#8B0000" title "SN2LO1 protons","./champs/fieldsn_SN2LO1_Z=82._N=126.dat"  u 1:7 with lines dt 2 lw 5 lc rgb "#8B0000" title "neutrons", "./champs/fieldsp_Sly5_Z=82._N=126.dat"  u 1:7 with lines ls 1 lw 5 lt rgb "#E9967A" title "Sly5 protons","./champs/fieldsn_Sly5_Z=82._N=126.dat"  u 1:7 with lines dt 2 lw 5 lt rgb "#E9967A" title "neutrons"


set output "a0r.eps"
set key font ",20" width 5 at 11,2
 set xrange [0:12.0]
 #set yrange [0:0.5] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
#set terminal pngcairo size 1000,900 enhanced font 'Helvetica,20'
set termoption dash
set colorsequence podo
 set xlabel "r [fm]" 
 set ylabel "A0R(r) [MeV]" 
 #set title "yes" 
#LABEL="SN2LO1"
#set obj 10 rect at 2,0.25 size char strlen(LABEL)+2, char 2 fc rgb "white"
#set label 10 LABEL at 2,0.25 front center font ",24"
 pl "./champs/fieldsp_SN2LO1_Z=82._N=126.dat"  u 1:8 with lines ls 1 lw 6 lt rgb "#8B0000" title "SN2LO1 protons","./champs/fieldsn_SN2LO1_Z=82._N=126.dat"  u 1:8 with lines dt 2 lw 5 lc rgb "#8B0000" title "neutrons", "./champs/fieldsp_Sly5_Z=82._N=126.dat"  u 1:8 with lines ls 1 lw 5 lt rgb "#E9967A" title "Sly5 protons","./champs/fieldsn_Sly5_Z=82._N=126.dat"  u 1:8 with lines dt 2 lw 5 lt rgb "#E9967A" title "neutrons"


set output "a0c.eps"
set key font ",20" width 5 at 11.5,3000
 set xrange [0:12.0]
 #set yrange [0:0.5] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
#set terminal pngcairo size 1000,900 enhanced font 'Helvetica,20'
set termoption dash
set colorsequence podo
 set xlabel "r [fm]" 
 set ylabel "A0C(r) [MeV fm^2]"
 set multiplot
set object 1 rect from 1,500 to 8,2400
set object 1 rect fc rgb 'white' fillstyle solid 0.0 noborder 
 #set title "yes" 
#LABEL="SN2LO1"
#set obj 10 rect at 2,0.25 size char strlen(LABEL)+2, char 2 fc rgb "white"
#set label 10 LABEL at 2,0.25 front center font ",24"
 pl "./champs/fieldsp_SN2LO1_Z=82._N=126.dat"  u 1:9 with lines ls 1 lw 6 lt rgb "#8B0000" title "SN2LO1 protons","./champs/fieldsn_SN2LO1_Z=82._N=126.dat"  u 1:9 with lines dt 2 lw 5 lc rgb "#8B0000" title "neutrons", "./champs/fieldsp_Sly5_Z=82._N=126.dat"  u 1:9 with lines ls 1 lw 5 lt rgb "#E9967A" title "Sly5 protons","./champs/fieldsn_Sly5_Z=82._N=126.dat"  u 1:9 with lines dt 2 lw 5 lt rgb "#E9967A" title "neutrons"
#Small plot
set origin 0.15,0.25
set size 0.55,0.55
set xrange [0:12.0]
set yrange [0:12]
#set key font ",20" width 5 at 12,12
unset xlabel
unset ylabel
unset label
unset object 1
set tics scale 1 front
set xtics 2 
set ytics 2 
 pl "./champs/fieldsp_Sly5_Z=82._N=126.dat"  u 1:9 with lines ls 1 lw 5 lt rgb "#E9967A" title "Sly5 protons","./champs/fieldsn_Sly5_Z=82._N=126.dat"  u 1:9 with lines dt 2 lw 5 lt rgb "#E9967A" title "neutrons"

unset multiplot
reset 

#load line style definitions
load 'parula.pal'


set style line 101 lc rgb '#808080' lt 1
set border 3 back ls 101


# Grid
set style line 102 lc rgb'#808080' lt 0 lw 1
set grid back ls 102

set output "a0cc.eps"
set key font ",20" width 5 at 5,-0.05
 set xrange [0:12.0]
 #set yrange [0:0.5] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
#set terminal pngcairo size 1000,900 enhanced font 'Helvetica,20'
set termoption dash
set colorsequence podo
 set xlabel "r [fm]" 
 set ylabel "A0CC(r) [MeV fm^4]" 
 #set title "yes" 
#LABEL="SN2LO1"
#set obj 10 rect at 2,0.25 size char strlen(LABEL)+2, char 2 fc rgb "white"
#set label 10 LABEL at 2,0.25 front center font ",24"
 pl "./champs/fieldsp_SN2LO1_Z=82._N=126.dat"  u 1:11 with lines ls 1 lw 6 lt rgb "#8B0000" title "SN2LO1 protons","./champs/fieldsn_SN2LO1_Z=82._N=126.dat"  u 1:11 with lines dt 2 lw 5 lc rgb "#8B0000" title "neutrons", "./champs/fieldsp_Sly5_Z=82._N=126.dat"  u 1:11 with lines ls 1 lw 5 lt rgb "#E9967A" title "Sly5 protons","./champs/fieldsn_Sly5_Z=82._N=126.dat"  u 1:11 with lines dt 2 lw 5 lt rgb "#E9967A" title "neutrons"


set output "u.eps"
set key font ",20" width 5 at 5,-40
 set xrange [0:12.0]
 #set yrange [0:0.5] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
#set terminal pngcairo size 1000,900 enhanced font 'Helvetica,20'
set termoption dash
set colorsequence podo
 set xlabel "r [fm]" 
 set ylabel "U(r) [MeV]" 
 #set title "yes" 
#LABEL="SN2LO1"
#set obj 10 rect at 2,0.25 size char strlen(LABEL)+2, char 2 fc rgb "white"
#set label 10 LABEL at 2,0.25 front center font ",24"
 pl "./champs/fieldsp_SN2LO1_Z=82._N=126.dat"  u 1:12 with lines ls 1 lw 6 lt rgb "#8B0000" title "SN2LO1 protons","./champs/fieldsn_SN2LO1_Z=82._N=126.dat"  u 1:12 with lines dt 2 lw 5 lc rgb "#8B0000" title "neutrons", "./champs/fieldsp_Sly5_Z=82._N=126.dat"  u 1:12 with lines ls 1 lw 5 lt rgb "#E9967A" title "Sly5 protons","./champs/fieldsn_Sly5_Z=82._N=126.dat"  u 1:12 with lines dt 2 lw 5 lt rgb "#E9967A" title "neutrons"

set output "u4.eps"
set key font ",20" width 5 at 10,-0.8
 set xrange [0:12.0]
 #set yrange [0:0.5] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
#set terminal pngcairo size 1000,900 enhanced font 'Helvetica,20'
set termoption dash
set colorsequence podo
 set xlabel "r [fm]" 
 set ylabel "U4(r) [MeV]" 
 #set title "yes" 
#LABEL="SN2LO1"
#set obj 10 rect at 2,0.25 size char strlen(LABEL)+2, char 2 fc rgb "white"
#set label 10 LABEL at 2,0.25 front center font ",24"
 pl "./champs/fieldsp_SN2LO1_Z=82._N=126.dat"  u 1:13 with lines ls 1 lw 6 lt rgb "#8B0000" title "SN2LO1 protons","./champs/fieldsn_SN2LO1_Z=82._N=126.dat"  u 1:13 with lines dt 2 lw 5 lc rgb "#8B0000" title "neutrons", "./champs/fieldsp_Sly5_Z=82._N=126.dat"  u 1:13 with lines ls 1 lw 5 lt rgb "#E9967A" title "Sly5 protons","./champs/fieldsn_Sly5_Z=82._N=126.dat"  u 1:13 with lines dt 2 lw 5 lt rgb "#E9967A" title "neutrons"

set output "vdj.eps"
set key font ",20" width 5 at 11.5,-0.8
 set xrange [0:12.0]
 #set yrange [0:0.5] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
#set terminal pngcairo size 1000,900 enhanced font 'Helvetica,20'
set termoption dash
set colorsequence podo
 set xlabel "r [fm]" 
 set ylabel " D J(r) [MeV]" 
 #set title "yes" 
#LABEL="SN2LO1"
#set obj 10 rect at 2,0.25 size char strlen(LABEL)+2, char 2 fc rgb "white"
#set label 10 LABEL at 2,0.25 front center font ",24"
 pl "./champs/fieldsp_SN2LO1_Z=82._N=126.dat"  u 1:14 with lines ls 1 lw 6 lt rgb "#8B0000" title "SN2LO1 protons","./champs/fieldsn_SN2LO1_Z=82._N=126.dat"  u 1:14 with lines dt 2 lw 5 lc rgb "#8B0000" title "neutrons", "./champs/fieldsp_Sly5_Z=82._N=126.dat"  u 1:14 with lines ls 1 lw 5 lt rgb "#E9967A" title "Sly5 protons","./champs/fieldsn_Sly5_Z=82._N=126.dat"  u 1:14 with lines dt 2 lw 5 lt rgb "#E9967A" title "neutrons"

set output "vdd.eps"
set key font ",20" width 5 at 11.5,250
 set xrange [0:12.0]
 #set yrange [0:0.5] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
#set terminal pngcairo size 1000,900 enhanced font 'Helvetica,20'
set termoption dash
set colorsequence podo
 set xlabel "r [fm]" 
 set ylabel " Density Dependant term [MeV]" 
 #set title "yes" 
#LABEL="SN2LO1"
#set obj 10 rect at 2,0.25 size char strlen(LABEL)+2, char 2 fc rgb "white"
#set label 10 LABEL at 2,0.25 front center font ",24"
 pl "./champs/fieldsp_SN2LO1_Z=82._N=126.dat"  u 1:15 with lines ls 1 lw 6 lt rgb "#8B0000" title "SN2LO1 protons","./champs/fieldsn_SN2LO1_Z=82._N=126.dat"  u 1:15 with lines dt 2 lw 5 lc rgb "#8B0000" title "neutrons", "./champs/fieldsp_Sly5_Z=82._N=126.dat"  u 1:15 with lines ls 1 lw 5 lt rgb "#E9967A" title "Sly5 protons","./champs/fieldsn_Sly5_Z=82._N=126.dat"  u 1:15 with lines dt 2 lw 5 lt rgb "#E9967A" title "neutrons"

set output "vso.eps"
set key font ",20" width 5 at 11.5,2
 set xrange [0:12.0]
 #set yrange [0:0.5] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
#set terminal pngcairo size 1000,900 enhanced font 'Helvetica,20'
set termoption dash
set colorsequence podo
 set xlabel "r [fm]" 
 set ylabel " N1LO Spin-orbit term [MeV]" 
 #set title "yes" 
#LABEL="SN2LO1"
#set obj 10 rect at 2,0.25 size char strlen(LABEL)+2, char 2 fc rgb "white"
#set label 10 LABEL at 2,0.25 front center font ",24"
 pl "./champs/fieldsp_SN2LO1_Z=82._N=126.dat"  u 1:16 with lines ls 1 lw 6 lt rgb "#8B0000" title "SN2LO1 protons","./champs/fieldsn_SN2LO1_Z=82._N=126.dat"  u 1:16 with lines dt 2 lw 5 lc rgb "#8B0000" title "neutrons", "./champs/fieldsp_Sly5_Z=82._N=126.dat"  u 1:16 with lines ls 1 lw 5 lt rgb "#E9967A" title "Sly5 protons","./champs/fieldsn_Sly5_Z=82._N=126.dat"  u 1:16 with lines dt 2 lw 5 lt rgb "#E9967A" title "neutrons"

set output "vso42r.eps"
set key font ",20" width 5 at 12,-0.02
 set xrange [0:12.0]
 #set yrange [0:0.5] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
#set terminal pngcairo size 1000,900 enhanced font 'Helvetica,20'
set termoption dash
set colorsequence podo
 set xlabel "r [fm]" 
 set ylabel "VSO42R(r) [MeV fm^2]" 
 #set title "yes" 
#LABEL="SN2LO1"
#set obj 10 rect at 2,0.25 size char strlen(LABEL)+2, char 2 fc rgb "white"
#set label 10 LABEL at 2,0.25 front center font ",24"
 pl "./champs/fieldsp_SN2LO1_Z=82._N=126.dat"  u 1:17 with lines ls 1 lw 6 lt rgb "#8B0000" title "SN2LO1 protons","./champs/fieldsn_SN2LO1_Z=82._N=126.dat"  u 1:17 with lines dt 2 lw 5 lc rgb "#8B0000" title "neutrons", "./champs/fieldsp_Sly5_Z=82._N=126.dat"  u 1:17 with lines ls 1 lw 5 lt rgb "#E9967A" title "Sly5 protons","./champs/fieldsn_Sly5_Z=82._N=126.dat"  u 1:17 with lines dt 2 lw 5 lt rgb "#E9967A" title "neutrons"

set output "vso41r.eps"
set key font ",20" width 5 at 12,-0.01
 set xrange [0:12.0]
 #set yrange [0:0.5] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
#set terminal pngcairo size 1000,900 enhanced font 'Helvetica,20'
set termoption dash
set colorsequence podo
 set xlabel "r [fm]" 
 set ylabel "VSO41R(r) [MeV fm^2]" 
 #set title "yes" 
#LABEL="SN2LO1"
#set obj 10 rect at 2,0.25 size char strlen(LABEL)+2, char 2 fc rgb "white"
#set label 10 LABEL at 2,0.25 front center font ",24"
 pl "./champs/fieldsp_SN2LO1_Z=82._N=126.dat"  u 1:18 with lines ls 1 lw 6 lt rgb "#8B0000" title "SN2LO1 protons","./champs/fieldsn_SN2LO1_Z=82._N=126.dat"  u 1:18 with lines dt 2 lw 5 lc rgb "#8B0000" title "neutrons", "./champs/fieldsp_Sly5_Z=82._N=126.dat"  u 1:18 with lines ls 1 lw 5 lt rgb "#E9967A" title "Sly5 protons","./champs/fieldsn_Sly5_Z=82._N=126.dat"  u 1:18 with lines dt 2 lw 5 lt rgb "#E9967A" title "neutrons"

set output "vso40r.eps"
set key font ",20" width 5 at 12,-0.15
 set xrange [0:12.0]
 #set yrange [0:0.5] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
#set terminal pngcairo size 1000,900 enhanced font 'Helvetica,20'
set termoption dash
set colorsequence podo
 set xlabel "r [fm]" 
 set ylabel "VSO40R(r) [MeV]" 
 #set title "yes" 
#LABEL="SN2LO1"
#set obj 10 rect at 2,0.25 size char strlen(LABEL)+2, char 2 fc rgb "white"
#set label 10 LABEL at 2,0.25 front center font ",24"
 pl "./champs/fieldsp_SN2LO1_Z=82._N=126.dat"  u 1:19 with lines ls 1 lw 6 lt rgb "#8B0000" title "SN2LO1 protons","./champs/fieldsn_SN2LO1_Z=82._N=126.dat"  u 1:19 with lines dt 2 lw 5 lc rgb "#8B0000" title "neutrons", "./champs/fieldsp_Sly5_Z=82._N=126.dat"  u 1:19 with lines ls 1 lw 5 lt rgb "#E9967A" title "Sly5 protons","./champs/fieldsn_Sly5_Z=82._N=126.dat"  u 1:19 with lines dt 2 lw 5 lt rgb "#E9967A" title "neutrons"


set output "vso40c.eps"
set key font ",20" width 5 at 12,0.03
 set xrange [0:12.0]
 #set yrange [0:0.5] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
#set terminal pngcairo size 1000,900 enhanced font 'Helvetica,20'
set termoption dash
set colorsequence podo
 set xlabel "r [fm]" 
 set ylabel "VSO40C(r) [MeV fm^2]" 
 #set title "yes" 
#LABEL="SN2LO1"
#set obj 10 rect at 2,0.25 size char strlen(LABEL)+2, char 2 fc rgb "white"
#set label 10 LABEL at 2,0.25 front center font ",24"
 pl "./champs/fieldsp_SN2LO1_Z=82._N=126.dat"  u 1:20 with lines ls 1 lw 6 lt rgb "#8B0000" title "SN2LO1 protons","./champs/fieldsn_SN2LO1_Z=82._N=126.dat"  u 1:20 with lines dt 2 lw 5 lc rgb "#8B0000" title "neutrons", "./champs/fieldsp_Sly5_Z=82._N=126.dat"  u 1:20 with lines ls 1 lw 5 lt rgb "#E9967A" title "Sly5 protons","./champs/fieldsn_Sly5_Z=82._N=126.dat"  u 1:20 with lines dt 2 lw 5 lt rgb "#E9967A" title "neutrons"

set output "vcoul.eps"
set key font ",20" width 5 at 12,24
 set xrange [0:12.0]
 #set yrange [0:0.5] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
#set terminal pngcairo size 1000,900 enhanced font 'Helvetica,20'
set termoption dash
set colorsequence podo
 set xlabel "r [fm]" 
 set ylabel "V Coulomb (r) [MeV]" 
 #set title "yes" 
#LABEL="SN2LO1"
#set obj 10 rect at 2,0.25 size char strlen(LABEL)+2, char 2 fc rgb "white"
#set label 10 LABEL at 2,0.25 front center font ",24"
 pl "./champs/fieldsp_SN2LO1_Z=82._N=126.dat"  u 1:21 with lines ls 1 lw 6 lt rgb "#8B0000" title "SN2LO1 protons","./champs/fieldsp_Sly5_Z=82._N=126.dat"  u 1:21 with lines ls 1 lw 5 lt rgb "#E9967A" title "Sly5 protons"


set output "vcoulex.eps"
set key font ",20" width 5 at 5,-0.02
 set xrange [0:12.0]
 #set yrange [0:0.5] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
#set terminal pngcairo size 1000,900 enhanced font 'Helvetica,20'
set termoption dash
set colorsequence podo
 set xlabel "r [fm]" 
 set ylabel "V Coulomb Exchange (r) [MeV]" 
 #set title "yes" 
#LABEL="SN2LO1"
#set obj 10 rect at 2,0.25 size char strlen(LABEL)+2, char 2 fc rgb "white"
#set label 10 LABEL at 2,0.25 front center font ",24"
 pl "./champs/fieldsp_SN2LO1_Z=82._N=126.dat"  u 1:22 with lines ls 1 lw 6 lt rgb "#8B0000" title "SN2LO1 protons","./champs/fieldsp_Sly5_Z=82._N=126.dat"  u 1:22 with lines ls 1 lw 5 lt rgb "#E9967A" title "Sly5 protons"

set output "vkmunu.eps"
set key font ",20" width 5 at 12,0.015
 set xrange [0:12.0]
 #set yrange [0:0.5] 
 set terminal postscript eps enhanced color font 'Helvetica,20'
#set terminal pngcairo size 1000,900 enhanced font 'Helvetica,20'
set termoption dash
set colorsequence podo
 set xlabel "r [fm]" 
 set ylabel "V K_{{/Symbol m n}}(r) [MeV]" 
 #set title "yes" 
#LABEL="SN2LO1"
#set obj 10 rect at 2,0.25 size char strlen(LABEL)+2, char 2 fc rgb "white"
#set label 10 LABEL at 2,0.25 front center font ",24"
 pl "./champs/fieldsp_SN2LO1_Z=82._N=126.dat"  u 1:23 with lines ls 1 lw 6 lt rgb "#8B0000" title "SN2LO1 protons","./champs/fieldsn_SN2LO1_Z=82._N=126.dat"  u 1:23 with lines dt 2 lw 5 lc rgb "#8B0000" title "neutrons", "./champs/fieldsp_Sly5_Z=82._N=126.dat"  u 1:23 with lines ls 1 lw 5 lt rgb "#E9967A" title "Sly5 protons","./champs/fieldsn_Sly5_Z=82._N=126.dat"  u 1:23 with lines dt 2 lw 5 lt rgb "#E9967A" title "neutrons"

exit
