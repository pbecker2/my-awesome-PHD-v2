set term postscript enhanced  font 'Helvetica,12'

#Format d'affichage


#Format du fichier de sortie

set output "EOS_SNM_param.eps"




#######Début des plots
### Start multiplot (2x2 layout)

# color definitions
#set style line  1 lc rgb '#0060ad' pt 5 ps 0.2 lt 1 lw 2    # blue

# load line style definitions
#load 'parula.pal'

# Axes
set style line 101 lc rgb '#808080' lt 1
#set border 3 back ls 101
#set tics nomirror out scale 0.75
# Grid
set style line 102 lc rgb'#808080' lt 0 lw 1
set grid back ls 102







			#####################################
			#Multiplot1: EOS               #####
			#####################################



set multiplot layout 2,2 rowsfirst #title 'Equations d'états dans la matière infinie' spacing screen 0.05

unset key
set macros
set termopt dash
#set key samplen 2 spacing .9 font ",9"

#Print the informations on the top

#set label 2 @VAL  at screen 0.69, 0.97 font "Helvetica,7" 





# Label position
POS = "at graph 0.40,0.9 font ',12'"
POS2 = "at graph 0.55,0.92 font ',8'"
# Axes Numeration
NOXTICS = "set xtics ('' 0,'' 0.1,'' 0.2,'' 0.3, '' 0.4); \
          unset xlabel"
XTICS = "set xtics ('0' 0,'0.1' 0.1,'0.2' 0.2,'0.3' 0.3, '0.4' 0.4);\
          set xlabel '{/Symbol r} [fm^{-3}]'"
NOYTICS = "set format y ''; unset ylabel"
YTICS = "set format y '%.0f'; set ylabel 'y'"
# Margins for each row resp. column
TMARGIN = "set tmargin at screen 0.98; set bmargin at screen 0.59"
BMARGIN = "set tmargin at screen 0.49; set bmargin at screen 0.10"
LMARGIN = "set lmargin at screen 0.10; set rmargin at screen 0.49"
RMARGIN = "set lmargin at screen 0.51; set rmargin at screen 0.90"

# Axes ranges
#set xrange [0:16]
#set yrange [-0.05:0.05]



# --- GRAPH a
@TMARGIN; @LMARGIN
@XTICS; @YTICS
# set xlabel '{/symbol r} (fm)' font ',12'
set ylabel ' E\A [MeV]' font ',12'
set label 'Matiere symetrique' at  0.05,20 font ',14'

#set xrange [0:0.35]
#set yrange [-18:15]

plot "./param/eos_SLy4.gfx" u 1:2  with lines ls 2  lt 2 lc rgb "#8B0000" title 'SLy4' ,\
     "./param/eos_fitN2LOb.gfx" u 1:2  w l  lt 1 lc rgb "#556B2F" title 'SN2LO1' 
unset ylabel
unset label      
#unset xlabel  


# --- GRAPH b
@TMARGIN; @RMARGIN
@XTICS; @NOYTICS 
#set xrange [0:0.35]
#set yrange [-18:15]
#set y2range [-18:15]
set key box @POS width +1
set label 'Matiere de neutron' at  0.25,10 font ',14'
unset ylabel
#unset ytics
set y2tics
set y2label 'E/N [MeV]' font ',12' 
plot "./param/eos_SLy4.gfx" u 1:3  with lines ls 2  lt 2 lc rgb "#8B0000" title 'SLy5' ,\
     "./param/eos_fitN2LOb.gfx" u 1:3  w l  lt 1 lc rgb "#556B2F" title 'SN2LO1' 
#unset ylabel      
#unset xlabel 
unset ylabel  
unset y2label
unset y2tics
unset key
unset label
     
     
# --- GRAPH c
@BMARGIN; @LMARGIN
@XTICS; @YTICS
#set xrange [0:0.35]
#set yrange [-30:0]
set ylabel ' E/A [MeV]'  font ',12'
set label 'Matiere polarisee' at  0.05,160 font ',14'
set ytics
plot "./param/eos_SLy4.gfx" u 1:4  with lines ls 2  lt 2 lc rgb "#8B0000" title 'SLy5' ,\
     "./param/eos_fitN2LOb.gfx" u 1:4  w l  lt 1 lc rgb "#556B2F" title 'SN2LO1' 
unset ylabel
unset label     
#unset xlabel     
     
     
# --- GRAPH d
@BMARGIN; @RMARGIN
@XTICS; @NOYTICS
#set xrange [0:0.35]
#set yrange [-30:0]
#set y2range [-30:0]
unset ylabel
set y2tics
#set y2range [-30:0]
set ytics
set y2label 'E/N [MeV]' font ',12' 
set label 'Matiere de neutron polarisee' at  0.15,15 font ',14'
plot "./param/eos_SLy4.gfx" u 1:5  with lines ls 2  lt 2 lc rgb "#8B0000" title 'SLy5' ,\
     "./param/eos_fitN2LOb.gfx" u 1:5  w l  lt 1 lc rgb "#556B2F" title 'SN2LO1' 
unset ytics
#unset ylabel 
unset label     
#unset xlabel  

unset multiplot
 reset

exit
