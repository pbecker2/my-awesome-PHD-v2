set term postscript enhanced  font 'Helvetica,12'

#Format d'affichage


#Format du fichier de sortie

set output "EOS_SNM_ST_param.eps"




#######Début des plots
### Start multiplot (2x2 layout)

# color definitions
#set style line  1 lc rgb '#0060ad' pt 5 ps 0.2 lt 1 lw 2    # blue

# load line style definitions
#load 'parula.pal'

# Axes
set style line 101 lc rgb '#808080' lt 1
#set border 3 back ls 101
#set tics nomirror out scale 0.75
# Grid
set style line 102 lc rgb'#808080' lt 0 lw 1
set grid back ls 102







			#####################################
			#Multiplot1: Rho and derivatives#####
			#####################################



set multiplot layout 2,2 rowsfirst #title 'Ajustement des canaux nucléaires'

unset key
set macros
set termopt dash
#set key samplen 2 spacing .9 font ",9"

#Print the informations on the top

#set label 2 @VAL  at screen 0.69, 0.97 font "Helvetica,7" 





# Label position
POS = "at graph 0.40,0.9 font ',12'"
POS2 = "at graph 0.55,0.92 font ',8'"
# Axes Numeration
NOXTICS = "set xtics ('' 0,'' 0.1,'' 0.2,'' 0.3); \
          unset xlabel"
XTICS = "set xtics ('0' 0,'0.1' 0.1,'0.2' 0.2,'0.3' 0.3);\
          set xlabel '{/Symbol r} [fm^{-3}]'"
NOYTICS = "set format y ''; unset ylabel"
YTICS = "set format y '%.0f'; set ylabel 'y'"
# Margins for each row resp. column
TMARGIN = "set tmargin at screen 0.90; set bmargin at screen 0.55"
BMARGIN = "set tmargin at screen 0.55; set bmargin at screen 0.20"
LMARGIN = "set lmargin at screen 0.10; set rmargin at screen 0.50"
RMARGIN = "set lmargin at screen 0.50; set rmargin at screen 0.90"

# Axes ranges
#set xrange [0:16]
#set yrange [-0.05:0.05]



# --- GRAPH a
@TMARGIN; @LMARGIN
@NOXTICS; @YTICS
# set xlabel '{/symbol r} (fm)' font ',12'
set ylabel ' (S=0,T=0)' font ',12'

set xrange [0:0.35]
set yrange [-18:15]

plot "EOS_SNM_ST_fit.dat" i 0 u 1:2 with points pt 7 ps 1 lt rgb "#E9967A" title 'BHF',\
     "./compose_param/SkyrmeST.dat" u 2:3  with lines ls 2  lt 2 lc rgb "#8B0000" title 'SN2LO1' ,\
     "./ST_channel/eos_SLY5.out" u 1:6  with lines ls 2  lt 6 lc rgb "#556B2F" title 'SLy5' 
     # "./ST_channel/eos_VLyB_NL0.out" u 1:3  w l lt 6 lc rgb "#556B2F" title 'NLO' ,\
     # "./ST_channel/eos_VLyB_N2L0.out" u 1:3  w l  lt 1 lc rgb "#556B2F" title 'N2LO' 
unset ylabel      
#unset xlabel  


# --- GRAPH b
@TMARGIN; @RMARGIN
@NOXTICS; @NOYTICS 
set xrange [0:0.35]
set yrange [-18:15]
set y2range [-18:15]
set key box @POS width +1
unset ylabel
#unset ytics
set y2tics
set y2label '(S=1,T=1)' font ',12' 
plot "EOS_SNM_ST_fit.dat" i 6 u 1:2 with points pt 7 ps 1 lt rgb "#E9967A" title 'BHF',\
      "./compose_param/SkyrmeST.dat" u 2:6  with lines ls 2  lt 2 lc rgb "#8B0000" title 'SN2LO1' ,\
     "./ST_channel/eos_SLY5.out" u 1:6  with lines ls 2  lt 6 lc rgb "#556B2F" title 'SLy5' 
   #  "./ST_channel/eos_VLyB_NL0.out" u 1:6  w l lt 6 lc rgb "#556B2F" title 'NLO' ,\
   #  "./ST_channel/eos_VLyB_N2L0.out" u 1:6  w l  lt 1 lc rgb "#556B2F" title 'N2LO'
#unset ylabel      
#unset xlabel 
unset ylabel  
unset y2label
unset y2tics
unset key
     
     
# --- GRAPH c
@BMARGIN; @LMARGIN
@XTICS; @YTICS
set xrange [0:0.35]
set yrange [-30:0]
set ylabel ' (S=1,T=0)'  font ',12'
set ytics
plot "EOS_SNM_ST_fit.dat" i 12 u 1:2 with points pt 7 ps 1 lt rgb "#E9967A" title 'HFB',\
      "./compose_param/SkyrmeST.dat" u 2:5  with lines ls 2  lt 2 lc rgb "#8B0000" title 'SN2LO1' ,\
     "./ST_channel/eos_SLY5.out" u 1:5  with lines ls 2  lt 6 lc rgb "#556B2F" title 'SLy5' 
   #  "./ST_channel/eos_VLyB_NL0.out" u 1:5  w l lt 6 lc rgb "#556B2F" title 'NLO' ,\
   #  "./ST_channel/eos_VLyB_N2L0.out" u 1:5 w l  lt 1 lc rgb "#556B2F" title 'N2LO'
unset ylabel     
#unset xlabel     
     
     
# --- GRAPH d
@BMARGIN; @RMARGIN
@XTICS; @NOYTICS
set xrange [0:0.35]
set yrange [-30:0]
set y2range [-30:0]
unset ylabel
set y2tics
set y2range [-30:0]
set y2label '(S=0,T=1)' font ',12' 

plot "EOS_SNM_ST_fit.dat" i 18 u 1:2 with points pt 7 ps 1 lt rgb "#E9967A" title 'HFB',\
     "./compose_param/SkyrmeST.dat" u 2:4  with lines ls 2  lt 2 lc rgb "#8B0000" title 'SN2LO1' ,\
     "./ST_channel/eos_SLY5.out" u 1:4  with lines smooth sbezier lw 2 lt 6 lc rgb "#8B0000" title 'SLy5' 
    # "./ST_channel/eos_VLyB_NL0.out" u 1:4  w l lt 6 lc rgb "#556B2F" title 'NLO' ,\
    # "./ST_channel/eos_VLyB_N2L0.out" u 1:4  w l  lt 1 lc rgb "#556B2F" title 'N2LO'
unset ytics
#unset ylabel      
#unset xlabel  

unset multiplot
 reset

exit
