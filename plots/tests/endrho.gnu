set term postscript eps enhanced color font 'Helvetica,18'

set output 'endrho.eps'

# Axes
set style line 101 lc rgb '#808080' lt 2
set border 3 back ls 101
set tics nomirror out scale 0.75
# Grid
set style line 102 lc rgb'#808080' lt 0 lw 2
set grid back ls 102
set macros
set termopt enhanced
set key at 25,-2 spacing 1.6 font ",14"
set colors podo 
set xlabel 'r [fm]'
set ylabel 'log_{10} {/Symbol r} (r)'
plot 'densités_Sly4_Z=6._N=6_cut0..dat' u 1:(log10($2)) w l lw 2 title '^{12} C', 'densités_Sly4_Z=8._N=8_cut0..dat' u 1:(log10($2)) w l lw 2 title '^{16} O', 'densités_Sly4_Z=20._N=20_cut0..dat' u 1:(log10($2)) w l lw 2 title '^{40} Ca', 'densités_Sly4_Z=20._N=22_cut0..dat' u 1:(log10($2)) w l lw 2 title '^{42} Ca', 'densités_Sly4_Z=20._N=28_cut0..dat' u 1:(log10($2)) w l lw 2  title '^{48} Ca', 'densités_Sly4_Z=82._N=126_cut0.dat' u 1:(log10($2)) w l lw 2 title '^{208} Pb'



set output 'endrhocut.eps'

# Axes
set style line 101 lc rgb '#808080' lt 2
set border 3 back ls 101
set tics nomirror out scale 0.75
# Grid
set style line 102 lc rgb'#808080' lt 0 lw 2
set grid back ls 102
set macros
set termopt enhanced
set key at 25,-2 spacing 1.6 font ",14"
#set podo colorsequence
set xlabel 'r [fm]'
set ylabel 'log_{10} {/Symbol r} (r)'
plot 'densités_Sly4_Z=6._N=6..dat' u 1:(log10($2)) w l lw 2 title '^{12} C', 'densités_Sly4_Z=8._N=8..dat' u 1:(log10($2)) w l lw 2 title '^{16} O', 'densités_Sly4_Z=20._N=20..dat' u 1:(log10($2)) w l lw 2 title '^{40} Ca', 'densités_Sly4_Z=20._N=22..dat' u 1:(log10($2)) w l lw 2 title '^{42} Ca', 'densités_Sly4_Z=20._N=28..dat' u 1:(log10($2)) w l lw 2  title '^{48} Ca', 'densités_Sly4_Z=82._N=126.dat' u 1:(log10($2)) w l lw 2 title '^{208} Pb'



exit
