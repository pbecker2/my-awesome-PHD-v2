set term postscript eps enhanced color font 'Helvetica,20'

set output 'cutoff.eps'

# Axes
set style line 101 lc rgb '#808080' lt 2
set border 3 back ls 101
set tics nomirror out scale 0.75
# Grid
set style line 102 lc rgb'#808080' lt 0 lw 2
set grid back ls 102
set macros
set termopt enhanced
set key at 1000,9.4 spacing 1.6 font ",18"
set label 1 at 380, 9.0 'E_{Lenteur} (^{12} C) = 90.588 MeV' font ",16"
set label 2 at 380, 8.2 'E_{Lenteur} (^{16} O) = 128.498 MeV' font ",16"
set label 3 at 380, 7.6 'E_{Lenteur} (^{40} Ca) = 344.262 MeV'  font ",16"
set label 4 at 380, 6.8 'E_{Lenteur} (^{42} Ca) = 362.539 MeV' font ",16"
set label 5 at 380, 6.0 'E_{Lenteur} (^{48} Ca) = 417.912 MeV' font ",16"
set label 6 at 380, 5.2 'E_{Lenteur} (^{126} Pb) = 1635.694 MeV' font ",16"

set xlabel 'Coupure sur la base Wood-Saxon [MeV]'
set ylabel '{/Symbol D} E : E_{Whisky} - E_{Lenteur} [MeV]'
plot 'tot_cutoff.dat' i 3 u 3:($4+90.587692016997821) ps 2 title '^{12} C', '' i 5 u 3:($4+128.49796836501719) ps 2 title '^{16} O', '' i 0 u 3:($4+344.26217888208993) ps 2 title '^{40} Ca', '' i 1 u 3:($4+362.53900237843453) ps 2 title '^{42} Ca', '' i 2 u 3:($4+417.91167607480941) ps 2 pt 8 title '^{48} Ca', '' i 4 u 3:($4+1635.6940498744782) ps 2 title '^{208} Pb'

reset 


set term postscript eps enhanced color font 'Helvetica,20'
set output 'cutoff_mf.eps'

# Axes
set style line 101 lc rgb '#808080' lt 2
set border 3 back ls 101
set tics nomirror out scale 0.75
# Grid
set style line 102 lc rgb'#808080' lt 0 lw 2
set grid back ls 102
set macros
set termopt enhanced
set key at 1000,9.4 spacing 1.6 font ",18"
set label 1 at 380, 9.0 'E_{Lenteur} (^{12} C) = 90.588 MeV' font ",16"
set label 2 at 380, 8.6 'E_{Lenteur} (^{16} O) = 128.498 MeV' font ",16"
set label 3 at 380, 8 'E_{Lenteur} (^{40} Ca) = 344.262 MeV'  font ",16"
set label 4 at 380, 7.4 'E_{Lenteur} (^{42} Ca) = 362.539 MeV' font ",16"
set label 5 at 380, 6.8 'E_{Lenteur} (^{48} Ca) = 417.912 MeV' font ",16"
set label 6 at 380, 6.2 'E_{Lenteur} (^{126} Pb) = 1635.694 MeV' font ",16"

set xlabel 'Coupure sur la base Wood-Saxon [MeV]'
set ylabel 'Précision de la formule magique'
plot 'tot_cutoff.dat' i 3 u 3:5 ps 2 title '^{12} C', '' i 5 u 3:5 ps 2 title '^{16} O', '' i 0 u 3:5 ps 2 title '^{40} Ca', '' i 1 u 3:5 ps 2 title '^{42} Ca', '' i 2 u 3:5 ps 2 pt 8 title '^{48} Ca', '' i 4 u 3:5 ps 2 title '^{126} Pb'


exit
