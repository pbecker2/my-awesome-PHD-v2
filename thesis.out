\BOOKMARK [0][-]{chapter.1}{Introduction}{}% 1
\BOOKMARK [0][-]{chapter.2}{Interaction nucléon-nucléon et interaction effective}{}% 2
\BOOKMARK [1][-]{section.2.1}{Du noyau à l'interaction nucléon-nucléon effective}{chapter.2}% 3
\BOOKMARK [2][-]{subsection.2.1.1}{Résoudre le problème à N-corps}{section.2.1}% 4
\BOOKMARK [2][-]{subsection.2.1.2}{Hamiltonien microscopique à N-corps}{section.2.1}% 5
\BOOKMARK [1][-]{section.2.2}{Forme générale d'un potentiel à deux corps}{chapter.2}% 6
\BOOKMARK [1][-]{section.2.3}{Pseudo-potentiels effectifs phénoménologiques}{chapter.2}% 7
\BOOKMARK [2][-]{subsection.2.3.1}{Pseudo-potentiel effectif de portée nulle : Skyrme}{section.2.3}% 8
\BOOKMARK [2][-]{subsection.2.3.2}{Pseudo-potentiel de portée finie: potentiel de Gogny}{section.2.3}% 9
\BOOKMARK [2][-]{subsection.2.3.3}{Pseudo-potentiel de portée finie: potentiel M3Y}{section.2.3}% 10
\BOOKMARK [1][-]{section.2.4}{Extension de Skyrme en gradients jusqu'à l'ordre 6 \(N3LO\)}{chapter.2}% 11
\BOOKMARK [0][-]{chapter.3}{Propriétés des potentiels effectifs dans la matière nucléaire infinie}{}% 12
\BOOKMARK [1][-]{section.3.1}{Introduction à la matière nucléaire infinie symétrique}{chapter.3}% 13
\BOOKMARK [1][-]{section.3.2}{Equations d'états de Skyrme N3LO dans la matière infinie}{chapter.3}% 14
\BOOKMARK [2][-]{subsection.3.2.1}{Matière infinie symétrique}{section.3.2}% 15
\BOOKMARK [2][-]{subsection.3.2.2}{Matière infinie de neutrons}{section.3.2}% 16
\BOOKMARK [2][-]{subsection.3.2.3}{Matière infinie asymétrique}{section.3.2}% 17
\BOOKMARK [2][-]{subsection.3.2.4}{Matière infinie polarisée}{section.3.2}% 18
\BOOKMARK [1][-]{section.3.3}{Pseudo-observables de la matière infinie}{chapter.3}% 19
\BOOKMARK [1][-]{section.3.4}{Étude préliminaire: ajustement des canaux nucléaires et des ondes partielles avec Skyrme N2LO et N3LO}{chapter.3}% 20
\BOOKMARK [2][-]{subsection.3.4.1}{Ajustement des canaux nucléaires avec Skyrme N2LO et Skyrme N3LO}{section.3.4}% 21
\BOOKMARK [2][-]{subsection.3.4.2}{Ajustement de la décompositon en ondes partielles de l'équation d'état avec Skyrme N2LO et Skyrme N3LO}{section.3.4}% 22
\BOOKMARK [1][-]{section.3.5}{Application de Skyrme N3LO à l'astrophysique}{chapter.3}% 23
\BOOKMARK [2][-]{subsection.3.5.1}{Introduction aux étoiles à neutrons}{section.3.5}% 24
\BOOKMARK [2][-]{subsection.3.5.2}{Ajustement\040de\040Skyrme\040N3LO}{section.3.5}% 25
\BOOKMARK [2][-]{subsection.3.5.3}{Propriétés résultant de l'ajustement}{section.3.5}% 26
\BOOKMARK [0][-]{chapter.4}{Comparaison\040du\040potentiel\040de\040Skyrme\040N3LO\040avec\040les\040potentiels\040de\040Gogny\040et\040M3Y}{}% 27
\BOOKMARK [1][-]{section.4.1}{Equations d'états globales des potentiels à portée finie}{chapter.4}% 28
\BOOKMARK [1][-]{section.4.2}{Décomposition en canaux \(S,T\) des équations d'états pour les potentiels à portée finie}{chapter.4}% 29
\BOOKMARK [1][-]{section.4.3}{Décomposition des équations d'états des interactions M3Y et Gogny en ondes partielles}{chapter.4}% 30
\BOOKMARK [1][-]{section.4.4}{Compléments sur la troisième gaussienne de l'interaction de Gogny}{chapter.4}% 31
\BOOKMARK [1][-]{section.4.5}{De la portée finie à la portée nulle}{chapter.4}% 32
\BOOKMARK [2][-]{subsection.4.5.1}{Invariance\040de\040jauge\040locale}{section.4.5}% 33
\BOOKMARK [2][-]{subsection.4.5.2}{Interaction\040M3Y}{section.4.5}% 34
\BOOKMARK [2][-]{subsection.4.5.3}{Interaction\040de\040Gogny}{section.4.5}% 35
\BOOKMARK [2][-]{subsection.4.5.4}{Conclusion sur le lien entre portée finie et portée nulle}{section.4.5}% 36
\BOOKMARK [0][-]{chapter.5}{Utilisation de Skyrme N2LO dans le cadre du champ moyen en symétrie sphérique}{}% 37
\BOOKMARK [1][-]{section.5.1}{Introduction}{chapter.5}% 38
\BOOKMARK [1][-]{section.5.2}{Fonctionnelle\040de\040Skyrme\040N2LO}{chapter.5}% 39
\BOOKMARK [1][-]{section.5.3}{Equation\040du\040champ-moyen\040de\040Skyrme\040N2LO}{chapter.5}% 40
\BOOKMARK [2][-]{subsection.5.3.1}{Densités de Skyrme NLO en symétrie sphérique}{section.5.3}% 41
\BOOKMARK [2][-]{subsection.5.3.2}{Nouvelles densités de N2LO en symétrie sphérique}{section.5.3}% 42
\BOOKMARK [2][-]{subsection.5.3.3}{Équation du champ-moyen de Skyrme N2LO en symétrie sphérique}{section.5.3}% 43
\BOOKMARK [2][-]{subsection.5.3.4}{Comportement de la fonction d'onde du système à l'origine pour Skyrme N2LO}{section.5.3}% 44
\BOOKMARK [2][-]{subsection.5.3.5}{Comportement des termes dépendants de la densité 0\(r\) à l'origine dans Skyrme N2LO}{section.5.3}% 45
\BOOKMARK [1][-]{section.5.4}{Résolution numérique de Skyrme N2LO à l'approximation Hartree-Fock: WHISKY2}{chapter.5}% 46
\BOOKMARK [2][-]{subsection.5.4.1}{Méthode de Hooverman}{section.5.4}% 47
\BOOKMARK [2][-]{subsection.5.4.2}{Application de la méthode de Hooverman au cas pratique de l'oscillateur harmonique}{section.5.4}% 48
\BOOKMARK [2][-]{subsection.5.4.3}{Utilisation d'une méthode à deux bases}{section.5.4}% 49
\BOOKMARK [2][-]{subsection.5.4.4}{Tests numériques effectués avec WHISKY2}{section.5.4}% 50
\BOOKMARK [2][-]{subsection.5.4.5}{Implémentation de l'appariement dans WHISKY2}{section.5.4}% 51
\BOOKMARK [1][-]{section.5.5}{Perspectives\040sur\040Skyrme\040N3LO\040pour\040le\040calcul\040de\040noyaux}{chapter.5}% 52
\BOOKMARK [2][-]{subsection.5.5.1}{Fonctionnelle préliminaire de Skyrme N3LO}{section.5.5}% 53
\BOOKMARK [2][-]{subsection.5.5.2}{Densités de Skyrme N3LO}{section.5.5}% 54
\BOOKMARK [0][-]{chapter.6}{Ajustement\040de\040Skyrme\040N2L0}{}% 55
\BOOKMARK [1][-]{section.6.1}{Méthode d'ajustement}{chapter.6}% 56
\BOOKMARK [1][-]{section.6.2}{Contraintes\040d'ajustement}{chapter.6}% 57
\BOOKMARK [2][-]{subsection.6.2.1}{Matière infinie symétrique et asymétrique}{section.6.2}% 58
\BOOKMARK [2][-]{subsection.6.2.2}{Contraintes assurant la stabilité de l'interaction}{section.6.2}% 59
\BOOKMARK [2][-]{subsection.6.2.3}{Contraintes associées aux calculs de noyaux}{section.6.2}% 60
\BOOKMARK [1][-]{section.6.3}{Stratégie d'ajustement}{chapter.6}% 61
\BOOKMARK [1][-]{section.6.4}{Résultats}{chapter.6}% 62
\BOOKMARK [2][-]{subsection.6.4.1}{Propriétés de la matière infinie}{section.6.4}% 63
\BOOKMARK [2][-]{subsection.6.4.2}{Stabilité de l'interaction}{section.6.4}% 64
\BOOKMARK [2][-]{subsection.6.4.3}{Propriétés des noyaux}{section.6.4}% 65
\BOOKMARK [2][-]{subsection.6.4.4}{Perspectives générales pour un futur ajustement et note d'intention}{section.6.4}% 66
\BOOKMARK [0][-]{chapter.7}{Conclusions\040et\040Perspectives}{}% 67
\BOOKMARK [0][-]{appendix.A}{Ondes\040partielles\040dans\040le\040potentiel\040de\040Skyrme}{}% 68
\BOOKMARK [1][-]{section.A.1}{Contributions\040des\040ondes\040partielles\040dans\040le\040potentiel\040de\040Skyrme}{appendix.A}% 69
\BOOKMARK [0][-]{appendix.B}{Compléments sur la matière infinie}{}% 70
\BOOKMARK [1][-]{section.B.1}{Equation d'état de la matière infinie asymétrique}{appendix.B}% 71
\BOOKMARK [1][-]{section.B.2}{Compléments sur l'utilisation de Skyrme N3LO pour les étoiles à neutrons}{appendix.B}% 72
\BOOKMARK [0][-]{appendix.C}{Formulaire\040pour\040les\040interactions\040de\040Gogny\040et\040de\040M3Y}{}% 73
\BOOKMARK [1][-]{section.C.1}{Pseudo-observables de la matière infinie des potentiels à portée finie de Gogny et M3Y}{appendix.C}% 74
\BOOKMARK [2][-]{subsection.C.1.1}{Equations d'état des potentiels de Gogny et M3Y}{section.C.1}% 75
\BOOKMARK [2][-]{subsection.C.1.2}{Contributions des canaux \(S,T\) aux équations d'états de Gogny et M3Y}{section.C.1}% 76
\BOOKMARK [1][-]{section.C.2}{Contributions des ondes partielles pour L<3 aux équations d'états de Gogny et M3Y}{appendix.C}% 77
\BOOKMARK [1][-]{section.C.3}{Relations de passage entre les paramètres de Skyrme N3LO et les paramètres de l'interaction de Gogny et de M3Y}{appendix.C}% 78
\BOOKMARK [2][-]{subsection.C.3.1}{Skyrme-Gogny}{section.C.3}% 79
\BOOKMARK [2][-]{subsection.C.3.2}{Skyrme-M3Y}{section.C.3}% 80
\BOOKMARK [1][-]{section.C.4}{Compléments sur l'invariance de jauge du spin-orbite de portée finie}{appendix.C}% 81
\BOOKMARK [0][-]{appendix.D}{Compléments sur l'interaction de Skyrme dans les noyaux}{}% 82
\BOOKMARK [1][-]{section.D.1}{Constantes\040de\040couplages\040pour\040Skyrme\040N3LO}{appendix.D}% 83
\BOOKMARK [1][-]{section.D.2}{Exemple de calcul en symétrie sphérique: calcul de \(r\)}{appendix.D}% 84
