\select@language {french}
\contentsline {chapter}{\numberline {1}Introduction}{3}{chapter.1}
\contentsline {chapter}{\numberline {2}Interaction nucléon-nucléon et interaction effective}{6}{chapter.2}
\contentsline {section}{\numberline {2.1}Du noyau à l'interaction nucléon-nucléon effective}{6}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Résoudre le problème à N-corps}{6}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Hamiltonien microscopique à N-corps}{8}{subsection.2.1.2}
\contentsline {section}{\numberline {2.2}Forme générale d'un potentiel à deux corps}{8}{section.2.2}
\contentsline {section}{\numberline {2.3}Pseudo-potentiels effectifs phénoménologiques}{10}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Pseudo-potentiel effectif de portée nulle : Skyrme}{10}{subsection.2.3.1}
\contentsline {paragraph}{Interaction standard}{10}{section*.3}
\contentsline {paragraph}{Terme dépendant de la densité}{10}{section*.4}
\contentsline {paragraph}{Termes tenseur}{11}{section*.5}
\contentsline {paragraph}{Avantages et inconvénients}{12}{section*.6}
\contentsline {subsection}{\numberline {2.3.2}Pseudo-potentiel de portée finie: potentiel de Gogny}{13}{subsection.2.3.2}
\contentsline {paragraph}{Forme standard}{13}{section*.7}
\contentsline {paragraph}{Termes tenseurs}{13}{section*.8}
\contentsline {paragraph}{Avantages et inconvénients}{13}{section*.9}
\contentsline {subsection}{\numberline {2.3.3}Pseudo-potentiel de portée finie: potentiel M3Y}{14}{subsection.2.3.3}
\contentsline {paragraph}{Terme central}{14}{section*.10}
\contentsline {paragraph}{Termes tenseurs et spin-orbite}{15}{section*.11}
\contentsline {paragraph}{Avantages et inconvénients}{15}{section*.12}
\contentsline {section}{\numberline {2.4}Extension de Skyrme en gradients jusqu'à l'ordre 6 (N3LO)}{16}{section.2.4}
\contentsline {paragraph}{Motivation}{16}{section*.13}
\contentsline {paragraph}{Forme de l'extension}{17}{section*.16}
\contentsline {paragraph}{Termes tenseur N2LO et N3LO}{18}{section*.17}
\contentsline {paragraph}{Avantages et inconvénients}{18}{section*.18}
\contentsline {chapter}{\numberline {3}Propriétés des potentiels effectifs dans la matière nucléaire infinie}{19}{chapter.3}
\contentsline {section}{\numberline {3.1}Introduction à la matière nucléaire infinie symétrique}{19}{section.3.1}
\contentsline {paragraph}{Motivation}{19}{section*.19}
\contentsline {paragraph}{Propriétés}{20}{section*.20}
\contentsline {paragraph}{Utilisation}{20}{section*.21}
\contentsline {paragraph}{Canaux nucléaires et ondes partielles}{20}{section*.22}
\contentsline {paragraph}{Ondes partielles}{20}{section*.23}
\contentsline {section}{\numberline {3.2}Equations d'états de Skyrme N3LO dans la matière infinie}{21}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Matière infinie symétrique}{21}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Matière infinie de neutrons}{23}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Matière infinie asymétrique}{24}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}Matière infinie polarisée}{24}{subsection.3.2.4}
\contentsline {paragraph}{Matière infinie symétrique polarisée}{24}{section*.24}
\contentsline {paragraph}{Matière infinie de neutron polarisée}{25}{section*.25}
\contentsline {section}{\numberline {3.3}Pseudo-observables de la matière infinie}{25}{section.3.3}
\contentsline {paragraph}{Notations}{25}{section*.26}
\contentsline {paragraph}{Saturation de la matière nucléaire infinie}{25}{section*.27}
\contentsline {paragraph}{Energie par nucléon à la densité de saturation}{26}{section*.28}
\contentsline {paragraph}{Incompressibilité de la matière nucléaire infinie}{26}{section*.29}
\contentsline {paragraph}{Masse effective}{28}{section*.31}
\contentsline {paragraph}{Énergie de symétrie}{28}{section*.32}
\contentsline {section}{\numberline {3.4}\'Etude préliminaire: ajustement des canaux nucléaires et des ondes partielles avec Skyrme N2LO et N3LO}{29}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Ajustement des canaux nucléaires avec Skyrme N2LO et Skyrme N3LO}{29}{subsection.3.4.1}
\contentsline {paragraph}{Méthode et procédure d'ajustement}{29}{section*.33}
\contentsline {subsection}{\numberline {3.4.2}Ajustement de la décompositon en ondes partielles de l'équation d'état avec Skyrme N2LO et Skyrme N3LO}{34}{subsection.3.4.2}
\contentsline {section}{\numberline {3.5}Application de Skyrme N3LO à l'astrophysique}{37}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}Introduction aux étoiles à neutrons}{37}{subsection.3.5.1}
\contentsline {subsection}{\numberline {3.5.2}Ajustement de Skyrme N3LO}{37}{subsection.3.5.2}
\contentsline {subsection}{\numberline {3.5.3}Propriétés résultant de l'ajustement}{40}{subsection.3.5.3}
\contentsline {paragraph}{Pression}{42}{section*.46}
\contentsline {paragraph}{Causalité}{43}{section*.49}
\contentsline {paragraph}{Energie de symétrie}{43}{section*.50}
\contentsline {paragraph}{Masse effective}{44}{section*.53}
\contentsline {paragraph}{Propriétés de l'étoile à neutrons}{46}{section*.56}
\contentsline {chapter}{\numberline {4}Comparaison du potentiel de Skyrme N3LO avec les potentiels de Gogny et M3Y}{49}{chapter.4}
\contentsline {section}{\numberline {4.1}Equations d'états globales des potentiels à portée finie}{49}{section.4.1}
\contentsline {section}{\numberline {4.2}Décomposition en canaux (S,T) des équations d'états pour les potentiels à portée finie}{52}{section.4.2}
\contentsline {section}{\numberline {4.3}Décomposition des équations d'états des interactions M3Y et Gogny en ondes partielles}{55}{section.4.3}
\contentsline {section}{\numberline {4.4}Compléments sur la troisième gaussienne de l'interaction de Gogny}{60}{section.4.4}
\contentsline {paragraph}{Choix des portées}{60}{section*.71}
\contentsline {paragraph}{Ajustement de l'interaction de Gogny avec une troisième gaussienne dans la matière infinie}{62}{section*.73}
\contentsline {section}{\numberline {4.5}De la portée finie à la portée nulle}{66}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}Invariance de jauge locale}{67}{subsection.4.5.1}
\contentsline {subsection}{\numberline {4.5.2}Interaction M3Y}{68}{subsection.4.5.2}
\contentsline {subsection}{\numberline {4.5.3}Interaction de Gogny}{70}{subsection.4.5.3}
\contentsline {subsection}{\numberline {4.5.4}Conclusion sur le lien entre portée finie et portée nulle}{71}{subsection.4.5.4}
\contentsline {chapter}{\numberline {5}Utilisation de Skyrme N2LO dans le cadre du champ moyen en symétrie sphérique}{73}{chapter.5}
\contentsline {section}{\numberline {5.1}Introduction}{73}{section.5.1}
\contentsline {paragraph}{Justification de l'utilisation du champ-moyen}{74}{section*.83}
\contentsline {paragraph}{Obtention de l'équation Hartree-Fock}{74}{section*.84}
\contentsline {paragraph}{Limites de la méthode Hartree-Fock}{75}{section*.85}
\contentsline {section}{\numberline {5.2}Fonctionnelle de Skyrme N2LO}{75}{section.5.2}
\contentsline {paragraph}{Contribution de Skyrme NLO à la densité d'énergie $\mathcal {E}_{Sk} (\mathbf {r})$.}{75}{section*.86}
\contentsline {paragraph}{Densités intervenant dans Skyrme NLO}{76}{section*.87}
\contentsline {paragraph}{Contribution de Skyrme N2LO à la densité d'énergie $\mathcal {E}_{Sk} (\mathbf {r})$}{77}{section*.88}
\contentsline {paragraph}{Contribution de Skyrme N2LO à la densité d'énergie $\mathcal {E}_{Sk} (\mathbf {r})$: cas particulier des densités mixtes.}{78}{section*.89}
\contentsline {section}{\numberline {5.3}Equation du champ-moyen de Skyrme N2LO}{78}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Densités de Skyrme NLO en symétrie sphérique}{79}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Nouvelles densités de N2LO en symétrie sphérique}{81}{subsection.5.3.2}
\contentsline {paragraph}{Densité $\tau _{0, \mu \nu }$ (r)}{81}{section*.92}
\contentsline {paragraph}{Densité Q(r)}{82}{section*.94}
\contentsline {paragraph}{Cas particulier du Supertau}{82}{section*.96}
\contentsline {paragraph}{Densité $V_{0, \mu \nu }$(r)}{83}{section*.98}
\contentsline {paragraph}{Densité $K_{0,\mu \nu \kappa }$(\bm {\mathbf r})}{86}{section*.101}
\contentsline {subsection}{\numberline {5.3.3}Équation du champ-moyen de Skyrme N2LO en symétrie sphérique}{88}{subsection.5.3.3}
\contentsline {subsection}{\numberline {5.3.4}Comportement de la fonction d'onde du système à l'origine pour Skyrme N2LO}{93}{subsection.5.3.4}
\contentsline {subsection}{\numberline {5.3.5}Comportement des termes dépendants de la densité $\rho _0$(r) à l'origine dans Skyrme N2LO}{94}{subsection.5.3.5}
\contentsline {paragraph}{Comportement de la densité $\rho $ et de ses dérivées à l'origine}{94}{section*.104}
\contentsline {paragraph}{Comportement des termes en $C^{M \rho }$ $\rho _0$ à l'origine}{94}{section*.105}
\contentsline {paragraph}{Comportement des termes en $C^{M \rho }$ $\rho _0^{(1)}$ à l'origine}{94}{section*.106}
\contentsline {paragraph}{Comportement des termes en $C^{M \rho }$ $\rho _0^{(2)}$ à l'origine}{95}{section*.107}
\contentsline {paragraph}{Comportement des termes en $C^{M \rho }$ $\rho _0^{(3)}$ à l'origine}{95}{section*.108}
\contentsline {section}{\numberline {5.4}Résolution numérique de Skyrme N2LO à l'approximation Hartree-Fock: WHISKY2}{95}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}Méthode de Hooverman}{97}{subsection.5.4.1}
\contentsline {paragraph}{Remplissage du hamiltonien pour un triplet donné}{97}{section*.110}
\contentsline {paragraph}{Conditions initiales}{98}{section*.111}
\contentsline {subsection}{\numberline {5.4.2}Application de la méthode de Hooverman au cas pratique de l'oscillateur harmonique}{99}{subsection.5.4.2}
\contentsline {paragraph}{Traitement de la dérivée}{99}{section*.114}
\contentsline {paragraph}{Remplissage de la matrice du Hamiltonien}{100}{section*.115}
\contentsline {paragraph}{Résultats}{100}{section*.116}
\contentsline {paragraph}{Inconvénients de la méthode de Hooverman}{101}{section*.118}
\contentsline {subsection}{\numberline {5.4.3}Utilisation d'une méthode à deux bases}{102}{subsection.5.4.3}
\contentsline {subsection}{\numberline {5.4.4}Tests numériques effectués avec WHISKY2}{104}{subsection.5.4.4}
\contentsline {paragraph}{Influence du pas}{104}{section*.120}
\contentsline {paragraph}{Influence de la taille de la boîte}{104}{section*.122}
\contentsline {paragraph}{Vérification de la cohérence des calculs}{105}{section*.124}
\contentsline {paragraph}{Influence de la coupure dans la base Wood-Saxon}{106}{section*.127}
\contentsline {paragraph}{Comportement de la queue de la densité de particule}{107}{section*.130}
\contentsline {paragraph}{Comparaison des résultats avec les programmes \textit {lenteur} et MOCCA}{108}{section*.133}
\contentsline {paragraph}{Remarque générale sur l'influence de Skyrme N2LO sur la densité}{112}{section*.136}
\contentsline {subsection}{\numberline {5.4.5}Implémentation de l'appariement dans WHISKY2}{112}{subsection.5.4.5}
\contentsline {section}{\numberline {5.5}Perspectives sur Skyrme N3LO pour le calcul de noyaux}{114}{section.5.5}
\contentsline {subsection}{\numberline {5.5.1}Fonctionnelle préliminaire de Skyrme N3LO}{114}{subsection.5.5.1}
\contentsline {subsection}{\numberline {5.5.2}Densités de Skyrme N3LO}{115}{subsection.5.5.2}
\contentsline {chapter}{\numberline {6}Ajustement de Skyrme N2L0}{117}{chapter.6}
\contentsline {section}{\numberline {6.1}Méthode d'ajustement}{117}{section.6.1}
\contentsline {section}{\numberline {6.2}Contraintes d'ajustement}{118}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}Matière infinie symétrique et asymétrique}{118}{subsection.6.2.1}
\contentsline {paragraph}{Pseudo-observables}{118}{section*.141}
\contentsline {paragraph}{Matière de neutrons}{119}{section*.143}
\contentsline {subsection}{\numberline {6.2.2}Contraintes assurant la stabilité de l'interaction}{119}{subsection.6.2.2}
\contentsline {paragraph}{Remarque concernant la contrainte de la réponse linéaire}{121}{section*.147}
\contentsline {subsection}{\numberline {6.2.3}Contraintes associées aux calculs de noyaux}{122}{subsection.6.2.3}
\contentsline {paragraph}{\'Energie de liaison}{122}{section*.148}
\contentsline {paragraph}{Rayons de protons}{122}{section*.149}
\contentsline {paragraph}{Paramètre spin-orbite $W_0$}{122}{section*.151}
\contentsline {section}{\numberline {6.3}Stratégie d'ajustement}{123}{section.6.3}
\contentsline {paragraph}{De l'inconvénient d'un traitement perturbatif a priori}{123}{section*.152}
\contentsline {paragraph}{De l'inconvénient d'utiliser une paramétrisation issue des contraintes de la matière infinie}{123}{section*.154}
\contentsline {section}{\numberline {6.4}Résultats}{125}{section.6.4}
\contentsline {subsection}{\numberline {6.4.1}Propriétés de la matière infinie}{126}{subsection.6.4.1}
\contentsline {paragraph}{Pseudo-observables de la matière infinie}{126}{section*.158}
\contentsline {paragraph}{Equations d'état dans la matière infinie}{128}{section*.162}
\contentsline {paragraph}{Canaux (S,T) dans la matière infinie}{128}{section*.164}
\contentsline {subsection}{\numberline {6.4.2}Stabilité de l'interaction}{129}{subsection.6.4.2}
\contentsline {paragraph}{Réponse linéaire dans la matière symétrique}{129}{section*.166}
\contentsline {paragraph}{Réponse linéaire dans la matière de neutrons}{129}{section*.167}
\contentsline {subsection}{\numberline {6.4.3}Propriétés des noyaux}{130}{subsection.6.4.3}
\contentsline {paragraph}{Résultats sur les noyaux ajustés}{131}{section*.170}
\contentsline {paragraph}{Chaînes d'isotopes}{131}{section*.172}
\contentsline {paragraph}{Profils de densité}{134}{section*.177}
\contentsline {subsection}{\numberline {6.4.4}Perspectives générales pour un futur ajustement et note d'intention}{135}{subsection.6.4.4}
\contentsline {chapter}{\numberline {7}Conclusions et Perspectives}{137}{chapter.7}
\contentsline {chapter}{\numberline {A}Ondes partielles dans le potentiel de Skyrme}{147}{appendix.A}
\contentsline {section}{\numberline {A.1}Contributions des ondes partielles dans le potentiel de Skyrme}{147}{section.A.1}
\contentsline {chapter}{\numberline {B}Compléments sur la matière infinie}{149}{appendix.B}
\contentsline {section}{\numberline {B.1}Equation d'état de la matière infinie asymétrique}{149}{section.B.1}
\contentsline {section}{\numberline {B.2}Compléments sur l'utilisation de Skyrme N3LO pour les étoiles à neutrons}{149}{section.B.2}
\contentsline {chapter}{\numberline {C}Formulaire pour les interactions de Gogny et de M3Y}{151}{appendix.C}
\contentsline {section}{\numberline {C.1}Pseudo-observables de la matière infinie des potentiels à portée finie de Gogny et M3Y}{151}{section.C.1}
\contentsline {subsection}{\numberline {C.1.1}Equations d'état des potentiels de Gogny et M3Y}{151}{subsection.C.1.1}
\contentsline {paragraph}{Interaction de Gogny}{151}{section*.181}
\contentsline {paragraph}{Interaction M3Y}{152}{section*.182}
\contentsline {subsection}{\numberline {C.1.2}Contributions des canaux $(S,T)$ aux équations d'états de Gogny et M3Y}{152}{subsection.C.1.2}
\contentsline {paragraph}{Interaction de Gogny}{152}{section*.184}
\contentsline {paragraph}{Interaction M3Y}{153}{section*.186}
\contentsline {section}{\numberline {C.2}Contributions des ondes partielles pour $L<3$ aux équations d'états de Gogny et M3Y}{153}{section.C.2}
\contentsline {section}{\numberline {C.3}Relations de passage entre les paramètres de Skyrme N3LO et les paramètres de l'interaction de Gogny et de M3Y}{156}{section.C.3}
\contentsline {subsection}{\numberline {C.3.1}Skyrme-Gogny}{157}{subsection.C.3.1}
\contentsline {subsection}{\numberline {C.3.2}Skyrme-M3Y}{157}{subsection.C.3.2}
\contentsline {section}{\numberline {C.4}Compléments sur l'invariance de jauge du spin-orbite de portée finie}{157}{section.C.4}
\contentsline {paragraph}{Potentiel de portée finie général}{157}{section*.189}
\contentsline {chapter}{\numberline {D}Compléments sur l'interaction de Skyrme dans les noyaux}{159}{appendix.D}
\contentsline {section}{\numberline {D.1}Constantes de couplages pour Skyrme N3LO}{159}{section.D.1}
\contentsline {section}{\numberline {D.2}Exemple de calcul en symétrie sphérique: calcul de $\rho $(r)}{160}{section.D.2}
