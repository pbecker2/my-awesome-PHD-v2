% -------------------------------------------------------------------
%  @LaTeX-class-file{
%     filename        = "harvard-thesis.cls",
%     version         = "0.1.1",
%     date            = "5 April 2011",
%     createdby       = "Jordan Suchow, modified by Andrew Leifer",
%     address         = "10 Akron St. 309, Cambridge, MA 02138"
%     telephone       = "+1 914 582 2646",
%     email           = "suchow at fas.harvard.edu",
%     codetable       = "ISO/ASCII",
%     keywords        = "LaTeX, harvard-thesis",
%     supported       = "send email to suchow@fas.harvard.edu",
%     docstring       = "A thesis class for a Harvard dissertation."
% --------------------------------------------------------------------

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{harvard-thesis}[2011/09/09 v0.1.1 Harvard University Thesis Class]
\LoadClass[11pt, oneside, letterpaper]{book}


%these should be inpreamble of .tex file, not .cls
\RequirePackage{graphicx}
\RequirePackage{hyperref}
\usepackage[centertags]{amsmath}
\RequirePackage{amssymb}
\RequirePackage[numbers, comma, sort&compress]{natbib}
%\RequirePackage[small, bf, sc]{titlesec}
%\RequirePackage[tight,nice]{units}
\RequirePackage[squaren,Gray]{SIunits}
\RequirePackage{verbatim}
\RequirePackage{url}
\usepackage{amsfonts}
\usepackage{bm}
\usepackage{shadow}
\usepackage{braket}
\usepackage{cleveref}
\usepackage{enumitem}
\usepackage{float}
\usepackage{tikz}
\usetikzlibrary{calc,positioning,decorations.pathreplacing,shapes,shadows,arrows.meta}
\usepackage{varwidth}
\usepackage[francais]{babel}

%\usepackage[usenames]{color}


%Begin Andy's package additions
\usepackage{pdfsync} %do pdf synchronization [andy]
\usepackage[closeFloats, noSeparatorLine]{packages/fltpage} %use the custom modified fltpage package
\RequirePackage{afterpage}

%%%%COMMENT BOX
\usepackage{tcolorbox}
\tcbuselibrary{theorems}
\newtcbtheorem[number within=section]{mytheo}{Exemple}%
{colback=green!5,colframe=green!35!black,fonttitle=\bfseries}{th}


\usepackage{listings}
\usepackage{color}

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\lstset{frame=tb,
  language=Fortran,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=none,
  numberstyle=\tiny\color{gray},
  keywordstyle=\color{blue},
  commentstyle=\color{dkgreen},
  stringstyle=\color{mauve},
  breaklines=true,
  breakatwhitespace=true,
  tabsize=3
}




%\synctex=1 % turn synctex on automatically to sync between pdf viewer [andy]

%   list an entire bibliography entry inline. Useful for acknowledging when my paper was previously published
\usepackage{bibentry}
\nobibliography*
%End Andy's package additions

\RequirePackage{lettrine}%big letter at start of chapter
\RequirePackage[width=15cm,height=22.5cm, a4paper]{geometry}


\RequirePackage{fancyhdr}
\pagestyle{fancy} % options: empty , plain , fancy
\RequirePackage[palatino]{quotchap}
\RequirePackage{titling}
\RequirePackage{setspace}
\RequirePackage{booktabs} % for much better looking tables
\RequirePackage[labelfont={sf,bf,small},textfont={sf,small},justification=RaggedRight,margin=0pt, figurewithin=section, tablewithin=section]{caption}
\onehalfspacing
%\raggedright

\parindent 11pt
\RequirePackage{mathspec}


% Instead of using default latex for math fonts, we can be fancy and try to adapt Chaparral Pro.. --ANDY
%\setmathsfont(Digits,Latin,Greek)[Numbers={OldStyle,Proportional}]{Chaparral Pro} %
%\setmathrm{Chaparral Pro}  %I added this to handle \mathbf

%Use this if you want math fonts that are slightly better than default Latex
\setmathfont{Cambria Math}

\RequirePackage{fontspec,xltxtra,xunicode}%xltxtra,
\defaultfontfeatures{Mapping=tex-text, Numbers=OldStyle}
\setromanfont[Mapping=tex-text, Numbers=OldStyle]{Chaparral Pro-Light} % Contextuals=Swash, Ligatures={{Historic,Contextual,Rare}}



%Set fonts for Mac Specific (Comment out on Windows XP)
%\setsansfont[Scale=MatchLowercase,Mapping=tex-text]{Helvetica Neue}
%\setmonofont[Scale=MatchLowercase]{Consolas}

% some definitions
\def\frenchtitle#1{\gdef\@frenchtitle{#1}}
\def\degreeyear#1{\gdef\@degreeyear{#1}}
\def\degreemonth#1{\gdef\@degreemonth{#1}}
\def\degree#1{\gdef\@degree{#1}}
\def\advisor#1{\gdef\@advisor{#1}}
\def\department#1{\gdef\@department{#1}}
\def\field#1{\gdef\@field{#1}}
\def\university#1{\gdef\@university{#1}}
\def\universitycity#1{\gdef\@universitycity{#1}}
\def\universitystate#1{\gdef\@universitystate{#1}}

% IF FULL
%Avoid clutter when writing: definition
\newif\iffull
%Set to true for dedication etc. pages, false to get rid of them
\fullfalse

\setcounter{tocdepth}{3}
\setcounter{secnumdepth}{3}

\renewcommand{\maketitle}{
\begin{center}
\includegraphics[height=3cm]{img/logo} %le fichier "logo" doit être dans le même dossier que le fichier tex
\end{center}
 \vspace{-0.2cm}

\fontsize{10pt}{12pt}\selectfont
N\textsuperscript{o} d'ordre NNT : xxx

\vspace{0.6cm}

\begin{center}
\fontsize{14pt}{16pt}\selectfont
\textbf{\uppercase{Thèse de doctorat de l'université de Lyon}}\\
\fontsize{12pt}{14pt}\selectfont
opérée au sein de\\
\textbf{l'Université Claude Bernard Lyon 1}

\vspace{0.4cm}

\textbf{École Doctorale ED52\\% rectifier le numéro d'accréditation
Physique et Astrophysique de Lyon}% nom complet de l'école doctorale

\vspace{0.4cm}

\textbf{Spécialité de doctorat : Physique Théorique\\
Discipline : Physique nucléaire } % éventuellement


\vspace{0.5cm}

Soutenue publiquement le 18/09/2017, par :\\
\fontsize{14pt}{16pt}\selectfont
\textbf{Pierre Becker}

\vspace{0.4cm} % adapter à la longueur du titre

\rule[19pt]{\textwidth}{0.6pt}

\fontsize{23pt}{26pt}\selectfont
\textbf{Développement d'une interaction nucléaire effective de nouvelle génération}

\rule{\textwidth}{0.6pt}

\vspace{0.4cm} % adapter à la longueur du titre
\end{center}

\fontsize{11pt}{13pt}\selectfont
Devant le jury composé de :
\bigskip

\fontsize{10pt}{12pt}\selectfont

BENDER Mickaël, Directeur de Recherche CNRS, Université Lyon 1 \hfill Président du jury % mention "président" à ne préciser qu'après la soutenance

\bigskip

COL\`O  Gianluca, Professeur, Université de Milan \hfill  Rapporteur

PERU-DESENFANTS Sophie, Ingénieur de Recherche CEA, CEA Île-de-France \hfill Rapporteure

%BENDER Michael, Directeur de Recherche CNRS, Université Lyon 1 \hfill Examinateur

COURTIN Sandrine, Professeure, Université de Strasbourg \hfill Examinatrice

NAVARRO Jesùs, Directeur de recherche, Université de Valence \hfill Examinateur

PASTORE Alessandro, Maître de conférence, Université de York \hfill Examinateur

\bigskip

DAVESNE Dany, Professeur, Université Lyon 1 \hfill Directeur de thèse

%Nom Prénom, grade/qualité, établissement/entreprise \hfill Co-directeur/trice de thèse % le cas échéant

%Nom Prénom, grade/qualité, établissement/entreprise \hfill Invité(e) % le cas échéant
%\thispagestyle{empty}\vspace*{\fill}
%	\begin{center}
%	\huge \thetitle \normalsize \\ \vspace{100pt}
%	Un thèse présentée  \\ par\\
%	\theauthor\\ pour\\ l' \@department\\
%	dans le cadre de l'obtention du\\
%	doctorat en \\ \@degree\\
%	 en \\ \@field\\
%	\vspace{24pt}
%	\@university\\ \@universitycity, \@universitystate\\
%	\@degreemonth\ \@degreeyear
%	\end{center} \vspace*{\fill}
}

\newcommand{\copyrightpage}{
	\iffull
	\newpage \thispagestyle{empty} \vspace*{\fill}
	\noindent \copyright~\textit{\@degreeyear ~-\theauthor}  \\
	All rights reserved.
	\vspace*{\fill} \newpage \rm
	\fi
}


%Andy's Alternative Creative Commons Licensing (NOTE this is non-traditional, but a good idea)
% See: http://creativecommons.org/weblog/entry/12824 for other PhD students who release their work under creative commons

\newcommand{\abstractpage}{
	\newpage
	\pagenumbering{roman}
	\setcounter{page}{3}
	\pagestyle{fancy}
%	\lhead{\@advisor} \rhead{\@author}
	\renewcommand{\headrulewidth}{0.0pt}
	\begin{center}
	%\normalsize \@frenchtitle \normalsize \\
	\vspace{-2pt}
	\sc Résumé en français \\\rm
	\end{center}
%	\begin{doublespace} %Harvard registrar requests that abstract is double spaced
	\input{frontmatter/abstract}
         \begin{center}
	\vspace{-2pt}
	\sc Abstract \\\rm
	\end{center}
        \input{frontmatter/abstracteng}
%	\end{doublespace}
	\newpage \lhead{} \rhead{}
	\cfoot{\thepage}
}

\newcommand{\dedicationpage}{
	\iffull
	\pagestyle{fancy}
	\newpage \thispagestyle{fancy} \vspace*{\fill}
	\sc \noindent \input{frontmatter/dedication}
	\vspace*{\fill} \newpage \rm
	\fi
	}

	\newcommand{\authorlist}{
	\iffull
		\pagestyle{fancy}
\thispagestyle{fancy}
		\section*{Author List}
		\noindent \input{frontmatter/authorlist}
		\newpage \rm
		\fi
		}


\newcommand{\acknowledgments}{
	\iffull
%	\vspace*{\fill} %This has to be removed, otherwise you get an additional empty page
	\section*{Acknowledgments}
	\noindent
	\input{frontmatter/thanks}
	\vspace*{\fill} \newpage
	\fi
	\setcounter{page}{1}
	\pagenumbering{arabic}
}



% An environment for paragraph-style section

\providecommand\newthought[1]{%
   \addvspace{1.0\baselineskip plus 0.5ex minus 0.2ex}%
   \noindent\textsc{#1}%
}



%Andys Spacing
%Be more forgiving, and stretch word spacing out more so that wormds don't run out into the margins..
\emergencystretch=1.5em
%End Andy Spacing


%Andy's Numbering Fixf
%Fix the figure counting so that it isn't affected by sections so as to be complient with harvard's annoying rule that says Figure 4.2.x cannot be followed by Figure 4.5.1.
\usepackage{chngcntr}

%The way it will now work is that figures will have two numbers and the first number will increment by chapter and the second number will increment within a chapter irrespective of section
\counterwithout{figure}{part}
\counterwithout{figure}{section}
\counterwithin{figure}{chapter}

%Repeat for Tables
\counterwithout{table}{part}
\counterwithout{table}{section}
\counterwithin{table}{chapter}
